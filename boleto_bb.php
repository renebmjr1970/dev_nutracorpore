<?php

	include("include/inc_conexao.php");
	
	
	$pedido	=	addslashes($_REQUEST["pedido"]);
	$email	=	strtolower(addslashes($_REQUEST["email"]));
	
	if(!is_numeric($pedido)){
		header("location: meus-pedidos.php");	
		exit();
	}
	
	
	$ssql = "select tblboleto.bagencia, tblboleto.bconta_corrente, tblboleto.bconta_corrente_dac, tblboleto.bcodpedido, tblboleto.bcodcadastro, tblboleto.bemail, tblboleto.bdata_vencimento, tblboleto.bdata_documento, tblboleto.bnumero_documento, tblboleto.bcarteira, tblboleto.bnosso_numero, 
	tblboleto.bnosso_numero_dac, tblboleto.bvalor_documento, tblboleto.binstrucao, tblboleto.breferencia_numerica, tblboleto.bcodigo_barras, tblboleto.btaxa, tblboleto.bstatus, 
	tblcadastro.crazao_social, tblcadastro.cnome, tblcadastro.ccpf_cnpj, tblcadastro.cemail
	from tblboleto 
	inner join tblcadastro on tblboleto.bcodcadastro = tblcadastro.cadastroid
	where tblboleto.bcodpedido='{$pedido}' and tblboleto.bemail='{$email}'";
	$result = mysql_query($ssql);
	if(!$result){
		echo "Erro ao carregar os dados do boleto<br>";
		//echo mysql_error($conexao);
		exit();
	}
	
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$taxa				=	$row["btaxa"];
			$data_vencimento	=	formata_data_tela($row["bdata_vencimento"]);
			$valor_documento	=	number_format($row["bvalor_documento"],2,",","");
			
			$cpf				=	formata_cpf_cnpj_tela($row["ccpf_cnpj"]);
			
			$nosso_numero		=	$row["bnosso_numero"];
			$numero_documento	=	$row["bnumero_documento"];
			
			$data_documento		=	formata_data_tela($row["bdata_documento"]);
			
			$agencia			=	$row["bagencia"];
			$conta_corrente		=	$row["bconta_corrente"];
			$conta_corrente_dac	=	$row["bconta_corrente_dac"];
			$carteira			=	$row["bcarteira"];
			
			$nome				=	$row["crazao_social"].' '.$row["cnome"];	
		}
		mysql_free_result($result);
	}
	
	
	
	

	
	//carrega variaveis e cedente
		$ssql = "select cedenteid, crazao_social, ccnpj, cbanco, cnumero_banco, cnumero_banco_dac, cagencia, cconta_corrente, cconta_corrente_dac, cnosso_numero, 
				cnumero_documento, ccarteira, cconvenio, ctaxa, cdias from tblcedente where cedenteid = 1";
		$result = mysql_query($ssql);		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$cedenteid			 		=	$row["cedenteid"];
				$cedente_razaosocial 		=	$row["crazao_social"];
				$cedente_cnpj		 		=	formata_cpf_cnpj_tela($row["ccnpj"]);
				$carteira					=	$row["ccarteira"];
				$dias						=	$row["cdias"];
				$convenio					=	$row["cconvenio"];
			}
			mysql_free_result($result);
		}



// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = $dias;
$taxa_boleto = $taxa;
$data_venc = $data_vencimento; //date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
$valor_cobrado = $valor_documento; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto = number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

$dadosboleto["nosso_numero"] = $nosso_numero;  // Nosso numero - REGRA: M�ximo de 8 caracteres!
$dadosboleto["numero_documento"] = $numero_documento;	// Num do pedido ou nosso numero
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = $data_documento; // Data de emiss�o do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula


// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $nome;
$dadosboleto["endereco1"] = $email;
$dadosboleto["endereco2"] = "" ;//$cidade . ' - ' .$estado .  ' - CEP: ' . $cep;
$dadosboleto["cpf"] = $cpf;


// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = utf8_encode("Pedido #$nosso_numero efetuado na loja ") . $site_nome;
$dadosboleto["demonstrativo2"] = utf8_encode("Em caso de d�vidas entre em contato conosco: " . $site_email);
if($taxa_boleto>0){
	$dadosboleto["demonstrativo3"] = utf8_encode("Taxa banc�ria - R$ ").number_format($taxa_boleto, 2, ',', '');
}


// INSTRU��ES PARA O CAIXA
$dadosboleto["instrucoes1"] = utf8_encode("- Sr. Caixa, n�o cobrar multa");
$dadosboleto["instrucoes2"] = utf8_encode("- N�o receber ap�s o vencimento");
$dadosboleto["instrucoes3"] = utf8_encode("");
$dadosboleto["instrucoes4"] = "";


// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "N";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DUP";


// ---------------------- DADOS FIXOS DE CONFIGURA��O DO SEU BOLETO --------------- //


// DADOS DA SUA CONTA - BANCO DO BRASIL
$dadosboleto["agencia"] = $agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $conta_corrente;	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $conta_corrente_dac; 	// Digito do Num da conta

// DADOS PERSONALIZADOS - BANCO DO BRASIL
$dadosboleto["convenio"] = $convenio;  // Num do conv�nio - REGRA: 6 ou 7 ou 8 d�gitos
$dadosboleto["contrato"] = "18939995"; // Num do seu contrato
$dadosboleto["carteira"] = $carteira;
$dadosboleto["variacao_carteira"] = ""; //-19 // Varia��o da Carteira, com tra�o (opcional)

// TIPO DO BOLETO
$dadosboleto["formatacao_convenio"] = strlen($convenio); // REGRA: 8 p/ Conv�nio c/ 8 d�gitos, 7 p/ Conv�nio c/ 7 d�gitos, ou 6 se Conv�nio c/ 6 d�gitos
$dadosboleto["formatacao_nosso_numero"] = 2; // REGRA: Usado apenas p/ Conv�nio c/ 6 d�gitos: informe 1 se for NossoN�mero de at� 5 d�gitos ou 2 para op��o de at� 17 d�gitos

/*
#################################################
DESENVOLVIDO PARA CARTEIRA 18

- Carteira 18 com Convenio de 8 digitos
  Nosso n�mero: pode ser at� 9 d�gitos

- Carteira 18 com Convenio de 7 digitos
  Nosso n�mero: pode ser at� 10 d�gitos

- Carteira 18 com Convenio de 6 digitos
  Nosso n�mero:
  de 1 a 99999 para op��o de at� 5 d�gitos
  de 1 a 99999999999999999 para op��o de at� 17 d�gitos

#################################################
*/


// SEUS DADOS
$dadosboleto["cedente"] = $cedente_razaosocial;
$dadosboleto["identificacao"] = "Boleto " . $site_nome;
$dadosboleto["cpf_cnpj"] = $cedente_cnpj;
$dadosboleto["endereco"] = "";
$dadosboleto["cidade_uf"] = "";

// N�O ALTERAR!
include("include/inc_funcoes_bb.php"); 
include("include/inc_layout_bb.php");
?>
