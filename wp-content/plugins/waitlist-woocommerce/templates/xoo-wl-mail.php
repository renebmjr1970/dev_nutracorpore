<?php

//Exit if accessed directly
if(!defined('ABSPATH')){
	return;
}

global $xoo_wl_emsy_logo_value,$xoo_wl_emsy_align_value;

$product_link = get_permalink($product->post->ID);
$product_name = $product->post->post_title;


//Product image
if(has_post_thumbnail($post_id)){
  $product_image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
}
else{
  if($product_type == 'variable'){
  	$product_image = wp_get_attachment_url(get_post_thumbnail_id($product_id));
  }
  elseif($product_type == 'variation'){
  	$product_image = wp_get_attachment_url(get_post_thumbnail_id($product->post->ID));
  }
  else{
  	$product_image = plugins_url('/assets/images/placeholder.jpg',dirname(__FILE__));
  }  
}

//Logo

if($xoo_wl_emsy_logo_value){
  $logo  = '<tr>';
  $logo .= '<td align="center" style="padding: 0 0 10px 0">';
  $logo .= '<img height="auto" width="auto" border="0" alt="Product Image" src="'.$xoo_wl_emsy_logo_value.'" style="display: block"/>';
  $logo .= '</td></tr>';   
}

$email_all = json_decode($waitlist,true);
$subject = sprintf(__('%s is back in stock','waitlist-woocommerce'),$product_name);

//Email Content
ob_start();
include(plugin_dir_path(__FILE__).'/xoo-wl-email-content.php');
$content = ob_get_clean();

foreach ($email_all as $emails => $qty) {
	wp_mail($emails, $subject, $content);
}
?>