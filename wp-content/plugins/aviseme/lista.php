﻿<?php
set_time_limit(0);

global $wpdb;

$sql 		= "SELECT * FROM tblproduto_aviseme ORDER BY avisemeid ASC";
$retorno 	= $wpdb->get_results($sql);
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>


<script type="text/javascript">
    function enviaemail(avisemeid,produto,nome,email){
        var url_site     = 'http://'+window.location.hostname+'/nutracorpore/';
        var avisemeid = eval(avisemeid);
        var produto = produto;

        console.log("avisemeid: "+avisemeid);
        console.log("produto: "+produto);

         jQuery.ajax({
            url: url_site+'wp-content/themes/nutracorpore/envia_email_aviseme.php',
            data: {avisemeid: avisemeid, produto: produto, nome: nome, email: email},
            type: 'GET',    
            success: function(data) {
                console.log(data);
            }
        });
    }

	jQuery(document).ready(function() {
    	jQuery('#grid').DataTable({
    		"language": {
    			"url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Portuguese-Brasil.json"
    		},
    		dom: 'Bfrtip',
    		buttons: [
             'csv', 'excel', 'pdf', 'print'
        	],
        	"pageLength": 50
    	});
	} );
</script>
<table id="grid" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Produto</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Status</th>
            <th>Data Cadastro</th>
            <th>Ação</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>#</th>
            <th>Produto</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Status</th>
            <th>Data Cadastro</th>
            <th>Ação</th>
        </tr>
    </tfoot>
    <tbody>
    	<?php
    	foreach ($retorno as $resultado) {
    		?>
	        <tr style="text-align: center !important;">
	            <td><?php echo $resultado->avisemeid; ?></td>
                <td><?php echo $resultado->aproduto; ?></td>
	            <td><?php echo $resultado->anome; ?></td>
	            <td><?php echo $resultado->aemail; ?></td>
                <td><?php if($resultado->astatus == 0){ echo "Não atendido"; } else { echo "atendido"; } ?></td>
	            <td><?php echo date('d/m/Y H:m:s',strtotime($resultado->adata_cadastro)); ?></td>
                <td><a href="javascript:void(0);" onclick="enviaemail(<?php echo $resultado->avisemeid;?>,'<?php echo $resultado->aproduto; ?>','<?php echo  $resultado->anome; ?>','<?php echo  $resultado->aemail;?>');">Enviar E-mail</a></td>
	        </tr>
	        <?php
	    }
        ?>
    </tbody>
</table>