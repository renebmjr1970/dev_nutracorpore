﻿<?php
set_time_limit(0);

global $wpdb;

$sql 		= "SELECT * FROM tblcadastro_news ORDER BY cadastroid ASC";
$retorno 	= $wpdb->get_results($sql);
?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>


<script type="text/javascript">
	jQuery(document).ready(function() {
    	jQuery('#grid').DataTable({
    		"language": {
    			"url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Portuguese-Brasil.json"
    		},
    		dom: 'Bfrtip',
    		buttons: [
             'csv', 'excel', 'pdf', 'print'
        	],
        	"pageLength": 50
    	});
	} );
</script>
<table id="grid" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>#</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Data Cadastro</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>#</th>
            <th>Nome</th>
            <th>E-mail</th>
            <th>Data Cadastro</th>
        </tr>
    </tfoot>
    <tbody>
    	<?php
    	foreach ($retorno as $resultado) {
    		?>
	        <tr style="text-align: center !important;">
	            <td><?php echo $resultado->cadastroid; ?></td>
	            <td><?php echo $resultado->cnome; ?></td>
	            <td><?php echo $resultado->cemail; ?></td>
	            <td><?php echo date('d/m/Y H:m:s',strtotime($resultado->cdata_cadastro)); ?></td>
	        </tr>
	        <?php
	    }
        ?>
    </tbody>
</table>
