<?php
/*
Plugin Name: Relatorio Usuarios Cadsatrados Newsletter
Description: Plugin responsável por gerenciar os cadsatros na newsletter
Version: 5.0b (1.0)
Author: Rene Ballesteros Machado Junior
Author URI: http://www.feeshop.com.br
*/

/* Define variáveis estáticas */

// Função ativar	
	@register_activation_hook( $pathPlugin, array('newsletter','ativar'));
 
	// Função desativar
	@register_deactivation_hook( $pathPlugin, array('newsletter','desativar'));
 
	//Ação de criar menu
	add_action('admin_menu', array('newsletter','criarMenu'));
	
	
	
class newsletter {
	
	 public function ativar(){
	 	add_option('newsletter', '');
	 }
	 
	 public function desativar(){
	 	delete_option('newsletter');
	 }
	 
	 public function criarMenu(){
		add_menu_page('newsletter','Newsletter Registros','edit_pages', 'newsletter/lista.php','', 'dashicons-tickets-alt');		  
	 }	
}
?>