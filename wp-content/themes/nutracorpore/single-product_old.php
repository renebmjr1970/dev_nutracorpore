<?php 
get_header();

set_time_limit(0);

$product_variation = new WC_Product_Variable( get_the_ID() );
$variations = $product_variation->get_available_variations();
?>
<main class="product-wrapper">
	<div class="breadcrumb">
		<div class="center-content">	
			<p class="path">
				<a href=""><span> DEFINIÇÃO MUSCULAR /</span></a>
				<a href=""><span> MASSA MUSCULAR /</span></a>
				<a href=""><span> MEU OBJETIVO  /</span></a>
				<a href=""><span> MASSA MAGRA /</span></a>
				<a href=""><span class="actual"> PROTEÍNAS</span></a>
			</p>
		</div>
	</div>
	<section class="product-page">
		<div class="center-content">
			<div class="product-details-segment">	
				<h1 class="product-title bordered">
					<?php echo get_the_title(); ?>
				</h1>
				<div class="bordered">
					<p class="topic">MARCA:</p>
					<h2 class="brand-final"><?php echo get_field('marca', get_the_ID())->post_title; ?></h2>
				</div>
				<fieldset class="rating bordered">
					<legend class="sr-only">De uma nota ao produto:</legend>
					<p class="topic">AVALIAÇÕES:</p>
					<?php
						$rating = $wpdb->get_results('SELECT AVG(rating) AS average_rating FROM product_rating WHERE post_id = '.get_the_ID());
						$average = ceil($rating[0]->average_rating);
					?>
					<form class="product-rating">	
						<input <?php echo ($average == 5) ? 'checked' : ''; ?> type="radio" id="star1" name="rating" value="5" data-id="<?php echo get_the_ID(); ?>" style="background: #000"><label for="star1" title="Uma estrela"></label>
						<input <?php echo ($average == 4) ? 'checked' : ''; ?> type="radio" id="star2" name="rating" value="4" data-id="<?php echo get_the_ID(); ?>"><label for="star2" title="Duas estrelas"></label>
						<input <?php echo ($average == 3) ? 'checked' : ''; ?> type="radio" id="star3" name="rating" value="3" data-id="<?php echo get_the_ID(); ?>"><label for="star3" title="Três estrelas"></label>
						<input <?php echo ($average == 2) ? 'checked' : ''; ?> type="radio" id="star4" name="rating" value="2" data-id="<?php echo get_the_ID(); ?>"><label for="star4" title="Quatro estrelas"></label>
						<input <?php echo ($average == 1) ? 'checked' : ''; ?> type="radio" id="star5" name="rating" value="1" data-id="<?php echo get_the_ID(); ?>"><label for="star5" title="Cinco estrelas"></label>
					</form>
				</fieldset>
				<div class="disponibility bordered">
					<p class="topic">DISPONIBILIDADE: <span><?php echo (get_field('_stock_status') == 'instock') ? 'Em estoque' : 'Fora de Estoque'; ?>.</span></p>
				</div>
				<form id="shipping-product">
					<div class="freight-calculation bordered">
						<p class="topic">CALCULE O FRETE:</p>
						<input type="hidden" name="action" value="shippingProduct">
						<input type="hidden" name="product_id" value="<?php echo get_the_ID(); ?>">
						<input type="text" name="zipcode">
						<button type="submit">
							<i class="fa fa-angle-right" aria-hidden="true"></i>	
						</button>
					</div>
				</form>
				<table class="freight-information" id="shipping_totals">
					<tr>	
						<th>Valor do Frete</th>
						<th>Disponibilidade</th>
					</tr>
					
					<!-- <tr>
						<td>R$ 25.74</td>
						<td>SEDEX para o CEP xxxxx-xxx em até xx dia(s).</td>
					</tr>
					<tr>
						<td>R$ 15.74</td>
						<td>PAC para o CEP xxxxx-xxx em até xx dia(s).</td>
					</tr>
					<tr>
						<td>R$ 14.28</td>
						<td>e-SEDEX para o CEP xxxxx-xxx em até xx dia(s).</td>
					</tr> -->
				</table>
				<a href="">Não sabe seu cep? Clique aqui.</a>
			</div>
			<div class="product-details-segment product-picture">
				<div>	
					<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>" alt="">
				</div>
				<!-- <div>	
					<img src="https://placeholdit.imgix.net/~text?txtsize=27&txt=285%C3%97385&w=285&h=385" alt="">
				</div>
				<div>	
					<img src="https://placeholdit.imgix.net/~text?txtsize=27&txt=285%C3%97385&w=285&h=385" alt="">
				</div> -->
			</div>
			<div class="product-details-segment">
				<?php
				if ($product_values->product_type == 'variable') {
					$full_price = get_field('_max_variation_price');
					if (get_field('_min_variation_price') < $full_price) {
						$price = get_field('_sale_price');
					}
				} else {
					$full_price = get_field('_regular_price');
					$price 		= get_field('_sale_price');
				}
				?>
				<div class="price-and-payment">
					<?php
					if ($price > 0) {
						$installments = number_format(($price / $payment_options['installments']), 2, ',', '.');
						?>
						<p class="discounted-price">De<span class="dashed">R$ <?php echo number_format($full_price, 2, ',', '.'); ?></span> por R$ 
							<?php echo number_format($price, 2, ',', '.'); ?></p>
							<?php
						} else {
							$installments = number_format(($full_price / $payment_options['installments']), 2, ',', '.');
							?>
							<p class="discounted-price">Por R$ <?php echo number_format($full_price, 2, ',', '.'); ?></p>
							<?php
						}
						?>
						<p class="final-installments">Em até <?php echo $payment_options['installments'];?>x de R$ <span class="installment-price">R$ <?php echo $installments; ?></span> sem juros</p>
						<?php
						$final_price = ($price > 0) ? $price : $full_price;
						$price_aux = explode('.', $final_price);
						?>
						<p class="final-price"><span class="xg">R$<span class="xxg"><?php echo $price_aux[0]; ?></span>,
							<?php echo (strlen($price_aux[1]) == 0) ? '00' : $price_aux[1]; ?></span><small>No boleto, depósito ou transferência</small></p>
						</div>
						
						<div class="promotional-information">
							<?php
							if ($price > 0) {
								?>
								<span class="discount-stripe">50 <small>OFF</small></span>
								<?php
							}
							?>
							<?php 
							if (get_field('frete_gratis')) { 
								?>
								<span class="generic-stripe yellow">FRETE GRÁTIS</span>
								<?php
							}
							?>
							<?php 
							if (get_field('lancamento')) { 
								?>
								<span class="generic-stripe orange">LANÇAMENTO</span>
								<?php
							}
							?>
							<!-- <span class="generic-stripe blue">COMPRE GANHE</span> -->
						</div>
						<div class="product-options">
							<?php
							if ($product_values->product_type == 'variable') :
								?>
							<select class="bordered" id="variation">
								<option disabled selected>
									SELECIONE A VARIAÇÃO
								</option>
								<?php
								foreach ($variations as $v) {
									if ($v['max_qty'] > 0) {
										?>
										<option value="<?php echo $v['variation_id']; ?>">
											<?php echo strtoupper(reset($v['attributes'])); ?>
										</option>
										<?php
									}
								}
								?>	
							</select>
							<?php
							endif;
							?>
					<!-- <small>Economize até R$ 249,00</small>
					<select class="bordered">
						<option disabled selected>
							SELECIONE A QUANTIDADE
						</option>
					</select> -->
				</div>
				<button class="add-to-cart add-cart" data-id="<?php echo get_the_ID();?>" data-variation="0" 
					data-variable="<?php echo $product_values->product_type; ?>">
					<i class="fa fa-shopping-cart" aria-hidden="true"></i>
					COMPRAR PRODUTO
				</button>
				<div class="safety">
					<img src="" alt="">
					AMBIENTE 100% SEGURO
				</div>
			</div>
		</div>
	</section>
	<section class="product-description">
		<article class="product-segment">
			<div class="center-content">	
				<h1 class="title-rulers single">
					<img src="" alt="">
					DETALHES DO PRODUTO
				</h1>
				<?php echo get_the_content(); ?>
			</div>
		</article>
		<article class="product-segment">
			<div class="center-content">	
				<h1 class="title-rulers single"><img src="" alt="">BENEFÍCIOS</h1>
				<?php echo get_field('beneficios'); ?>
				<!-- <ul>
					<li>-Whey Protein Isolado</li>
					<li>-Whey Protein Hidrolisado </li>
					<li>-Whey Protein Concentrado </li>
					<li>-Máximo desempenho </li>
					<li>-25g de proteínas por dose;</li>
					<li>-Ação rápida; </li>
					<li>-Fonde de aminoácidos essenciais.</li>
				</ul> -->
			</div>
		</article>
		<article class="product-segment">
			<div class="center-content">	
				<h1 class="title-rulers single"><img src="" alt=""> SUGESTÃO DE USO</h1>
				<?php echo get_field('sugestao_de_uso'); ?>
			</div>
		</article>
		<article class="product-segment">
			<div class="center-content">
				<h1 class="title-rulers single"> <img src="" alt="">INFORMAÇÃO NUTRICIONAL</h1>
				<div class="table-description">
					<p>
						<b>INFORMAÇÃO NUTRICIONAL</b>
						<span>Fighters Food - 1.080g - Nutrabolics</span>
						<span>Porção de 72g (2 colheres-medidas)</span>
					</p>
				</div>
				<table class="nutritional-information">
					<tr>
						<th></th>
						<th>Quant. por Porção</th>
						<th>% VD*</th>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table>
				<div class="disclaimers">
					<p class="note"></p>
					<p class="ingredients"></p>
				</div>
			</div>
		</article>
	</section>

	<section class="gifts">
		<div class="center-content">
			<h1 class="lined">PRODUTOS RELACIONADOS</h1>
			<div class="padded giftbox">
				<div class="shaded-box">
					<div class="inner" data-tab="0">
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled="" selected="">--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled="" selected="">--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled selected>--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled="" selected="">--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled="" selected="">--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php 
get_footer();
?>