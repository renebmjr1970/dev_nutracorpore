<?php
// carregamos o core do wordpress
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $current_user,$woocommerce,$wpdb;


$userID = $current_user->ID;
$user_info = get_userdata($userID);
$return = array();
$newsletter = $_POST['newsletter'];

/* ALTERAR SENHA */
if ( isset( $_POST['editpassword'] ) ) {

	$x = wp_check_password( $_POST['oldpw'], $current_user->user_pass, $userID );

	if($x) {
	    if( !empty($_POST['newpw']) && !empty($_POST['confirmnewpw'])) {
	        if($_POST['newpw'] == $_POST['confirmnewpw']) {
	            $udata['ID'] = $userID;
	            $udata['user_pass'] = $_POST['newpw'];
	            $uid = wp_update_user( $udata );
	            if($uid) {
	            	$return['status'] = 'success';
	            	$return['message'] = 'A senha foi atualizada com sucesso'; 
	            } else {
	            	$return['status'] = 'error';
	            	$return['message'] = 'Falha em atualizar sua senha, tente novamente mais tarde.';
	            }
	        }
	        else {
            	$return['status'] = 'error';
            	$return['message'] = 'Sua senha de confirmação está incorreta';
	        }
	    }
	    else {
        	$return['status'] = 'error';
        	$return['message'] = 'Digite uma nova Senha e a confirmação da mesma';
	    }
	} 
	else {
    	$return['status'] = 'error';
    	$return['message'] = 'Senha atual incorreta';
	}

	$return_JSON = json_encode($return);
	echo $return_JSON;

	unset($_POST);

/* SE NAO, ALTERAÇÃO NORMAL DE CONTA */
} else {

	$sql = "SELECT
		*
	FROM
		`wp_usermeta`

	WHERE
		`user_id` = '".$userID."'
	AND
		meta_key LIKE '%shipping_%'";
	$results = $wpdb->get_results($sql);


	try {

		/* EDIT FROM: Minha Conta */

		if ( isset( $_POST['cep'] ) ) update_user_meta( $userID, 'shipping_postcode', $_POST['cep']);

		if ( isset( $_POST['endereco'] ) ) update_user_meta( $userID, 'shipping_address_1', $_POST['endereco']);
		
		if(get_user_meta($userID, 'billing_number', true) == ""){

			add_user_meta( $userID, 'billing_number', $_POST['numero']);

		}else{

			update_user_meta( $userID, 'billing_number', $_POST['numero']);

		}

		if ( isset( $_POST['complemento'] ) ) update_user_meta( $userID, 'complement', $_POST['complemento']);
		if ( isset( $_POST['referencia'] ) ) update_user_meta( $userID, 'reference', $_POST['referencia']);
		if ( isset( $_POST['bairro'] ) ) update_user_meta( $userID, 'neighborhood', $_POST['bairro']);
		if ( isset( $_POST['cidade'] ) ) update_user_meta( $userID, 'billing_city', $_POST['cidade']);
		if ( isset( $_POST['estado'] ) ) update_user_meta( $userID, 'billing_state', $_POST['estado']);


		$return['status'] = 'success';
		$return['message'] = 'Dados alterados com sucesso';

	} catch (Exception $e) {

		$return['status'] = 'error';
		$return['message'] = 'Ocorreu um erro, tente novamente';

	}

	$return_JSON = json_encode($return);
	echo $return_JSON;

	unset($_POST);
}

