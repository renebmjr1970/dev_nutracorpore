<?php 
	get_header();
?>
<main>
	<?php include 'promotional.php'; ?>
	<section class="generic-institutional">
		<div class="center-content">
			<h1 class="lined">INSTITUCIONAL</h1>
			<div class="padded shaded-box">
				<?php
					while ( have_posts() ) : the_post();
				?>
				<!--<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>" alt="Prateleira com produtos de suplementação" class="headlight">-->
				<h2 class="full-lined red"><?php echo get_the_title(); ?></h2>
				<?php
						the_content();
					endwhile;
				?>
				<!-- <p class="average">
					Com a experiência adquiria em muitos anos de atividade no ramo de venda e distribuição de suplementos através de lojas físicas, a NUTRACORPORE chega na internet para trazer o que tem de melhor no mercado de suplementação e fitness, sempre com os melhores produtos e preços do mercado.
				</p>
				-->
			</div>
		</div>
	</section>
</main>
<?php 
	get_footer();
?>
