<div class="entry shaded-box">
								<div class="picture">
									<a href="<?php echo $values['data']->post->guid; ?>">
										<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $values['product_id'], 'thumbnail' ) ); ?>" alt="<?php echo $values['data']->post->post_title; ?>">
									</a>
								</div>
								<div class="product-info">
									<h1>
										<a href="<?php echo $values['data']->post->guid; ?>">
											<?php echo $values['data']->post->post_title; ?>
										</a>
									</h1>
									<?php
										$sql  = 'SELECT * FROM wp_posts ';
										$sql .= 'WHERE ID = '.get_post_meta($values['product_id'], 'marca', true);
										$brand = $wpdb->get_results($sql);
									?>
									<h2 class="brand"><?php echo $brand[0]->post_title; ?></h2>
									<button class="remove" data-id="<?php echo $item; ?>">✕</button>
								</div>
								<div class="price-quantity">
									<p>R$ <span><?php echo number_format($values['line_subtotal'], 2, ',', '.'); ?></span>/un</p>	
									<button type="button" class="less numeral">-</button>
									<?php
										$quantity = ($values['quantity'] < 10 && !strstr($values['quantity'], '0')) ? '0'.$values['quantity'] : $values['quantity'];
									?>
									<input type="number" name="quantity" data-id="<?php echo $item; ?>" value="<?php echo $quantity; ?>" disabled>
									<button type="button" class="plus numeral">+</button>
								</div>
							</div>