<?php 
	get_header();
?>
<main>
	<section class="promotional">
		<!--
		<div class="center-content">
		<?php
			$args = array(
				'post_type' => 'Banner',
				'posts_per_page' => 1
				);
			
			$loop = new WP_Query( $args );

			if ( $loop->have_posts() ) :
				while ( $loop->have_posts() ) : $loop->the_post();
					if (get_field('tipo_de_banner') == 'faleconosco') :
					 echo '<img src="'.wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) ).'" alt="" class="banner">';
					endif;
				endwhile;
			endif;
		?>
		</div>-->
	</section>
	<div class="center-content">
		<h1 class="lined">FALE CONOSCO</h1>
		<div class="padded">
			<section class="shaded-box contact-us">
				<article class="information-segment">
					<p class="field-descriptor"><b>SAC: </b>(11) 3683.0306</p>
					<p class="field-descriptor"><b>WhatsApp: </b>(11) 97225.3257</p>
					<p class="field-descriptor"><b>E-mail:</b> sac@nutracorpore.com.br</p>
					<p class="field-descriptor">Ou se preferir, use nosso formulário ao lado.</p>
					<p class="field-descriptor"><strong>HORÁRIO DE ATENDIMENTO</strong></p>
					<p class="field-descriptor"><b>Segunda a Quinta </b> das 9h às 19h</p>
					<p class="field-descriptor"><b>Sexta </b>até as 18h</p>
					<p class="field-descriptor"><b>Sábado </b>das 9h às 13h</p>
				</article>
				<article class="information-segment">
					<form id="form-faleconosco" class="contact" method="post" action="/nutracorpore/fale-conosco/#wpcf7-f6141-o1">
						<div style="display: none;">
							<input type="hidden" name="_wpcf7" value="6141">
							<input type="hidden" name="_wpcf7_version" value="4.6.1">
							<input type="hidden" name="_wpcf7_locale" value="pt_BR">
							<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f6141-o1">
							<input type="hidden" name="_wpnonce" value="aed59bbde5">
						</div>
						<fieldset>
							<legend class="sr-only">Suas Informações de Contato:</legend>
							<label>
								<span class="field-descriptor">Nome*</span>
								<input type="text" name="your-name" value="" size="40" class="field wpcf7-form-control wpcf7-text wpcf7-validates-as-required wpcf7-not-valid" aria-required="true" aria-invalid="true">
							</label>
							<label>
								<span class="field-descriptor">E-mail*</span>
								<input type="email" name="your-email" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email wpcf7-not-valid field" aria-required="true" aria-invalid="true">
							</label>
							<label>
								<span class="field-descriptor">Telefone</span>
								<input type="tel" name="your-phone" title="Seu Telefone" class="field wpcf7-form-control wpcf7-text" aria-invalid="false">
							</label>
							<label>
								<span class="field-descriptor">Assunto*</span>
								<input type="text" name="your-subject" required title="Assunto da mensagem" value="" class="field wpcf7-form-control wpcf7-text" aria-invalid="false">
							</label>
							<label>
								<span class="field-descriptor">Cód. de Segurança</span>
								<div id="CaptchaDiv" class="captcha"></div>
								<input name="txtcaptcha" id="txtCaptcha" type="text" class="field">
							</label>
							<label>
								<span class="field-descriptor">Mensagem</span>
								<textarea name="your-message" class="field wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
							</label>
						</fieldset>
						<button class="generic-blue">ENVIAR</button>
					</form>
				</article>
			</section>
		</div>
	</div>
</main>
<?php 
	get_footer();
?>


<script type="text/javascript">
	// Captcha Script

	function checkform(theform){
		var why = "";

		if(theform.CaptchaInput.value == ""){
			why += "- Please Enter CAPTCHA Code.\n";
		}
		if(theform.CaptchaInput.value != ""){
			if(ValidCaptcha(theform.CaptchaInput.value) == false){
				why += "- The CAPTCHA Code Does Not Match.\n";
			}
		}
		if(why != ""){
			alert(why);
			return false;
		}
	}

	var a = Math.ceil(Math.random() * 9)+ '';
	var b = Math.ceil(Math.random() * 9)+ '';
	var c = Math.ceil(Math.random() * 9)+ '';
	var d = Math.ceil(Math.random() * 9)+ '';
	var e = Math.ceil(Math.random() * 9)+ '';

	var code = a + b + c + d + e;
	document.getElementById("CaptchaDiv").innerHTML = code;

	// Validate input against the generated number
	function ValidCaptcha(){
		var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
		var str2 = removeSpaces(document.getElementById('CaptchaInput').value);
		if (str1 == str2){
			return true;
		}else{
			return false;
		}
	}

	// Remove the spaces from the entered and generated code
	function removeSpaces(string){
		return string.split(' ').join('');
	}
</script>

<?php 
	get_footer();
?>
