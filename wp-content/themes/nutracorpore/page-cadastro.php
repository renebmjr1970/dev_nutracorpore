<?php
	if ( is_user_logged_in() ) {
		header('location: '.get_bloginfo('url').'/minha-conta/');
	}
	
	get_header();
	global $woocommerce;
	
	// $woocommerce->customer->set_shipping_city('São Paulo');
	// $woocommerce->customer->set_shipping_state('São Paulo');
	// $woocommerce->customer->set_shipping_address('Rua Doutor Zuquim');
	// $woocommerce->customer->set_shipping_postcode('02035-022');
	// $woocommerce->customer->set_shipping_country('Brasil');
	// echo '<pre>';
	// print_r($woocommerce->shipping_method);
	// echo '</pre>';
	// die();
	// $woocommerce->cart->calculate_shipping();
	// $woocommerce->cart->calculate_totals();
	// echo '<pre>';
	// print_r($woocommerce->cart->get_cart_shipping_total());
	// echo '</pre>';
	// die();
?>
<main>
	<?php include 'promotional.php'; ?>
	<section class="register">
		<div class="center-content">
			<h1 class="lined">CADASTRE-SE</h1>
			<div class="padded">
				<form id="register" method="post">
					<?php if (isset($_GET['flux'])) { ?>
						<input type="hidden" name="action" value="<?php echo $_GET['flux']; ?>">
					<?php } else { ?>
						<input type="hidden" name="action" value="register">
					<?php } ?>
					<article id="tipo-cadastro" class="shaded-box fieldbox register-type">
						<h2 class="has-icon users">TIPO DE CADASTRO</h2>
						<legend>
							<p class="sr-only">
								Escolha seu tipo de cadastro
							</p>
						</legend>
						<label>
							<input type="radio" name="type" value="fisica">
							<span class="average">Pessoa Física</span>
							<span class="error-field"></span>
						</label>
						<label>	
							<input type="radio" name="type" value="juridica">
							<span class="average">Pessoa Jurídica</span>
							<span class="error-field"></span>
						</label>
					</article>
					<article id="dados-acesso" class="shaded-box fieldbox corrected-nth">
						<h2 class="has-icon user">DADOS DE ACESSO</h2>
						<legend>
							<p class="sr-only">
								Preencha seus dados de acesso
							</p>
						</legend>
						<label class="half">
							<span class="average has-icon envelope"></i>Digite seu E-mail</span>
							<input class="field" type="email" name="email">
							<span class="error-field"></span>
						</label>
						<label class="half">	
							<span class="average">Digite sua Senha</span>
							<input class="field" type="password" name="password">
							<span class="error-field"></span>
						</label>
						<label class="half">	
							<span class="average has-icon lock">Confirme seu E-mail</span>
							<input class="field" type="email" name="email-confirm">
							<span class="error-field"></span>
						</label>
						<label class="half">	
							<span class="average">Confirme sua Senha</span>
							<input class="field" type="password" name="password-confirm">
							<span class="error-field"></span>
						</label>
					</article>
					<article id="dados-pessoais" class="shaded-box fieldbox divided">
						<h2 class="has-icon id-card">DADOS PESSOAIS</h2>
						<legend>
							<p class="sr-only">
								Preencha seus dados pessoais
							</p>
						</legend>
						<label>
							<span class="average">Nome Completo</span>
							<input type="text" class="field" name="name">
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average" data-hide="juridica">CPF</span>
							<span class="average" data-hide="fisica" style="display:none">CNPJ</span>
							<input type="text" class="field" name="documento">
							<span class="error-field"></span>
						</label>

						<label data-hide="fisica" style="display: none">
							<span class="average">Razão Social</span>
							<input type="text" class="field" name="razaosocial">
							<span class="error-field"></span>	
						</label>
						<label data-hide="fisica" style="display: none">
							<span class="average">Inscrição Estadual</span>
							<input type="text" class="field" name="inscricaoestadual">
							<span class="error-field"></span>	
						</label>
						<label>
							<span class="average">Celular</span>
							<input type="text" class="field" name="cellphone" placeholder="(11) 11111-1111">
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average">Telefone Fixo</span>
							<input type="text" class="field" name="phone" placeholder="(11) 1111-1111">
							<span class="error-field"></span>
						</label>
						<label data-hide="juridica">
							<span class="average">Sexo</span>
							<select class="field minimal" name="sex">
								<option value="0" disabled selected>- Selecione -</option>
								<option value="Feminino">Feminino</option>
								<option value="Masculino">Masculino</option>
							</select>
							<span class="error-field"></span>
						</label>
						<label data-hide="juridica">
							<span class="average">Data de Nascimento</span>
							<input type="text" class="field" name="birthday" placeholder="11/11/1111">
							<span class="error-field"></span>
						</label>
					</article>
					<article id="endereco" class="shaded-box fieldbox divided">
						<h2 class="has-icon marker">ENDEREÇO</h2>
						<legend>
							<p class="sr-only">
								Preencha seus dados de endereço
							</p>
						</legend>
						<label>
							<span class="average" style="float:left;">CEP</span>
							<small class="zip-code-help average">
								<a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">
									<div class="questionmark">?</div>
									Não sei meu CEP
								</a>
							</small>
							<input type="text" class="field" name="zipcode" onchange="buscacep(this.value);">
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average">Endereço</span>
							<input type="text" class="field" name="address" id="address">
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average">Número</span>
							<input type="number" class="field" name="number">
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average">Complemento</span>
							<input type="text" class="field" name="complement">
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average">Estado</span>
							<select class="field minimal" name="state" id="state">
								<option value="0" disabled selected>- Selecione -</option>
								<option value="AC">Acre</option>
								<option value="AL">Alagoas</option>
								<option value="AP">Amapá</option>
								<option value="AM">Amazonas</option>
								<option value="BA">Bahia</option>
								<option value="CE">Ceará</option>
								<option value="DF">Distrito Federal</option>
								<option value="ES">Espírito Santo</option>
								<option value="GO">Goiás</option>
								<option value="MA">Maranhão</option>
								<option value="MT">Mato Grosso</option>
								<option value="MS">Mato Grosso do Sul</option>
								<option value="MG">Minas Gerais</option>
								<option value="PA">Pará</option>
								<option value="PB">Paraíba</option>
								<option value="PR">Paraná</option>
								<option value="PE">Pernambuco</option>
								<option value="PI">Piauí</option>
								<option value="RJ">Rio de Janeiro</option>
								<option value="RN">Rio Grande do Norte</option>
								<option value="RS">Rio Grande do Sul</option>
								<option value="RO">Rondônia</option>
								<option value="RR">Roraima</option>
								<option value="SC">Santa Catarina</option>
								<option value="SP">São Paulo</option>
								<option value="SE">Sergipe</option>
								<option value="TO">Tocantins</option>
							</select>
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average">Cidade</span>
							<input type="text" class="field" name="city" id="city">
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average">Bairro</span>
							<input type="text" class="field" name="neighborhood" id="neighborhood">
							<span class="error-field"></span>
						</label>
						<label>
							<span class="average">Referência</span>
							<input type="text" class="field" name="reference">
							<span class="error-field"></span>
						</label>
					</article>
					<button id="confirmaCadastro" type="submit" class="generic-blue shaded-box">CONFIRMAR</button>
				</form>
			</div>
		</div>
	</section>
</main>
<?php
	get_footer();
?>
<script type="text/javascript">
	function buscacep(cep){
		var cep = cep;
		var tam = cep.length;
		
		if(eval(tam) < 8) {
			alert("CEP INVÁLIDO, DIGITE NOVAMENTE");
			return false;
		}

		var url = "<?php echo get_bloginfo('template_url'); ?>/ajaxcep.php?cep="+cep;

		$.ajax({
			type: "GET",
			url: url,
			dataType: "html",
			success: function(data){ 
				console.log(data);
			var endereco = data.split(",");
			console.log(endereco);
			
			$("#address").val(endereco[0]);
			$("#city").val(endereco[1]);
			$("#state").val(endereco[2]);
			$("#neighborhood").val(endereco[3]);

			$('[name="number"]').focus();
		  }	
		})
	}
</script>