<?php 
	if (is_user_logged_in() && !isset($_GET['checkout'])) {
		header('Location: '.get_bloginfo('url').'/minha-conta');
	} else if(is_user_logged_in() && isset($_GET['checkout'])) {
		header('Location: '.get_bloginfo('url').'/checkout');
	}
	get_header();

	$showfrete = array();
?>
<main>
	<section class="checkout">
		<div class="center-content">
			<h1 class="lined" id="finalizar" onclick="entrar()" style="cursor: pointer;">FINALIZAR COMPRA</h1>
			<div class="padded extra">
				<article class="fieldbox shaded-box first-step">
					<h2 class="has-icon user"></i>IDENTIFIQUE-SE</h2>
					<form id="login">
						<?php if (isset($_GET['checkout'])) { ?>
							<input type="hidden" name="redirect" value="<?php echo $_GET['checkout']; ?>">
						<?php } else { ?>
							<input type="hidden" name="redirect" value="initial">
						<?php } ?>
						<input type="hidden" name="action" value="login">
						<fieldset>
							<legend class="field-descriptor">Para continuar, informe seus dados de acesso.</legend>
							<label>
								<span class="field-descriptor">
									Digite seu e-mail
								</span>
								<input type="email" name="email" required class="field">
							</label>
							<label>
								<span class="field-descriptor">
									Senha
								</span>
								<input type="password" name="password" required class="field">
							</label>
							<div class="reminders">	
								<a href="<?php echo get_bloginfo('url'); ?>/senha/" style="    margin-bottom: 0.5rem;">
									<span class="field-descriptor has-icon lock">
										Esqueceu a senha?
									</span>
								</a>
								<?php
									if (isset($_GET['checkout'])) :
								?>
										<a href="<?php echo get_bloginfo('url').'/cadastro?flux=checkout'; ?>">
											<span class="field-descriptor"><b>NOVO CADASTRO</b></span>
										</a>
								<?php
									else :
								?>
										<a href="<?php echo get_bloginfo('url').'/cadastro'; ?>">
											<span class="field-descriptor"><b>NOVO CADASTRO</b></span>
										</a>
								<?php
									endif;
								?>
							</div>
							<button class="generic-blue" id="entrar">ENTRAR</button>
						</fieldset>
					</form>
				</article>
				<?php include 'order-resume.php'; ?>
			</div>
		</div>
	</section>
</main>
<?php 
	get_footer();
?>
