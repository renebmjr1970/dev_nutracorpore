<?php
								if (is_user_logged_in()) :
									$shipping = json_decode( shippingCalculate($woocommerce->customer->postcode) );
								foreach ($shipping as $s) :
									?>
								<label>
									<div class="field radiobox">
										<span class="boxed">
											<input type="radio" name="shipping_method" value="<?php echo $s->metodo; ?>">
										</span>
										<small class="freight-price">R$ <?php echo $s->Valor; ?></small>
										<small class="delivery-time"><b> <?php echo $s->PrazoEntrega; ?> Dia(s)</b></small>
										<small class="means"><?php echo $s->metodo; ?></small>
									</div>
								</label>
								<?php
								endforeach;
								endif;
								?>