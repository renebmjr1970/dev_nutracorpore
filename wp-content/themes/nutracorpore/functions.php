<?php

// Add thumbnail support
//--------------------------
add_theme_support( 'post-thumbnails');

/* ************************
POST TYPE BANNERS
************************* */
function Banner() {
    $labels = array(

        'name' => _x('Banners', 'post type general name'),
        'singular_name' => _x('Banners', 'post type singular name'),
        'add_new' => _x('Adicionar Banner', 'Novos Banners'),
        'add_new_item' => __('Novo Banner'),
        'edit_item' => __('Editar Banner'),
        'new_item' => __('Novo Banner'),
        'view_item' => __('Ver Banner'),
        'search_items' => __('Procurar Banner'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Banners'

        );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail')
        );

    register_post_type( 'Banner' , $args );
}

add_action('init', 'Banner');


/* ************************
POST TYPE MARCAS
************************* */
function Marca() {
    $labels = array(

        'name' => _x('Marcas', 'post type general name'),
        'singular_name' => _x('Marcas', 'post type singular name'),
        'add_new' => _x('Adicionar Marca', 'Novos Marcas'),
        'add_new_item' => __('Novo Marca'),
        'edit_item' => __('Editar Marca'),
        'new_item' => __('Novo Marca'),
        'view_item' => __('Ver Marca'),
        'search_items' => __('Procurar Marca'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Marcas'

        );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail')
        );

    register_post_type( 'Marca' , $args );
}

add_action('init', 'Marca');



function addToCart()
{
    global $woocommerce;

    $woocommerce->cart->set_session();
    if (isset($_POST['variation']) && $_POST['variation'] > 0) {
        $pedido = $woocommerce->cart->add_to_cart($_POST["id"], $_POST['quantity'], $_POST['variation']);
    } else {
        $pedido = $woocommerce->cart->add_to_cart($_POST["id"], $_POST['quantity']);
    }
    

    $return = array(
        'success' => true,
        'cart' => $pedido
    );

    print_r(json_encode($return));
    die();
}

add_action('wp_ajax_addToCart', 'addToCart');
add_action('wp_ajax_nopriv_addToCart', 'addToCart');


function couponCalculate()
{
    global $woocommerce;
    print_r($woocommerce->cart->add_discount($_POST['id']));
    die();
}

add_action('wp_ajax_couponCalculate', 'couponCalculate');
add_action('wp_ajax_nopriv_couponCalculate', 'couponCalculate');


function login()
{
    $info = array();
    $info['user_login'] = $_POST['email'];
    $info['user_password'] = $_POST["password"];
    $info['remember'] = true;

    $user_signon = wp_signon( $info, false );

    echo json_encode($user_signon);
    die();
}

add_action('wp_ajax_login', 'login');
add_action('wp_ajax_nopriv_login', 'login');


function changeQuantity()
{
    global $woocommerce, $product;
    $productID = $woocommerce->cart->cart_contents[$_POST['id']]['data']->id;
    
    if ( $_POST['variation'] == 'variation' ) {
        $variation_obj = new WC_Product_variation($_POST['variationID']);
        $qtd = $variation_obj->get_stock_quantity();
        //$qtd = 1;
    } else {
        $product = wc_get_product($productID);
        $qtd = $product->get_stock_quantity();
    }


    if ( $_POST['quantity'] > $qtd ) {
        $return = array(
            'success' =>  false,
            'message' => 'Desculpe, temos apenas '.$qtd.' no estoque deste produto',
            'id' => $_POST['id'],
            'quantity' => $qtd
        );

        $woocommerce->cart->set_quantity($_POST['id'], $qtd, true);
    } else {
        $return = array(
            'success' => true,
            'message' => 'sucesso',
            'id' => $_POST['id'],
            'quantity' => $qtd
        );

        $woocommerce->cart->set_quantity($_POST['id'], $_POST['quantity'], true);
    }

    print_r(json_encode($return));
    die();
}

add_action('wp_ajax_changeQuantity', 'changeQuantity');
add_action('wp_ajax_nopriv_changeQuantity', 'changeQuantity');


function checkUsername()
{
    if ( username_exists( $_POST['user'] ) ) {
        echo 'userexist';
    } else {
        echo 'usernotexist';
    }
    die();
}

add_action('wp_ajax_checkUsername', 'checkUsername');
add_action('wp_ajax_nopriv_checkUsername', 'checkUsername');


function checkEmail()
{
    if ( email_exists( $_POST['email'] ) ) {
        echo 'emailexist';
    } else {
        echo 'emailnotexist';
    }
    die();
}

add_action('wp_ajax_checkEmail', 'checkEmail');
add_action('wp_ajax_nopriv_checkEmail', 'checkEmail');

function removeProduct()
{
    global $woocommerce;
    $woocommerce->cart->remove_cart_item($_POST['id']);

    $return = array(
        'success' => true
    );

    print_r(json_encode($return));
    die();
}

add_action('wp_ajax_removeProduct', 'removeProduct');
add_action('wp_ajax_nopriv_removeProduct', 'removeProduct');


function registerCheckout() 
{
    global $wpdb, $user_ID, $woocommerce;

    $error = array();

    $name             = $_POST['name'];
    $checkSobrenome   = explode( ' ', $_POST['name'] );
    $email            = $_POST['email'];
    $cpf              = $_POST['cpf'];
    $cellphone        = $_POST['cellphone'];
    $password         = $_POST['password'];
    $password_confirm = $_POST['password-confirm'];

    if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error['email'] = 'Preencha corretamente o campo email';
    }

    if (strlen($name) == 0) {
        $error['name'] = 'Preencha corretamente o campo nome';
    }

    if( !isset( $checkSobrenome[1] ) ) {
        $error['name'] = 'Digite seu nome completo';
    }

    if (strlen($cellphone) == 0) {
        $error['cellphone'] = 'Preencha corretamente o campo Telefone';
    }

    if (strlen($cpf) == 0) {
        $error['cpf'] = 'Preencha corretamente o campo CPF';
    }

    if (strlen($password) < 6) {
        $error['password'] = 'Sua senha deve conter mais de 6 caracteres';
    }

    if (! isset($error['password']) && ($password != $password_confirm)) {
        $error['password'] = 'As senhas são incompativeis';
        $error['password-confirm'] = 'As senhas são incompativeis';
    }

    if (count($error) > 0) {
        echo json_encode($error);
        die();
    }
    
    $new_user_id = wp_create_user( $name, $password, $email );
    // print_r($new_user_id);
    // die();
    
    if (is_object($new_user_id)) {
        echo false;
        die();
    }

    add_user_meta( $new_user_id, 'documento', $cpf);
    add_user_meta( $new_user_id, 'cellphone', $cellphone);

    $info = array(
        'user_login' => $email,
        'user_password' => $password,
        'remember' => true
    );

    $user_signon = wp_signon( $info, false );

    echo true;
    die();
}

add_action('wp_ajax_registerCheckout', 'registerCheckout');
add_action('wp_ajax_nopriv_registerCheckout', 'registerCheckout');


function addressCheckout()
{
    global $woocommerce, $current_user;

    $city = $_POST['city'];
    $state = $_POST['state'];
    $zipcode = $_POST['zipcode'];
    $address = $_POST['address'];
    $number = $_POST['number'];
    $complement = $_POST['complement'];
    $reference = $_POST['reference'];
    $neighborhood = $_POST['neighborhood'];
    $userID = $current_user->ID;

    update_user_meta( $userID, 'complement', $complement);
    update_user_meta( $userID, 'reference', $reference);
    update_user_meta( $userID, 'neighborhood', $neighborhood);
    update_user_meta( $userID, 'billing_postcode', $zipcode );
    update_user_meta( $userID, 'shipping_postcode', $zipcode );
    update_user_meta( $userID, 'billing_address_1', $address );
    update_user_meta( $userID, 'shipping_address_1', $address );
    update_user_meta( $userID, 'number', $number );

    $woocommerce->customer->set_shipping_city($city);
    $woocommerce->customer->set_shipping_state($state);
    $woocommerce->customer->set_shipping_address($address);
    $woocommerce->customer->set_shipping_postcode($zipcode);
    $woocommerce->customer->set_shipping_country('BR');

    echo true;
    die();
}

add_action('wp_ajax_addressCheckout', 'addressCheckout');
add_action('wp_ajax_nopriv_addressCheckout', 'addressCheckout');


function validaCPF( $cpf = '' ) { 

    $cpf = str_pad(preg_replace('/[^0-9]/', '', $cpf), 11, '0', STR_PAD_LEFT);
    // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
    if ( strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
        return FALSE;
    } else { // Calcula os números para verificar se o CPF é verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return FALSE;
            }
        }
        return TRUE;
    }
}

function validar_cnpj($cnpj) {
    $cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
    // Valida tamanho
    if (strlen($cnpj) != 14)
        return false;
    // Valida primeiro dígito verificador
    for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
    {
        $soma += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }
    $resto = $soma % 11;
    if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
        return false;
    // Valida segundo dígito verificador
    for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
    {
        $soma += $cnpj{$i} * $j;
        $j = ($j == 2) ? 9 : $j - 1;
    }
    $resto = $soma % 11;
    return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
}

function register()
{
    global $wpdb, $user_ID, $woocommerce;
    $errors = array();

    $formSerialize = $_POST['formSerialize'];
    parse_str($formSerialize, $arr);

    $type = $arr['type'];
    $name = $arr['name'];
    $documento = $arr['documento'];
    $razaosocial = $arr['razaosocial'];
    $inscricaoestadual = $arr['inscricaoestadual'];
    $cellphone = $arr['cellphone'];
    $phone = $arr['phone'];
    $sex = $arr['sex'];
    $birthday = $arr['birthday'];
    $email = $arr['email'];
    $confirm_email = $arr['email-confirm'];
    $password = $arr['password'];
    $password_confirm = $arr['password-confirm'];
    
    $city = $arr['city'];
    $state = $arr['state'];
    $zipcode = $arr['zipcode'];
    $address = $arr['address'];
    $number = $arr['number'];
    $complement = $arr['complement'];
    $reference = $arr['reference'];
    $neighborhood = $arr['neighborhood'];

    $errors['type'] = '';
    if ( $type == 'fisica' ) {

        $errors['cnpj'] = '';
        $errors['razaosocial'] = '';
        $errors['inscricaoestadual'] = '';

        $errors['documento'] = '';
        if ($documento == '' ) {
            $errors['documento'] = 'Preencha corretamente o campo CPF';
        }
        if ( !validaCPF( $documento ) ) {
            $errors['documento'] = 'Preencha corretamente o campo CPF';
        }

        $errors['phone'] = '';
        if ( $phone == '') {
            $errors['phone'] = 'Preencha corretamente o campo Telefone';
        }
        
        $errors['sex'] = '';
        if ( count($sex) == 0 ) {
            $errors['sex'] = 'Preencha corretamente o campo Sexo';
        }

        $errors['birthday'] = '';
        if (strlen($birthday) == 0) {
            $errors['birthday'] = 'Preencha corretamente o campo Data de Nascimento';
        }
    } else if ( $type == 'juridica' ) {
        $errors['phone'] = '';
        $errors['sex'] = '';
        $errors['birthday'] = '';

        $errors['documento'] = '';
        if ( validar_cnpj($documento) || $documento == '' ) {
            $errors['documento'] = 'Preencha corretamente o campo CNPJ';
        }

        if ( $razaosocial = '' ) {
            $errors['razaosocial'] = 'Preencha corretamente o campo Razão Social';
        }

        $errors['inscricaoestadual'] = '';
        if ( $inscricaoestadual == '' ) {
            $errors['inscricaoestadual'] = 'Preencha corretamente o campo Inscrição Estadual';
        }
    } else {
        $errors['type'] = 'Selecione um Tipo de Cadastro';
    }

    $errors['email'] = '';
    $errors['email-confirm'] = '';
    if (! filter_var($email, FILTER_VALIDATE_EMAIL) || $email == '') {
        $errors['email'] = 'Preencha corretamente o campo email';
        $errors['email-confirm'] = '';
    }
    if ( $errors['email'] == '' && ( $email != $confirm_email) ) {
        $errors['email'] = 'Campos de email diferentes';
        $errors['email-confirm'] = 'Campos de email diferentes';
    }

    $errors['password'] = '';
    $errors['password-confirm'] = '';
    if (strlen($password) < 6) {
        $errors['password'] = 'Sua senha deve conter mais de 6 caracteres';
    }
    if ( $errors['password'] == '' && ($password != $password_confirm)) {
        $errors['password'] = 'As senhas são incompativeis';
        $errors['password-confirm'] = 'As senhas são incompativeis';
    }

    $errors['name'] = '';
    if (strlen($name) == 0) {
        $errors['name'] = 'Preencha corretamente o campo nome';
    }

    $errors['cellphone'] = '';
    if (strlen($cellphone) == 0) {
        $errors['cellphone'] = 'Preencha corretamente o campo Celular';
    }

    $errors['city'] = '';
    if (strlen($city) == 0 ) {
        $errors['city'] = 'Preencha corretamente o campo Cidade';
    }

    $errors['state'] = '';
    if (strlen($state) == 0 ) {
        $errors['state'] = 'Preencha corretamente o campo Estado';
    }

    $errors['zipcode'] = '';
    if (strlen($zipcode) == 0 ) {
        $errors['zipcode'] = 'Preencha corretamente o campo CEP';
    }

    $errors['address'] = '';
    if (strlen($address) == 0 ) {
        $errors['address'] = 'Preencha corretamente o campo Endereço';
    }

    $errors['number'] = '';
    if (strlen($number) == 0 ) {
        $errors['number'] = 'Preencha corretamente o campo Número';
    }

    $errors['neighborhood'] = '';
    if (strlen($neighborhood) == 0 ) {
        $errors['neighborhood'] = 'Preencha corretamente o campo Bairro';
    }
    
    $hasError = false;
    foreach ($errors as $error) {
        if ( $error != '' ) {
            $hasError = true;
            break;
        }
    }

    if ( !$hasError ) {
        if ($_POST["id"] == 0) {

            $nameSeparated = explode(' ', $arr['name']);
            $i = 0;
            $firstname = '';
            $lastname = '';

            foreach ($nameSeparated as $value) {
                if ( $i != 0 ) {
                    $lastname .= $value.' ';
                } else {
                    $firstname = $value;
                }
                $i++;
            }

            $values = range(1,49);

            while(count($values)>6) {
              unset($values[array_rand($values)]);
            }

            $username = $firstname.$lastname.str_replace(' ', '', implode(', ',$values));

            $new_user_id = wp_create_user( $username, $password, $email );

            if ( is_wp_error( $new_user_id ) ) {

                $errorUser = $new_user_id->get_error_message();


                $response = array(
                    'success' => false, 
                    'error' => $new_user_id->get_error_message()
                );

            } else {
                $response = array(
                    'success' => true,
                    'id' => $new_user_id
                );

                update_user_meta( $new_user_id, 'first_name', $firstname );
                update_user_meta( $new_user_id, 'last_name', $lastname );
                update_user_meta( $new_user_id, 'nascimento', $arr['birthday']);
                update_user_meta( $new_user_id, 'sexo', $arr['sex']);

                add_user_meta( $new_user_id, 'documento', $arr['documento']);
                add_user_meta( $new_user_id, 'celular', $arr['cellphone']);
                add_user_meta( $new_user_id, 'tipo_pessoa', $arr['type']);
                add_user_meta( $new_user_id, 'number', $arr['number']);
                add_user_meta( $new_user_id, 'neighborhood', $arr['neighborhood']);

                //Billing
                update_user_meta( $new_user_id, 'billing_phone', $arr['phone']);
                update_user_meta( $new_user_id, 'billing_email', $arr['email']);
                update_user_meta( $new_user_id, 'billing_first_name', $firstname );
                update_user_meta( $new_user_id, 'billing_last_name', $lastname );
                update_user_meta( $new_user_id, 'billing_company', $arr['razaosocial'] );
                update_user_meta( $new_user_id, 'billing_address_1', $arr['address']);
                update_user_meta( $new_user_id, 'billing_address_2', $arr['complement'] );
                update_user_meta( $new_user_id, 'billing_city', $arr['city'] );
                update_user_meta( $new_user_id, 'billing_postcode', $arr['zipcode'] );
                update_user_meta( $new_user_id, 'billing_country', 'Brasil' );
                update_user_meta( $new_user_id, 'billing_state', $arr['state'] );

                //Shipping
                update_user_meta( $new_user_id, 'shipping_first_name', $firstname );
                update_user_meta( $new_user_id, 'shipping_last_name', $lastname );
                update_user_meta( $new_user_id, 'shipping_company', $arr['razaosocial']);
                update_user_meta( $new_user_id, 'shipping_address_1', $arr['address']);
                update_user_meta( $new_user_id, 'number', $arr['number']);
                update_user_meta( $new_user_id, 'shipping_address_2', $arr['complement'] );
                update_user_meta( $new_user_id, 'shipping_city', $arr['city'] );
                update_user_meta( $new_user_id, 'shipping_postcode', $arr['zipcode'] );
                update_user_meta( $new_user_id, 'shipping_country', 'Brasil' );
                update_user_meta( $new_user_id, 'shipping_state', $arr['state'] );
            }

            $info = array(
                'user_login' => $email,
                'user_password' => $password,
                'remember' => true
            );

            $user_signon = wp_signon( $info, false );

        } else {
            update_user_meta( $new_user_id, 'first_name', $firstname );
            update_user_meta( $new_user_id, 'last_name', $lastname );
            update_user_meta( $new_user_id, 'nascimento', $arr['birthday']);
            update_user_meta( $new_user_id, 'sexo', $arr['sex']);

            add_user_meta( $new_user_id, 'documento', $arr['documento']);
            add_user_meta( $new_user_id, 'celular', $arr['cellphone']);
            add_user_meta( $new_user_id, 'tipo_pessoa', $arr['type']);
            add_user_meta( $new_user_id, 'number', $arr['number']);
            add_user_meta( $new_user_id, 'neighborhood', $arr['neighborhood'] );

            //Billing
            update_user_meta( $new_user_id, 'billing_phone', $arr['phone']);
            update_user_meta( $new_user_id, 'billing_email', $arr['email']);
            update_user_meta( $new_user_id, 'billing_first_name', $firstname );
            update_user_meta( $new_user_id, 'billing_last_name', $lastname );
            update_user_meta( $new_user_id, 'billing_company', $arr['razaosocial'] );
            update_user_meta( $new_user_id, 'billing_address_1', $arr['address']);
            update_user_meta( $new_user_id, 'billing_address_2', $arr['complement'] );
            update_user_meta( $new_user_id, 'billing_city', $arr['city'] );
            update_user_meta( $new_user_id, 'billing_postcode', $_POST['zipcode'] );
            update_user_meta( $new_user_id, 'billing_country', 'Brasil' );
            update_user_meta( $new_user_id, 'billing_state', $arr['state'] );

            //Shipping
            update_user_meta( $new_user_id, 'shipping_first_name', $firstname );
            update_user_meta( $new_user_id, 'shipping_last_name', $lastname );
            update_user_meta( $new_user_id, 'shipping_company', $arr['razaosocial']);
            update_user_meta( $new_user_id, 'shipping_address_1', $arr['address']);
            update_user_meta( $new_user_id, 'shipping_address_2', $arr['complement'] );
            update_user_meta( $new_user_id, 'shipping_city', $arr['city'] );
            update_user_meta( $new_user_id, 'number', $arr['number']);
            update_user_meta( $new_user_id, 'shipping_postcode', $arr['zipcode'] );
            update_user_meta( $new_user_id, 'shipping_country', 'Brasil' );
            update_user_meta( $new_user_id, 'shipping_state', $arr['state'] );
            update_user_meta( $new_user_id, 'neighborhood', $arr['neighborhood'] );

            $response = array(
                'success' => true,
                'id' => $new_user_id
            );

            $info = array(
                'user_login' => $email,
                'user_password' => $password,
                'remember' => true
            );

            $user_signon = wp_signon( $info, false );
        }
    } else {
        $response = $errors;
    }

    print_r(json_encode($response));
    die();

}

add_action('wp_ajax_register', 'register');
add_action('wp_ajax_nopriv_register', 'register');

function shippingCalculate($cep = '', $product_id = 0)
{
        global $woocommerce,$wpdb;
        $shipping_methods = array('40010' => 'SEDEX', '41106' => 'PAC');

        $items = WC()->cart->get_cart();
        /*foreach ($items as $item) {
            $frete_gratis = get_post_meta($item['product_id'], 'frete_gratis', true);
            if ($frete_gratis) {
                $shipping_cost = 0;
            }
        }*/

        $shipping = new WC_Correios_Webservice();
        if ($product_id > 0) {
            $shipping->set_width( get_post_meta($product_id, '_width', true) );
            $shipping->set_height( get_post_meta($product_id, '_height', true) );
            $shipping->set_length( get_post_meta($product_id, '_length', true) );
            $shipping->set_weight( get_post_meta($product_id, '_weight', true) );
        } else {
            $packages = $woocommerce->cart->get_shipping_packages();
            $woocommerce->cart->calculate_totals();
            $shipping->set_package($packages[0]);
        }

        $shipping->set_service(array(40010, 41106));
        $shipping->set_origin_postcode('04534-011');
        $shipping->set_destination_postcode($cep);
        $pack = $shipping->get_shipping();

        $sql_getFreeShipping = 'SELECT
                                    option_value
                                FROM
                                    wp_options
                                WHERE
                                    option_name = "woocommerce_free_shipping_5_settings"
                                ';

        $results = $wpdb->get_results($sql_getFreeShipping);
        $results_unserialized = maybe_unserialize($results[0]->option_value);
        $valorfretegratis = $results_unserialized['min_amount'];

        $options = array();
        foreach ($pack as $p) {
            $code = intval($p->Codigo);
            $p->metodo = $shipping_methods[$code];
            
            /*
            if (isset($shipping_cost) && $shipping_cost == 0) {
                $p->ValorSemAdicionais = 0;
            } else if ($p->metodo == 'PAC' && $woocommerce->cart->cart_contents_total > $valorfretegratis) {
                $p->ValorSemAdicionais = 0;
            }*/
            if ($p->metodo == 'PAC' && $woocommerce->cart->cart_contents_total > $valorfretegratis) {
                $p->ValorSemAdicionais = 0;
            }

            $options[] = $p;
        }
        
        return json_encode($options);
}


function checkoutCalculate()
{
    $cep = $_POST['cep'];
    echo shippingCalculate($cep);
    die();
}

add_action('wp_ajax_checkoutCalculate', 'checkoutCalculate');
add_action('wp_ajax_nopriv_checkoutCalculate', 'checkoutCalculate');


function shippingProduct()
{
    echo shippingCalculate($_POST['zipcode'], $_POST['product_id']);
    die();
}

add_action('wp_ajax_shippingProduct', 'shippingProduct');
add_action('wp_ajax_nopriv_shippingProduct', 'shippingProduct');


function createOrder()
{
    global $woocommerce;

    print_r($woocommerce->checkout->process_checkout());
    // print_r($woocommerce->checkout->create_order());
    setcookie('address-checkout', '');
    die();
}

add_action('wp_ajax_createOrder', 'createOrder');
add_action('wp_ajax_nopriv_createOrder', 'createOrder');


function rating()
{
    global $wpdb;
    $id = $_POST['id'];
    $rating = $_POST['rating'];

    $results = $wpdb->get_results('
        SELECT
            *
        FROM
            product_rating
        WHERE
            post_id = '.$id.'
    ');

    if ( count($results) == '00' || count($results) == '0' ) {
        $wpdb->query('
            INSERT INTO 
                product_rating(post_id, count, total_rating) 
            VALUES('.$id.', 1, '.$rating.')
        ');
    } else {

        $valTotalRate = $results[0]->total_rating;
        $valTotalRate = $valTotalRate + $rating;

        $valCount = $results[0]->count;
        $valCount++;

        $wpdb->query("
            UPDATE product_rating
            SET
                count = ".$valCount.",
                total_rating = ".$valTotalRate."
            WHERE
                post_id = ".$id."
        ");
    }

}

add_action('wp_ajax_rating', 'rating');
add_action('wp_ajax_nopriv_rating', 'rating');


function filterProducts()
{
    $query = new WP_Query( array( 'cat' => $_POST['categories'] ) );
    
    die();
}

add_action('wp_ajax_filterProducts', 'filterProducts');
add_action('wp_ajax_nopriv_filterProducts', 'filterProducts');


function custom_woocommerce_correios_shipping_methods( $rates, $package ) {
    $items = WC()->cart->get_cart();
    foreach ($items as $item) {
        $frete_gratis = get_post_meta($item['product_id'], 'frete_gratis', true);
        if ($frete_gratis) {
            $shipping_cost = 0;
        }
    }


    if ( isset( WC()->cart->subtotal ) ) {
        foreach ( $rates as $key => $rate ) {
            if (isset($shipping_cost) && $shipping_cost == 0) {
                $rates[ $key ]['cost'] = 0;
            } 

            if ( 'PAC' == $rate['id'] && WC()->cart->cart_contents_total > 149 ) {
                $rates[ $key ]['cost'] = 0;
            }
        }
    }

    return $rates;
}

add_filter( 'woocommerce_correios_shipping_methods', 'custom_woocommerce_correios_shipping_methods', 10, 2 );


function searchProducts()
{
    global $wpdb;

    
    
    // $search = $_POST['search'];
    // $items  = array();
    // $results = $wpdb->get_results('SELECT * FROM wp_posts WHERE post_title LIKE "%'.$search.'%"');
    // foreach ($results as $r) {
    //     $brand = $wpdb->get_results('SELECT meta_value FROM wp_postmeta WHERE post_id = '.$r->ID.' AND (meta_key = "marca")');
    //     $brand_name = $wpdb->get_results('SELECT post_title FROM wp_posts WHERE ID = '.$brand[0]->meta_value);
    //     $price = $wpdb->get_results('SELECT meta_value FROM wp_postmeta WHERE post_id = '.$r->ID.' AND (meta_key = "_sale_price")');

    //     $items[] = array('title' => $r->post_title, 'brand' => $brand_name[0]->post_title, 'price' => number_format($price[0]->meta_value, 2, ',', '.'));
    // }

    // echo json_encode($items);
    die();
}

add_action('wp_ajax_searchProducts', 'searchProducts');
add_action('wp_ajax_nopriv_searchProducts', 'searchProducts');


/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
 register_sidebar( array(
  'name'          => __( 'Sidebar', 'twentysixteen' ),
  'id'            => 'sidebar-1',
  'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentysixteen' ),
  'before_widget' => '<section id="%1$s" class="widget %2$s">',
  'after_widget'  => '</section>',
  'before_title'  => '<h2 class="widget-title">',
  'after_title'   => '</h2>',
 ) );
}
add_action( 'widgets_init', 'twentysixteen_widgets_init' );


if ( ! function_exists( 'woocommerce_breadcrumb' ) ) {

    /**
     * Output the WooCommerce Breadcrumb.
     *
     * @param array $args
     */
    function woocommerce_breadcrumb( $args = array() ) {
        $args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
            'delimiter'   => '&nbsp;&#47;&nbsp;',
            'wrap_before' => '<nav class="breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '><div class="center-content"><p class="path">',
            'wrap_after'  => '</p></div></nav>',
            'before'      => '',
            'after'       => ''
        ) ) );

        $breadcrumbs = new WC_Breadcrumb();

        if ( ! empty( $args['home'] ) ) {
            $breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
        }

        $args['breadcrumb'] = $breadcrumbs->generate();

        wc_get_template( 'global/breadcrumb.php', $args );
    }
}



if ( function_exists( 'register_nav_menu' ) ) {
    register_nav_menu( 'menu_categoria', 'menu-top-category' );
}

function mySearchFilter($query) {
    if ($query->is_search) {
       $query->set('post_type', 'product');
    }
    return $query;
}

add_filter('pre_get_posts','mySearchFilter');

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function setPostViews($postID) {
    global $wpdb;
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
        $sql = "update wp_posts set view_count = $count WHERE ID = $postID";
        $wpdb->query($sql);
    }
}
// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function refreshPagination($qtd_produtos) {
    $qtdList = 12;
    $numPagination = ceil( $qtd_produtos / $qtdList );
    $_SESSION['pagFinal'] = $numPagination;
    $html = '';
    if ( $numPagination > 1 ) {
        $html .= '
        <span class="first-page-numbers previous activate-load" style="display:none;">
            <span class="display-ib">
                <div class="small-arrow left"></div>
            </span>
        </span>
      ';

        $html .= '<span class="page-numbers current">1</span>';
        $i = 1;
        while ( $i < $numPagination ) {
            if ( $i != $numPagination ) {

                if ( $i >= 3 ) {
                
                    if ( $i >= ($numPagination-2) ) {
                        $html .= '<span class="page-numbers hide-this last-page activate-load show-prev-arrow">'.($i+1).'</span>';
                    } else {
                        $html .= '<span class="page-numbers hide-this activate-load show-prev-arrow">'.($i+1).'</span>';
                    }

                } else {
                    $html .= '<span class="page-numbers activate-load show-prev-arrow">'.($i+1).'</span>';
                }
            } else {
                $html .= '<span class="last-page-numbers activate-load show-prev-arrow">... '.($i+1).'</span>';
            }

            $i++;
        }

        if ( $numPagination > 3 ) {
            $html .= '
                <span class="last-page-numbers activate-load show-prev-arrow">... '.$numPagination.'</span>
                <span class="last-page-numbers next activate-load">
                    <span class="display-ib">
                        <div class="small-arrow right"></div>
                    </span>
                </span> 
            ';
        }

        $html .= '
        <style>
        .woocommerce-pagination{opacity: 1 !important;}
        </style>
        ';

        return $html;
    }
}


add_action( 'woocommerce_cart_calculate_fees','calculateShipping' );

function calculateShipping() {
    
    global $woocommerce, $post;
    
    if ( isset( $_POST['value'] ) && isset( $_POST['method'] ) ) {
        setcookie( 'shipping_value', $_POST['value'], time()+604800, '/' );
        setcookie( 'address-checkout', $_POST['method'], time()+604800, '/' );
    }
    
    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
      return;

    if ( isset( $_COOKIE["shipping_value"] ) ) {
        $fee = $_COOKIE["shipping_value"];
        $woocommerce->cart->add_fee( 'Shipping', $fee, true, '' );
    }
}
add_action('wp_ajax_calculateShipping', 'calculateShipping');







function add_class_attachment_link($html){
    $postid = get_the_ID();
    $html = str_replace('<a','<a data-fancybox="gallery"',$html);
    return $html;
}
add_filter('wp_get_attachment_link','add_class_attachment_link',10,1);









add_shortcode('gallery', 'my_gallery_shortcode');    
function my_gallery_shortcode($attr) {
    $post = get_post();

static $instance = 0;
$instance++;

if ( ! empty( $attr['ids'] ) ) {
    // 'ids' is explicitly ordered, unless you specify otherwise.
    if ( empty( $attr['orderby'] ) )
        $attr['orderby'] = 'post__in';
    $attr['include'] = $attr['ids'];
}

// Allow plugins/themes to override the default gallery template.
$output = apply_filters('post_gallery', '', $attr);
if ( $output != '' )
    return $output;

// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
if ( isset( $attr['orderby'] ) ) {
    $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
    if ( !$attr['orderby'] )
        unset( $attr['orderby'] );
}

extract(shortcode_atts(array(
    'order'      => 'ASC',
    'orderby'    => 'menu_order ID',
    'id'         => $post->ID,
    'itemtag'    => 'dl',
    'icontag'    => 'dt',
    'captiontag' => 'dd',
    'columns'    => 3,
    'size'       => 'thumbnail',
    'include'    => '',
    'exclude'    => ''
), $attr));

$id = intval($id);
if ( 'RAND' == $order )
    $orderby = 'none';

if ( !empty($include) ) {
    $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

    $attachments = array();
    foreach ( $_attachments as $key => $val ) {
        $attachments[$val->ID] = $_attachments[$key];
    }
} elseif ( !empty($exclude) ) {
    $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
} else {
    $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
}

if ( empty($attachments) )
    return '';

if ( is_feed() ) {
    $output = "\n";
    foreach ( $attachments as $att_id => $attachment )
        $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
    return $output;
}

$columns = intval($columns);
$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
$float = is_rtl() ? 'right' : 'left';

$selector = "gallery-{$instance}";

$gallery_style = $gallery_div = '';
if ( apply_filters( 'use_default_gallery_style', true ) )
    $gallery_style = "
    <style type='text/css'>
        #{$selector} {
            margin: auto;
        }
        #{$selector} .gallery-item {
            float: {$float};
            margin-top: 10px;
            text-align: center;
            width: {$itemwidth}%;
        }
        #{$selector} img {
            border: 2px solid #cfcfcf;
        }
        #{$selector} .gallery-caption {
            margin-left: 0;
        }
    </style>
    <!-- see gallery_shortcode() in wp-includes/media.php -->";
$size_class = sanitize_html_class( $size );
$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";
$output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );
$fancybox_thumb = '<div id="fancybox-thumbs" class="bottom">
                        <ul style="width: 395px; left: 610px;">';

$i = 0;
foreach ( $attachments as $id => $attachment ) {
    $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_url($id, $size, false, false) : wp_get_attachment_url($id, $size, true, false);
    $output .= '
        <div>
            <a data-fancybox="gallery" rel="group1" href="'.$link.'" tabindex="0"><img width="150" height="150" src="'.$link.'" class="attachment-thumbnail size-thumbnail" alt="" srcset="'.$link.'"></a>
            
        </div>';

    $fancybox_thumb .= '<li class=""><a style="width:75px;height:50px;" href="javascript:jQuery.fancybox.jumpto('.$i.');"><img src="'.$link.'" style="width: 75px; height: 78px; top: -14px; left: 0px;"></a></li>';
    $i++;
}

$fancybox_thumb .= '</ul></div>';

$output .= "
    </div>\n";

return $output.$fancybox_thumb;
}
//Product short description:
add_action( 'wpo_wcpdf_after_item_meta', 'wpo_wcpdf_show_product_description', 10, 3 );
function wpo_wcpdf_show_product_description ( $template_type, $item, $order ) {
    if (empty($item['product'])) return;
    if ( method_exists( $item['product'], 'get_short_description' ) ) {
        $description = $item['product']->get_short_description();
    } else { // WC 2.6 or older:
        $description = $item['product']->post->post_excerpt;
    }
 
    printf('<div class="product-description">%s</div>', $description );
}
//Product full description:
add_action( 'wpo_wcpdf_after_item_meta', 'wpo_wcpdf_show_product_full_description', 10, 3 );
function wpo_wcpdf_show_product_full_description ( $template_type, $item, $order ) {
    if (empty($item['product'])) return;
    if ( method_exists( $item['product'], 'get_description' ) ) {
        $description = $item['product']->get_description();
    } else { // WC 2.6 or older:
        $description = $item['product']->post->post_content;
    }
 
    printf('<div class="product-description">%s</div>', $description );
}
//Product variation description:
add_action( 'wpo_wcpdf_after_item_meta', 'wpo_wcpdf_show_product_variation_description', 10, 3 );
function wpo_wcpdf_show_product_variation_description ( $template_type, $item, $order ) {
    if (empty($item['product'])) return;
    if (isset( $item['variation_id'] ) && $item['variation_id'] != 0) {
        printf('<div class="product-description">%s</div>', $item['product']->get_variation_description() );
    }
}