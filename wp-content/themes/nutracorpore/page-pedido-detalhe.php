<?php 
set_time_limit(0);

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $wpdb, $post, $woocommerce;

$current_user = wp_get_current_user();

/*echo "<pre>";
print_r($_GET);
echo "</pre><hr>";

echo "<pre>";
print_r($current_user);
echo "</pre><hr>";
*/
get_header();

$id = $_GET['id'];

//$antigo = new PDO('mysql:host=mysql5c.winserversecure.com;charset=utf8;dbname=nutracorpore', 'nutracorpore', 'feeshop2k14*');
$antigo = new PDO('mysql:host=localhost;charset=utf8;dbname=nutracorpore', 'root', '');

$ssql = "SELECT
			bnosso_numero,
			bnosso_numero_dac,
			tblpedido.pedidoid,
			tblpedido.pcodorcamento,
			tblpedido.pcodnota_fiscal,
			tblpedido.pcodigo,
			tblpedido.pcodcadastro,
			tblpedido.ptitulo,
			tblpedido.pnome,
			tblpedido.pendereco,
			tblpedido.pnumero,
			tblpedido.pcomplemento,
			tblpedido.pbairro,
			tblpedido.pcidade,
			tblpedido.pestado,
			tblpedido.pcep,
			tblpedido.psubtotal,
			tblpedido.pvalor_desconto,
			tblpedido.pvalor_desconto_cupom,
			tblpedido.pvalor_frete,
			tblpedido.pvalor_presente,
			tblpedido.pvalor_total,
			tblpedido.pcodforma_pagamento,
			tblpedido.pcodcondicao_pagamento,
			tblpedido.pcodfrete,
			tblpedido.pcodcupom_desconto,
			tblpedido.pcodigo_rastreamento,
			tblpedido.pcodstatus,
			tblpedido.pdata_cadastro,
			tblpedido.pcartao_numero,
			tblpedido.pcartao_retorno,
			tblpedido.ptexto_presente,
			tblpedido.preferencia,
			tblpedido.deposito,
			tblcadastro_endereco.etitulo,
			tblcadastro_endereco.enome,
			tblcadastro_endereco.eendereco,
			tblcadastro_endereco.enumero,
			tblcadastro_endereco.ecomplemento,
			tblcadastro_endereco.ebairro,
			tblcadastro_endereco.ecidade,
			tblcadastro_endereco.eestado,
			tblcadastro_endereco.ecep,
			tblcadastro_endereco.ereferencia,
			tblforma_pagamento.fdescricao,
			tblcondicao_pagamento.ccondicao,
			tblcadastro.cadastroid,
			tblcadastro.crazao_social,
			tblcadastro.cnome,
			tblcadastro.ccodtipo,
			tblcadastro.ccpf_cnpj,
			tblcadastro.crg_ie,
			tblcadastro.cdata_nascimento,
			tblcadastro.cemail,
			tblcadastro.ctelefone,
			tblcadastro.ccelular,
			tblcadastro_tipo.ttipo,
			tblpedido_status.statusid,
			tblpedido_status.sdescricao,
			tb_cadastro_entrega.ereferencia AS referencia_entrega,
			tblcupom_desconto.ccupom,
			tbllista_presente.ltitulo,
			tbllista_presente.lcredito,
			tblpedido_status.sstatus,
			tblfrete_tipo.fdescricao AS fretedescricao
		FROM
			tblpedido
		LEFT JOIN tblcadastro_endereco ON tblpedido.pcodendereco_fatura = tblcadastro_endereco.enderecoid
		LEFT JOIN tblcadastro_endereco AS tb_cadastro_entrega ON tblpedido.pcep = tb_cadastro_entrega.ecep
		LEFT JOIN tblcadastro ON tblpedido.pcodcadastro = tblcadastro.cadastroid
		LEFT JOIN tblcadastro_tipo ON tblcadastro.ccodtipo = tblcadastro_tipo.tipoid
		LEFT JOIN tblforma_pagamento ON tblpedido.pcodforma_pagamento = tblforma_pagamento.formapagamentoid
		LEFT JOIN tblcondicao_pagamento ON tblpedido.pcodcondicao_pagamento = tblcondicao_pagamento.condicaoid
		LEFT JOIN tblpedido_status ON tblpedido.pcodstatus = tblpedido_status.statusid
		LEFT JOIN tblcupom_desconto ON tblpedido.pcodcupom_desconto = tblcupom_desconto.cupomid
		LEFT JOIN tbllista_presente ON tblpedido.pcodlista_presente = tbllista_presente.listaid
		LEFT JOIN tblboleto ON tblpedido.pedidoid = tblboleto.bcodpedido
		LEFT JOIN tblfrete_tipo ON tblpedido.pcodfrete = tblfrete_tipo.freteid
		WHERE
			tblpedido.pedidoid = '$id'
		LIMIT 0,
		 1";
		 
$results = $antigo->query($ssql);

if($results){
	foreach ($results as $row) {
			$orcamento				= $row["pcodorcamento"];
			$nota_fiscal			= intval($row["pcodnota_fiscal"]);		
			
			$codigo_boleto			= $row["bnosso_numero"].$row["bnosso_numero_dac"];
			$codigo_pedido	 		= 15500+$row["pcodigo"];
			//$codigo_pedido	 		= substr($codigo_pedido, strlen($codigo_pedido)-6,6);
			$codigo_cliente			= $row["pcodcadastro"];
			$_SESSION['cod']        = $row["pcodcadastro"];
			$razao_social			= $row["crazao_social"] . "&nbsp;" . $row["cnome"];
			$tipo_cadastro			= $row["ttipo"];
			$cnpj_cpf				= $row["ccpf_cnpj"];
			$ie_rg					= $row["crg_ie"];
			//$data_nascimento 		= date('d/m/Y',strtotime($row["cdata_nascimento"]);
			$telefone				= $row["ctelefone"];
			$celular				= $row["ccelular"];
			$data_pedido     		= $row["pdata_cadastro"];
			$identificacao	 		= $row["ptitulo"];
			$nome 			 		= $row["pnome"];
			$email 			 		= $row["cemail"];
			$endereco		 		= $row["pendereco"]. ", " . $row["pnumero"]. " - " . $row["pcomplemento"]. " - " . $row["pbairro"];
			$cidade 		 		= $row["pcidade"];
			$estado			 		= $row["pestado"];
			$cep 			 		= $row["pcep"];
			$cep			 		= substr($cep, 0,5). "-" . substr($cep, 5,3);
			$referencia_entrega		= $row["referencia_entrega"];
			$codigo_rastreamento	= $row["pcodigo_rastreamento"];
			$codigo_forma_pagamento = $row["pcodforma_pagamento"];
			$deposito 				= $row["deposito"];
			
			if($row["etitulo"]!=""){
				$endereco_fatura	=  $row["etitulo"]." - ".$row["eendereco"].",&nbsp;".$row["enumero"]."&nbsp;".$row["ecomplemento"]." - ".$row["ebairro"]." - ".$row["ecidade"]." - ".$row["eestado"]." - ".$row["ecep"];
			}
			
			$subtotal		 		= number_format( $row["psubtotal"], 2, "," , "." );
			$valor_desconto	 		= $row["pvalor_desconto"];
			//$valor_presente			= number_format(($row["pvalor_presente"],2,",","."));
			$valor_frete	 		= $row["pvalor_frete"];
			//$valor_total	 		= number_format(($row["pvalor_total"],2,",","."));
			$forma_pagamento 		= $row["fdescricao"];
			$cod_condicao_pgto 		= $row["pcodcondicao_pagamento"];
			$condicao_pagamento 	= $row["ccondicao"];
			
			$status					= $row["statusid"];
			$status_pedido 			= $row["sdescricao"];
			
			$referencia				= $row["preferencia"];
			
			$tipo_frete 			= $row["pcodfrete"];
			$cupom_desconto			= $row["ccupom"];
			$valor_cupom_desconto	= number_format($row["pvalor_desconto_cupom"],2,",",".");
			
			$texto_presente			= str_replace("\r","<br />",$row["ptexto_presente"]."");
			
			
			$lista_presente_titulo 	= $row["ltitulo"];

			$sstatus			 	= $row["sstatus"];
			$fretedescricao			= $row["fretedescricao"];
			
			
			if($lista_presente_titulo!=""){
				$lista_presente_tipo = ($row["lcredito"]==-1) ? "Crédito" : "Envio de Mercadoria";
			}
			
	}
}

$ssql = "SELECT tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.psubtitulo, tblpedido_item.pvalor_unitario, tblproduto.pdisponivel, tblproduto.ppeso, 
								tblpedido_item.pquantidade, tblpedido_item.pcodproduto, tblpedido_item.ppresente, pai.ppropriedade AS propriedade_pai, tblpedido_item.pcodtamanho, tblproduto_propriedade.ppropriedade AS ptamanho, 
								tblpedido_item.pcodpropriedade, proper.ppropriedade AS ppropriedade,
								(tblpedido_item.pvalor_unitario*tblpedido_item.pquantidade) AS subtotalproduto
								FROM tblproduto
								
								INNER JOIN tblpedido_item ON tblproduto.produtoid = tblpedido_item.pcodproduto
								LEFT JOIN tblproduto_propriedade ON tblpedido_item.pcodtamanho = tblproduto_propriedade.propriedadeid
								LEFT JOIN tblproduto_propriedade AS proper ON tblpedido_item.pcodpropriedade = proper.propriedadeid
								LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
								where tblpedido_item.pcodpedido=$id and tblpedido_item.pquantidade > 0
								";
$results = $antigo->query($ssql);


?>
<div class="woocommerce-MyAccount-content">

<div class="center-content">
	<?php 
		include 'promotional.php'; 
		global $woocommerce, $current_user;
	?>
	<h1 class="lined">MINHA CONTA</h1>
	<div class="padded">
		<?php include_once('account-sidebar.php'); ?>
		<main class="account shaded-box">	
			<h2 class="full-lined red">DETALHES DO PEDIDO</h2>
			<table class="full-order-listing-blue">
				<tbody>
					<tr>
						<th style="text-align:left;">Descrição</th>
						<th>Quantidade</th>
						<th>Preço Unitário</th>
						<th style="text-align:right;">Valor Total</th>
					</tr>
					<?php
						$subtotal = 0;
						$totalPrice = 0;
						if($results){
							foreach ($results as $row) {
								$subtotal .= ($subtotal+$row['subtotalproduto']);
								?>
								<tr class="order_item" style="text-align:left;">
									<td style="text-align:left;">
										<?php echo $row['pproduto'];?> 
									</td>
									<td><?php echo sprintf("%02d", $row['pquantidade']);?></td>
									<td class="product-total">
										<?php echo 'R$ '.number_format($row['subtotalproduto'],2,',','.');?> 
									</td>
									<td style="text-align:right;">
										<?php 
											echo 'R$ '.number_format($row['subtotalproduto'],2,',','.');
											$totalPrice += $row['subtotalproduto'];
										?> 
									</td>
								</tr>
								<?php
							}
						}
					?>
				</tbody>
			</table>
			<style>
				.detailed-info{
					width: 100%;
					text-align: right;
					margin-top: 10px;
					font-size: 1.2rem;
				}
				.detailed-info tr td strong {
				    margin-left: 10px;
				}
				.detailed-info tr td {
				    margin-top: 5px;
				    display: block;
				}
			</style>
			<table class="detailed-info">
				<tr>
					<td>Sub-Total <strong>R$ <?php echo number_format($totalPrice,2,',','.') ?></strong></td>
				</tr>
				<tr>
					<td>Frete <strong>R$ <?php echo number_format($valor_frete,2,',','.'); ?></strong></td>
				</tr>
				<tr>
					<td>Desconto <strong>( R$ <?php echo number_format($valor_desconto,2,',','.'); ?> )</strong></td>
				</tr>
				<tr>
					<td>Total <strong>R$ <?php echo number_format(($totalPrice+$valor_frete)-$valor_desconto,2,',','.'); ?></strong></td>
				</tr>
			</table>
			<div style="text-align: right;margin-top: 20px;">
				<a class="generic-blue" href="javascript:history.go(-1);" style="padding: 10px 20px;text-transform: uppercase;">Voltar</a>
			</div>
		</main>
	</div>
</div>
<?php 
	get_footer(); 
?>