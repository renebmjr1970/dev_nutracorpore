<?php
 
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

if ( isset( $_GET['action'] ) ) {
	$action = $_GET['action'];
}


switch ($action) {
	case 'searchProduct':
		searchProduct();
		break;
	
	case 'ajaxList':
		ajaxList();
		break;
	
	case 'ajaxNewsletteroptin':
		ajaxNewsletteroptin();
		break;

	case 'ajaxAviseme':
		ajaxAviseme();
		break;

	default:
		break;
}

function valid_email($email) {
    return !!filter_var($email, FILTER_VALIDATE_EMAIL);
}

function ajaxAviseme(){
	global $wpdb;

	$nome 		= $_GET['nome'];
    $email 		= $_GET['email'];
    $user_id 	= $_GET['user_id'];
    $produto_id = $_GET['produto_id'];
    $produto 	= $_GET['produto'];

	if(!empty($_GET['nome']) && !empty($_GET['email'])){
		$sql = "INSERT INTO tblproduto_aviseme (accodproduto,aproduto,acodcadastro,anome,aemail,astatus,adata_cadastro) VALUES ($produto_id,'$produto',$user_id,'$nome','$email',0,NOW())";
		$wpdb->query($sql);
		echo 'salvo';
	} else {
		echo 'fail';
	}
}

function ajaxNewsletteroptin(){
	global $wpdb;
	$newsemail = $_GET['newsemail'];

	$sql 		= "SELECT  COUNT(cemail) AS total FROM tblcadastro_news WHERE cemail = '$newsemail'";
	$retorno 	= $wpdb->get_results($sql);

	foreach ($retorno as $resultado) {
		$total = $resultado->total;
	}

	if($total == 0){
		$newsemailAUX = explode('@',$newsemail);
		$sql = "INSERT INTO tblcadastro_news (cnome,cemail,cdata_cadastro) VALUES ('$newsemailAUX[0]','$newsemail',NOW())";
	}

	if($total == 1){
		$sql = "UPDATE tblcadastro_news SET cemail = '$newsemail' WHERE cemail = '$newsemail'";
	}
	$wpdb->query($sql);

	if( valid_email($newsemail) ) {
		echo "Seu e-mail foi cadastrado com sucesso.";
	} else {
		echo "Este não é um e-mail válido";
	}
	
}

function searchProduct() {
	global $wpdb;

	$like_term = '%' . $wpdb->esc_like( $_GET['search'] ) . '%';
	$sql = "
		SELECT DISTINCT
			(post.ID)
		FROM
			wp_posts AS post
		INNER JOIN wp_postmeta ON post.ID = wp_postmeta.post_id
		WHERE
			meta_key = '_stock'
		AND (
			post_title LIKE '$like_term'
			OR post_content LIKE '$like_term'
		)
		AND post_type = 'product'
		AND meta_value > 0
		LIMIT 5
	";
	
	$posts = $wpdb->get_results($sql);
	
	$found_products = array();

	$i = 0;
	foreach ($posts as $post) {
		$produto_id = $post->ID;

		$product = wc_get_product( $produto_id );
		$found_products[$i]['titulo'] = get_the_title($produto_id);
		$found_products[$i]['url'] = get_permalink($produto_id);
		$found_products[$i]['preco'] = number_format($product->get_price(),2,',','.');
		$found_products[$i]['image'] = wp_get_attachment_image_src( get_post_thumbnail_id($produto_id), 'thumbnail' )[0];
		$found_products[$i]['sql'] = $sql;
		
		$i++;
	}

	echo json_encode($found_products);
}

function ajaxList() {
	global $wpdb, $pagFinal;

	$output = '';
	$incrementador = 0;

	@$ids 			= $_GET['postsId'];
	@$mSlugs 		= $_GET['marcaSlug'];
	@$category_name = $_GET['category_name'];
	@$variacoes 	= $_GET['variacoes'];
	@$classificarPor= $_GET['classificarPor'];
	@$paginacao		= $_GET['paginacao'];

	//echo "classificarPor: $classificarPor";

	if(!empty($ids)){
		$valorInCategorias = '(';
		
		foreach ($ids as $value) {
			$valorInCategorias .= '"'.$value.'",';
		}
		$valorInCategorias = rtrim($valorInCategorias, ',');
		$valorInCategorias .= ')';
	}

	if(!empty($mSlugs)){
		$valorInMarcas = '(';

		foreach ($mSlugs as $valueS) {
			$valorInMarcas .= '"'.$valueS.'",';
		}
		$valorInMarcas = rtrim($valorInMarcas, ',');
		$valorInMarcas .= ')';
	}

	if(!empty($variacoes)){
		$valorInvar = '(';

		foreach ($variacoes as $valueV) {
			$valorInvar .= '"'.$valueV.'",';
		}
		$valorInvar = rtrim($valorInvar, ',');
		$valorInvar .= ')';
	}

	//echo "category_name: $category_name<br>";


	$sqlcatpai = "SELECT
					wp_terms.term_id,
					wp_term_relationships.object_id
				FROM
					wp_terms
				INNER JOIN wp_term_relationships ON wp_terms.term_id = wp_term_relationships.term_taxonomy_id
				INNER JOIN wp_posts AS posts ON wp_term_relationships.object_id = posts.ID
				WHERE
					slug = '$category_name' 
				AND posts.post_type = 'product' ";

	//echo $sqlcatpai;
	$rescatpai = $wpdb->get_results($sqlcatpai);

	foreach ($rescatpai as $rescatp):
		$result1[] =  $rescatp->object_id;
	endforeach;

	$sql = "SELECT
				wtrs.object_id
			FROM
				wp_term_relationships
			INNER JOIN wp_term_relationships AS wtrs ON wp_term_relationships.object_id = wtrs.object_id
			WHERE
				1 = 1";
			
			if(!empty($valorInMarcas)){
				$sql .= "	AND wp_term_relationships.term_taxonomy_id IN $valorInMarcas";
			}
			if(!empty($valorInCategorias)){
				$sql .= "	AND wtrs.term_taxonomy_id IN $valorInCategorias";
			}
	$sql .= " GROUP BY object_id ";

	$categorias = $wpdb->get_results($sql);

	foreach ($categorias as $categoria):
		$result2[] = $categoria->object_id;
	endforeach;

	@$resultaux = array_intersect($result1,$result2);

	//echo "conta: ".count($resultaux);

	if(!empty($valorInvar)) {
		$sql = "SELECT
					wp_term_relationships.object_id
				FROM
					wp_terms
				INNER JOIN wp_term_relationships ON wp_terms.term_id = wp_term_relationships.term_taxonomy_id
				WHERE
					slug IN $valorInvar";
		$variacoes = $wpdb->get_results($sql);
		
		foreach ($variacoes as $variacao):
			$result3[] = $variacao->object_id;
		endforeach;
		$resultfinal = array_intersect($resultaux,$result3);
	} else {
		$resultfinal = $resultaux;
	}

	if(!empty($resultfinal)){
		$valorRf = '(';
		foreach ($resultfinal as $valueRF) {
			$valorRf .= '"'.$valueRF.'",';
		}
		$valorRf = rtrim($valorRf, ',');
		$valorRf .= ')';

		$itensEncontrados = count($resultfinal);

		

		$returnPaginacao = refreshPagination($itensEncontrados);


		$sql = "SELECT ID AS object_id FROM wp_posts WHERE ID IN $valorRf";

		if($classificarPor == "nomeProduto") {
			$sql .= " ORDER BY post_title ASC";
		}
		if($classificarPor == "maisAcessados") {
			$sql .= " ORDER BY view_count DESC";
		}
		if($classificarPor == "maisBaratos") {
			$sql = "SELECT
						ID AS object_id
					FROM
						wp_posts
					INNER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
					WHERE
						ID IN $valorRf
					AND meta_key = '_price'
					AND wp_posts.post_type = 'product'
					ORDER BY
						CAST(meta_value AS UNSIGNED) ASC";		
		}
		if($classificarPor == "maisCaros") {
			$sql = "SELECT
						ID AS object_id
					FROM
						wp_posts
					INNER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
					WHERE
						ID IN $valorRf
					AND meta_key = '_price'
					AND wp_posts.post_type = 'product'
					ORDER BY
						CAST(meta_value AS UNSIGNED) DESC";		
		}	
		if($classificarPor == "maisRelevantes") {
			$sql = "SELECT
						post_id AS object_id,
						(total_rating / count) AS total_rating
					FROM
						product_rating
					WHERE
						post_id IN $valorRf";
		}
		if($classificarPor == "maisVendidos") {
			$sql = "SELECT
						ID AS object_id
					FROM
						wp_posts
					LEFT JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
					WHERE
						ID IN $valorRf
					AND meta_key = 'total_sales'
					AND wp_posts.post_type = 'product'
					ORDER BY
						meta_value DESC";		
		}	

		if(empty($paginacao)) {
			$sql .= " LIMIT 0,12";
		} else {
			$fim = ($paginacao*12);

			if ( $paginacao != $_SESSION['pagFinal'] ) {
				$ini = ($fim-12);
			} else {
				$ini = $fim;
			}

			$sql .= " LIMIT $ini,12";
		}
			
		$sqldebug = $sql;

		$categorias = $wpdb->get_results($sql);


		$contaItens = 0;
		foreach ($categorias as $categoria):

			$postId = $categoria->object_id;
			$product = wc_get_product( $postId );

			$tickets = new WC_Product_Variable($postId);
			$variables = $tickets->get_available_variations();
			if(!empty($variables)){
				//echo "<pre>";
				//print_r($variables);
				//echo "</pre>";
				//echo "<hr>";
			}
			unset($tickets);

			if ( $product && $product->get_price()) {
				if ( is_numeric($product->get_price()) ) {
					$payment_options   	= get_option('woocommerce_cielo_credit_settings');
					$payment_discounts 	= get_option('woocommerce_payment_discounts');
					$installments		= number_format(($product->get_price() / $payment_options['installments']), 2, ',', '.');
					$percent 			= str_replace('%', '', $payment_discounts['boleto']['amount']);
					$productType = $product->product_type;
					$addClass = ( $product->product_type != 'variable' && $product->is_in_stock() ) ? '' : 'no-add';

					if(is_array($percent)) {
						$percent = $percent['amount'];
					}

					$output .= '<div class="product">';
					$output .= '<figure>';

					if ( get_field( 'frete_gratis', $postId ) ) { 
						$output .= '<span class="generic-stripe orange">FRETE GRÁTIS</span>';
					}
					if ( get_field( 'lancamento', $postId ) ) { 
						$output .= '<span class="generic-stripe green">LANÇAMENTO</span>';
					}

					$output .= '<img src="'.wp_get_attachment_url( get_post_thumbnail_id( $postId, "thumbnail" ) ).'" alt="">
					<div class="overlay">
						<a href="'.get_the_permalink($postId).'" class="product-detail-page '.$addClass.' ">';
					$output .= '
							<i class="fa fa-search" aria-hidden="true"></i>
						</a>';

						if ( $product->product_type != 'variable' && $product->is_in_stock() ) {
							$output .= '
							<button class="add-cart" data-id="'.$postId.'">
								<i class="fa fa-shopping-cart" aria-hidden="true"></i>
							</button>';
						}

					$output .= '
					</div>
					<figcaption class="name">
						<a href="'.get_the_permalink($postId).'">
						'.get_the_title($postId).'
						</a>
					</figcaption>
					</figure>
						<p class="full-price">
							R$ '.number_format($product->get_price(), 2, ',', '.').'
							<span class="installments">em até '.$payment_options['installments'].'x de R$ '. $installments.'</span>
						</p>
						<p class="discount-price">';
						
						$price = $product->price - ($product->price * ($percent / 100));
						$price = number_format($price, 2, '.', ',');
						$price = explode('.', $price);

						if ( ! isset( $price[1] ) ) {
							$output .= $price[0].',<span class="cents">00</span>';
						} else {
							$output .= $price[0].',<span class="cents">'.$price[1].'</span>';
						}
						
						$output .= '</p>
						<small>no boleto ou depósito</small>
					</div>';
				}
			}
			$contaItens++;
		endforeach;

		if($contaItens >0){
			$return = array(
				'sqlDebug' => $sqldebug,
				'numitens' => $itensEncontrados,
				'paginacao' => $returnPaginacao,
				'datalist' => $output
			);
		} else {
			$return = array(
				'sqlDebug' => '',
				'numitens' => '',
				'paginacao' => '',
				'datalist' => "<span style='color:red;font-size:22px;'>Nenhum resultado encontrado</span>"
			);	
		}
		$returnJSON = json_encode($return);
		echo $returnJSON;
	} else {
		$return = array(
			'sqlDebug' => '',
			'numitens' => '',
			'paginacao' => '',
			'datalist' => "<span style='color:red;font-size:22px;'>Nenhum resultado encontrado</span>"
		);
		$returnJSON = json_encode($return);
		echo $returnJSON;
	}
}