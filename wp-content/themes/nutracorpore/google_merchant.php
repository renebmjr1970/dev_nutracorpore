<?php
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $wpdb, $woocommerce;

//echo "<pre>";
//print_r($woocommerce->customer);
//echo "</pre>";
//die;

$bloginfo 		= get_bloginfo();
$url 			= network_site_url( '/' );
$site_descricao = get_bloginfo( 'description' );

//echo "bloginfo: $bloginfo<br>";
//echo "site_descricao: $site_descricao<br>";
//echo "url: $url<hr>";
//$product_description = $item['product']->post->post_excerpt;
//$product_description = get_the_product_description(10018);

header("Content-type: text/xml");

	echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">';
	echo '<channel>';
	echo "<title>".$bloginfo."</title>";
	echo "<link>".$url."</link>";
	echo "<description>".$site_descricao."</description>";

	$ssql = "SELECT
				pst.ID,
				pst.post_title,
				pst.post_status,
				pst.post_type,
				pst.post_parent,
				(
					SELECT
						pstm.meta_value
					FROM
						wp_postmeta AS pstm
					WHERE
						pstm.post_id = pst.ID
					AND pstm.meta_key = 'marca'
				) AS marca_ID,
				(
					SELECT
						pstmn.post_title
					FROM
						wp_posts AS pstmn
					WHERE
						pstmn.ID = marca_ID
				) AS marca,
				pst.post_name
			FROM
				wp_posts AS pst
			WHERE
				pst.post_status = 'publish'
			AND pst.post_type = 'product'
			ORDER BY
				ID ASC ";
	//echo "<hr>ssql: $ssql<hr>";
	$retorno 	= $wpdb->get_results($ssql);

	//echo "<pre>";
	//print_r($retorno);
	//echo "</pre>";
	// echo "<br>";

	if($retorno){
		foreach ($retorno as $resultado) {
			
			$ID		 	  	= $resultado->ID;
			$produto_data 	= get_product($ID);
			$product_detail = $produto_data->get_data();
			$descricao 		= $product_detail['description'];
			$codigo 		= $product_detail['sku'];
			$produto 		= $product_detail['name'];
			$imagem 		= get_the_post_thumbnail_url($ID, 'full');
			$valor	 		= $product_detail['price'];
			$marca	 		= $resultado->marca;
			$post_name 		= $resultado->post_name;

			$categorias 	= $product_detail['category_ids'];

			/*echo "ID: $ID<br>";
			echo "descricao: $descricao<br>";
			echo "codigo: $codigo<br>";
			echo "produto: $produto<br>";
			echo "imagem: $imagem<br>";
			echo "valor: $valor<br>";
			echo "marca: $marca<br>";*/

/*			echo "<pre>";
			print_r($categorias);
			echo "</pre><hr>";*/

			$categoria	  = '';

			for ($x = 0; $x <= count($categorias)-1; $x++) {
    			$categorianame[] = get_cat_name( $categorias[$x] );

    			//echo $categorias[$x]."<br>";

    			$term = get_term_by('id', $categorias[$x], 'product_cat');
    			$categoriaslug[] = $term->slug;


				

				if($categoria!=""){
					$categoria .= " > ";
				}
				$categoria .= $categorianame[$x];
				$subcategoria = $categoriaslug[$x];	
			} 

			$link  = $url."" . $post_name . "?utm_source=google&utm_medium=shopping&utm_campaign=".$subcategoria;


	/*		echo "<pre>";
			print_r($categorianame);
			echo "</pre><hr>";


			echo "<pre>";
			print_r($categoriaslug);
			echo "</pre><hr>";	*/		

			/*echo "<pre>";
			print_r($product_detail);
			echo "</pre><br>";*/

			
			$codigo		  = $codigo;
			$produto 	  = mb_convert_case(($produto), MB_CASE_TITLE, "UTF-8");
			$imagem		  = $imagem;
			$descricao	  = $descricao;
			$valor		  = number_format($valor,2,",",".");
			$marca		  = $marca;


/*			echo "ID: $ID<br>";
			echo "descricao: $descricao<br>";
			echo "codigo: $codigo<br>";
			echo "produto: $produto<br>";
			echo "imagem: $imagem<br>";
			echo "valor: $valor<br>";
			echo "marca: $marca<br>";

			echo "categoria: $categoria<br>";
			echo "subcategoria: $subcategoria<br>";

			echo "post_name: $post_name<br>";
			echo "link: $link<br>";*/


			
			echo "<item>";
			echo "<title><![CDATA[".$produto."]]></title>";
			echo "<link><![CDATA[".$link."]]></link>";
			echo "<description><![CDATA[".$descricao."]]></description>"; 
			echo "<g:id>".$id."</g:id>";
			echo "<g:condition>new</g:condition>";
			echo "<g:price>".$valor."</g:price>";
			echo "<price-no-rebate>".$comdesconto."</price-no-rebate>";
			echo "<month-price-number>".$parcelas."</month-price-number>";
			echo "<month-price-value>".$parcela."</month-price-value>"; 
			echo "<g:availability>in stock</g:availability>";
			echo "<g:image_link><![CDATA[".$imagem."]]></g:image_link>";
	echo "	<g:shipping>
                <g:country>BR</g:country>
                <g:service>Transportadora</g:service>
                <g:price>0.00</g:price>
            </g:shipping>";
			echo "<g:brand><![CDATA[".$marca."]]></g:brand>";
			echo "<g:google_product_category><![CDATA[".$categoria."]]></g:google_product_category>";
			echo "<g:product_type><![CDATA[".$categoria."]]></g:product_type>";
			echo "<g:mpn><![CDATA[".$codigo."]]></g:mpn>";
			echo "</item>";
			
		}
		
	}

	echo '</channel>';
	echo '</rss>';

?>