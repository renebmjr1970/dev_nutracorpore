<?php 
	get_header();
	include 'promotional-category.php'
?>

<div class="center-content cleared taxonomysec">
	<div class="open-sidebar"><i class="fa fa-filter" aria-hidden="true"></i>Filtro</div>
	<aside class="categories" style="display: none;margin-top: 10px;">
		<?php

			global $wpdb;

			 $aux = explode('/', $_SERVER['REQUEST_URI']);

			//echo "<pre>";
			//print_r($aux);
			//echo "</pre>";

			$total = count($aux);
			$category_name = (empty($aux[$total - 1])) ? $aux[$total - 2] : $aux[$total - 1];
			echo '<div class="close-sidebar">X</div>';
			echo "<input type='hidden' id='category_name' name='category_name' value='$category_name'>";
			/**
			* CATEGORIAS
			* se for o pai, irá aparecer todos os filhos. Se for selecionado o filho, aparecerá apenas ele.
			**/
			$sql = "SELECT
						wp_terms.term_id,
						wp_terms.term_id AS id_filho, 
						wp_terms.name AS nome,
						wp_terms.slug,
						wp_terms.term_group,
						wp_term_taxonomy.parent,
						wp_term_taxonomy.parent AS pai,
						wp_term_taxonomy.count
					FROM
						wp_terms
					INNER JOIN wp_term_taxonomy ON wp_terms.term_id = wp_term_taxonomy.term_id
					WHERE slug = '".sanitize_title($category_name)."'";

			//echo $sql."<hr>";

			$marcas = $wpdb->get_results($sql);

			/*
			if ( $marcas[0]->parent == 0 ) {
				
				echo "<span style='font-family: Oswald-REGULAR;font-size: 16px;cursor: pointer;color: #f00;'>".str_replace('-',' ',strtoupper($category_name))."</span><br><br><br>";
			} else {
				echo "<span style='font-family: Oswald-REGULAR;font-size: 16px;cursor: pointer;color: #f00;'>".str_replace('-',' ',strtoupper($marcas[0]->nome))."</span><br><br><br>";
			}
			*/

			echo '<div class="sidebar-cat-box">';
				echo '<h1 class="categ1">Categorias</h1>';
				echo '<ul class="categorias-content">';

				if ( $marcas[0]->parent != 0 ) {
					echo '<li style="font-size: 16px;background-color: #002c4b;color: white;text-align: center;padding: 10px;">'.strtoupper($category_name).'</li>';
				}

				if ( $marcas[0]->parent == 0 ) {
					$sql = '
						SELECT
							term.term_id,
							term.`name`,
							term.slug,
							term_taxo.term_taxonomy_id AS id_filho,
							term_taxo.taxonomy,
							term_taxo.parent,
							term_taxo.count,
						(
							SELECT
								terms2.`name`
							FROM
								wp_terms AS terms2
							WHERE
								terms2.term_id = term_taxo.term_taxonomy_id
						) AS categoria_filho_name,
						(
							SELECT
								terms3.slug
							FROM
								wp_terms AS terms3
							WHERE
								terms3.term_id = term_taxo.term_taxonomy_id
						) AS categoria_filho_slug
						FROM
							wp_terms AS term
						INNER JOIN wp_term_taxonomy AS term_taxo ON term.term_id = term_taxo.parent
						WHERE
							term.slug = "'.sanitize_title($category_name).'"
							AND term_taxo.parent = term.term_id
							ORDER BY categoria_filho_name ASC;
					';

					//echo "<br>".$sql."<br>";

					$categorias = $wpdb->get_results($sql);

					foreach ($categorias as $categoria):

						//print_r($categoria);
						//echo "<hr>";
						$oclick = "$('.categorias').show();$('.categ1').html('Colocar o nome dos filtros selecionados aqui');";
						echo '
						<li style="font-size:1.4rem">
							<input  onclick="'.$oclick.'" type="checkbox" id="'.$categoria->id_filho.'" value="'.$categoria->id_filho.'" name="categorias[]" class="category-select">

							<label for="'.$categoria->id_filho.'">'.ucfirst(strtolower(htmlentities($categoria->categoria_filho_name))).' ('.$categoria->count.')</label>

						</li>';
						
					endforeach;

					$term_id = $categoria->term_id;

				} else {
					$term_id = $marcas[0]->id_filho;				
				}

				echo '</ul>';
			echo '</div>';


			/**
			* Marcas
			* se for o pai, irá aparecer todos os filhos. Se for selecionado o filho, aparecerá apenas ele.
			**/
			$sql = '
				SELECT DISTINCT
				 (
				  (
				   SELECT
				    wp_posts.post_title
				   FROM
				    wp_postmeta AS pometa
				   INNER JOIN wp_posts ON pometa.meta_value = wp_posts.ID
				   WHERE
				    pometa.post_id = term_relat.object_id
				   AND pometa.meta_key = "marca"
				  )
				 ) AS marca
				FROM
					wp_term_relationships AS term_relat
				INNER JOIN wp_posts AS posts ON term_relat.object_id = posts.ID
				WHERE
					term_relat.term_taxonomy_id = '.$term_id.'
				ORDER BY
					marca ASC
			';

			$marcas = $wpdb->get_results($sql);
			//echo "<br>".$sql."<br>";

			echo '<div class="sidebar-marcas-box">';
				echo '<h1>Marcas</h1>';
				echo '<ul class="marcas-content">';

					foreach ($marcas as $marca):
						$sqlcatid = "SELECT wter.term_id FROM wp_terms AS wter WHERE wter.slug = '$marca->marca'";
						$rescatid = $wpdb->get_results($sqlcatid);
						foreach ($rescatid as $catsid):
							$catid = $catsid->term_id;
						endforeach;
						//echo "catid: $catid<br>";
						
						if ( isset( $catid ) ) {
							$sqlcnt = "SELECT
											wtrs.object_id
										FROM
											wp_term_relationships
										INNER JOIN wp_term_relationships AS wtrs ON wp_term_relationships.object_id = wtrs.object_id
										WHERE
											wp_term_relationships.term_taxonomy_id = $catid
										AND wtrs.term_taxonomy_id = $term_id
										GROUP BY
											object_id";
							//echo $sqlcnt."<br>";

							$rescnt = $wpdb->get_results($sqlcnt);
							$conta = 0;
							foreach ($rescnt as $catcnt):
								$conta++;
							endforeach;
							if($conta >0){
								?>
								<li style="font-size: 1.4rem">
									<input type="checkbox" id="<?php  echo $marca->marca; ?>" value="<?php echo $catid; ?>" class="category-select">
									<label for="<?php echo $marca->marca; ?>"><?php echo ucfirst(strtolower(htmlentities($marca->marca))); ?> (<?php echo $conta;?>)</label>
								</li>
							<?php
							}
						}
						$conta = 0;
					endforeach;

				echo '</ul>';
			echo '</div>';

			/**
			* Variação
			* variações do produto selecionado
			**/

			$sql = "SELECT
				term.term_id,
				term.`name`,
				term.slug,
				term.term_group,
				termtax.term_taxonomy_id,
				termrel.object_id
			FROM
				wp_terms AS term
			INNER JOIN wp_term_taxonomy AS termtax ON term.term_id = termtax.term_id
			INNER JOIN wp_term_relationships AS termrel ON termtax.term_taxonomy_id = termrel.term_taxonomy_id
			WHERE
				term.slug = '$category_name'";		

				//echo $sql;	
			
			$objetos = $wpdb->get_results($sql);
			
			$valorInObjetos = '(';
			foreach ($objetos as $objetosID):
				$valorInObjetos .= '"'.$objetosID->object_id.'",';
			endforeach;
			$valorInObjetos = rtrim($valorInObjetos, ',');
			$valorInObjetos .= ')';

			//echo $valorInObjetos;

			$sql = '
				SELECT
					wp_postmeta.meta_value
				FROM
					wp_posts
				INNER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
				WHERE
					post_parent IN '.$valorInObjetos.'
				AND wp_postmeta.meta_key LIKE "%attribute_pa_%"
				AND wp_postmeta.meta_value <> "" 
				GROUP BY
					meta_value
				ORDER BY
					meta_value ASC
			';

			//echo $sql;

			$marcas = $wpdb->get_results($sql);
			
			if ( !empty($marcas) ) {
				echo '<div class="sidebar-variacoes-box">';
					echo '<h1>Variações</h1>';
					echo '<ul class="variacoes-content">';

						foreach ($marcas as $marca):
							$sqlconta = '
								SELECT COUNT(
									wp_postmeta.meta_value) AS contou
								FROM
									wp_posts
								INNER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
								WHERE
									post_parent IN '.$valorInObjetos.'
								AND wp_postmeta.meta_key LIKE "%attribute_pa_%"
								AND wp_postmeta.meta_value ="'. $marca->meta_value .'"
								ORDER BY
									meta_value ASC
							';		
							//echo $sqlconta."<br>";		
							$variqtd = $wpdb->get_results($sqlconta);
							foreach ($variqtd as $variqtds):
								$contou = $variqtds->contou;
							endforeach;

							if(!empty($marca->meta_value)){
							?>
								<li style="font-size: 1.4rem">
									<input type="checkbox" id="<?php echo $marca->meta_value; ?>" value="<?php echo $marca->meta_value; ?>" class="category-select">
									<label for="<?php echo $marca->meta_value; ?>"><?php echo ucfirst(strtolower($marca->meta_value)); ?> (<?php echo $contou;?>)</label>
								</li>
							<?php
							}
						endforeach;

					echo '</ul>';
				echo '</div>';
			}
		?>
	</aside>
	
	<script>
		
		var hey = parseInt(12);

	</script>

	<?php

	$sql = "SELECT
				term.term_id,
				term.name,
				term.slug,
				term.term_group,
				termtax.term_taxonomy_id,
				termrel.object_id
			FROM
				wp_terms AS term
			INNER JOIN wp_term_taxonomy AS termtax ON term.term_id = termtax.term_id
			INNER JOIN wp_term_relationships AS termrel ON termtax.term_taxonomy_id = termrel.term_taxonomy_id
			INNER JOIN wp_posts AS posts ON termrel.object_id = posts.ID
			INNER JOIN wp_postmeta AS post_meta ON posts.ID = post_meta.post_id 
 			WHERE
				term.slug = '$category_name'
			AND posts.post_type = 'product'
			AND post_meta.meta_value = 'instock'
			";
	
	//echo "sql:<br> $sql<br>";
	//echo "<hr>";
	
	$results = $wpdb->get_results($sql);	

	$sql = "SELECT
				term.term_id,
				term.name,
				term.slug,
				term.term_group,
				termtax.term_taxonomy_id,
				termrel.object_id
			FROM
				wp_terms AS term
			INNER JOIN wp_term_taxonomy AS termtax ON term.term_id = termtax.term_id
			INNER JOIN wp_term_relationships AS termrel ON termtax.term_taxonomy_id = termrel.term_taxonomy_id
			INNER JOIN wp_posts AS posts ON termrel.object_id = posts.ID
			WHERE
				term.slug = '$category_name'
			AND posts.post_type = 'product' ";


		$resultsc = $wpdb->get_results($sql);	
		$qtd_produtos = count($resultsc);

	//echo "sql:<br> $sql<br>";
	//echo "<hr>";
	?>
	
	<main class="category">
		<div class="pagination">
			<small class="index display-ib vertical-middle"><div class="display-ib qtdProdEncontrados"><?php echo $qtd_produtos;?></div> Itens encontrados <?php echo $limite; ?></small><!--
			--><nav class="woocommerce-pagination display-ib vertical-middle" style="opacity: 0">
					<!-- Página: <?php echo refreshPagination($qtd_produtos);  ?> -->
				</nav><!--
			--><div class="sorting display-ib vertical-middle">
				<label style="display: block;width: 100%;padding-top: 5px;">
					<span style="vertical-align: top;">Classificar por:</span>
					<select class="classificar-por" name="classificarPor">
						<option disabled selected>Selecione</option>
						<option value="maisRelevantes">Mais Avaliados</option>
						<option value="maisVendidos">Mais Vendidos</option>
						<option value="maisAcessados">Mais Acessados</option>
						<option value="maisCaros">Maior Valor</option>
						<option value="maisBaratos">Menor Valor</option>
						<option value="nomeProduto">Nome do Produto</option>
					</select>
				</label>
			</div>
		</div>
		<div class="grid-products greyed four">
			<?php
				$payment_options   = get_option('woocommerce_cielo_credit_settings');
				$payment_discounts = get_option('woocommerce_payment_discounts');
				$percent 		   = str_replace('%', '', $payment_discounts['boleto']['amount']);
				$conta 			   = 1;
				foreach ($results as $r) :
					$args = array(
						'post_type' => 'product',
						'post__in' => array($r->object_id)
					);
					$loop = new WP_Query( $args );
					if ( $loop->have_posts() ) :
						while ( $loop->have_posts() ) : $loop->the_post();
							$installments = number_format(($product->get_price() / $payment_options['installments']), 2, ',', '.');
			?>	
						<div class="product">
							<figure>
								<?php 
								if (get_field('frete_gratis')) { 
									?>
									<span class="generic-stripe orange">FRETE GRÁTIS</span>
									<?php
								}
								?>
								<?php 
								if (get_field('lancamento')) { 
									?>
									<span class="generic-stripe green">LANÇAMENTO</span>
									<?php
								}
								?>
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="">
								<div class="overlay">
									<?php echo ">>>>".$product->is_in_stock(); ?>
									<a href="<?php echo get_the_permalink(); ?>" class="product-detail-page 
									<?php echo ( $product->product_type != 'variable' && $product->is_in_stock() ) ? '' : 'no-add';?>">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
									<?php
									if ( $product->product_type != 'variable' && $product->is_in_stock() ) :
									?>
									<button class="add-cart activate-load" data-id="<?php echo get_the_ID();?>">
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>
									</button>
									<?php
									endif;
									?>
								</div>
								<figcaption class="name">
									<a href="<?php echo get_the_permalink(); ?>">
										<?php echo get_the_title(); ?>
									</a>
								</figcaption>
							</figure>
							<p class="full-price">

							<?php 

							$n = 1;

							while($n < 7){

								$parcela = $product->get_price() / $n;

								if( $product->get_price() <= 40 ){

									$final = 'em até 1x de R$ '.number_format($product->get_price(),2,',','.');

								}else{

									if( $parcela <= 40 ){
										echo '';
									}else{

										$final = 'em até '.$n.'x de R$ '.str_replace('.',',',number_format($parcela,2));

									}

								}

							$n++;

							}					
							
							?>


								R$ <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
								<!--<span class="installments">em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span>-->
								<span class="installments"><?php echo $final; ?></span>
							</p>
							<p class="discount-price">
								<?php

								//echo "<pre>";
								//print_r($product);
								//echo "</pre>";
								//die;

								$price = $product->price - ($product->price * ($percent / 100));
								$price = number_format($price, 2, ',', '.');
								?>
								<?php echo $price; ?>
							</p>
							<small>no boleto ou depósito</small>
						</div>
						<?php
						endwhile;
						endif;
						//echo $conta."<br>";
						$conta++;
						if($conta == 12+3){
							//die();
							$conta = 1;
						}
				endforeach;
			?>
		</div>

	</main>
</div>
<script>
function processaClassificacao(valor){
	alert(valor);
}

</script>
<?php 
	get_footer();
?>