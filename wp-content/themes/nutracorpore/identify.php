<?php include_once('header.php') ?>
<main>
	<section class="identification">
		<section class="promotional">
			<div class="center-content">
				<img src="https://placeholdit.imgix.net/~text?txtsize=50&txt=1200%C3%97200&w=1200&h=200" alt="" class="banner">
			</div>
		</section>
		<div class="center-content">	
			<h1 class="lined">IDENTIFICAÇÃO</h1>
			<div class="padded">
				<article class="identify fieldbox shaded-box">
					<h2>JÁ TENHO CADASTRO</h2>
					<form>	
						<fieldset>
							<legend class="field-descriptor">Se você já possui uma conta, informe os dados de acesso.</legend>
							<label>
								<span class="field-descriptor">
									Digite seu e-mail*
								</span>
								<input type="email" name="email" required class="field">
							</label>
							<label>
								<span class="field-descriptor">
									Senha*
								</span>
								<input type="password" name="password" required class="field">
							</label>
							<a href="">
								<i class="fa fa-lock" aria-hidden="true"></i>
								<span class="field-descriptor">Esqueceu a senha?</span>
							</a>
							<button class="generic-blue">ENTRAR</button>
						</fieldset>
					</form>
				</article>
				<article class="identify fieldbox shaded-box">
					<h2><img src="" aria-hidden="true">ESTA É MINHA PRIMEIRA COMPRA</h2>
					<legend class="field-descriptor">Cadastre-se agora mesmo em nossa loja, é fácil, rápido e seguro.</legend>
					<fieldset>
						<label>
							<span class="field-descriptor">Digite o email que deseja cadastrar</span>
							<input type="email" name="email" class="field">
						</label>
						<button class="generic-blue">CADASTRAR</button>
					</fieldset>
				</article>
			</div>
		</div>
	</section>
</main>
<?php include_once('footer.php') ?>