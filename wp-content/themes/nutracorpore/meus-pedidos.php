<?php
if (! is_user_logged_in()) {
	header('Location: '.get_bloginfo('url').'/login');
}
get_header();
?>
<div class="center-content">
	<?php include 'promotional.php'; ?>
	<h1 class="lined">MINHA CONTA</h1>
	<div class="padded">
		<?php include_once('account-sidebar.php'); ?>
		<main class="account shaded-box">
			<article class="description-segment">	
				<h2 class="full-lined red">TODOS OS PEDIDOS</h2>
				<table class="full-order-listing">
					<tr>
						<th>No. Pedido</th>
						<th>Data do pedido</th>
						<th>Valor total</th>
						<th>F. Pagamento</th>
						<th>Status</th>
						<th>Links</th>
					</tr>
					<tr>
						<td>17757</td>
						<td>02/12/2016 17:21:46</td>
						<td>R$ 175,07</td>
						<td>Boleto</td>
						<td>Cancelado pelo lojista</td>
						<td><a href="">Ver Detalhes</a></td>
					</tr>
					<tr>
						<td>17757</td>
						<td>02/12/2016 17:21:46</td>
						<td>R$ 175,07</td>
						<td>Boleto</td>
						<td>Cancelado pelo lojista</td>
						<td><a href="">Ver Detalhes</a></td>
					</tr>
					<tr>
						<td>17757</td>
						<td>02/12/2016 17:21:46</td>
						<td>R$ 175,07</td>
						<td>Boleto</td>
						<td>Cancelado pelo lojista</td>
						<td><a href="">Ver Detalhes</a></td>
					</tr>
				</table>
			</article> 
		</main>
	</div>
</div>
<?php 
get_footer();
?>