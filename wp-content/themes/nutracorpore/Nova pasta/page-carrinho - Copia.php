<?php 
get_header();
$payment_options   = get_option('woocommerce_cielo_credit_settings');
$payment_discounts = get_option('woocommerce_payment_discounts');
?>
<main>
	<section class="purchases">	
		<div class="center-content">	
			<div class="safe">
				<h1 class="lined bigger">CARRINHO</h1>
				<img src="<?php echo get_bloginfo('template_url'); ?>/images/common/safe-enviroment.png" alt="Ambiente 100% seguro - Comodo SSL" class="safe-certificate">
			</div>
			<div class="cart-details padded">
				<div class="entries">
					<?php
						//get_cart_discount_total
					global $woocommerce;
					$items = $woocommerce->cart->get_cart();
					$woocommerce->cart->calculate_totals();
					    
					$installments_value = $woocommerce->cart->cart_contents_total / $payment_options['installments'];
					$percent 			= str_replace('%', '', $payment_discounts['boleto']);
					$discount_value 	= $woocommerce->cart->cart_contents_total - ($woocommerce->cart->cart_contents_total * ($percent / 100));

					if($items){
						foreach($items as $item => $values) {
							$_product = $values['data']->post;
							$price = get_post_meta($values['product_id'] , '_price', true);
							?>
							<div class="entry shaded-box">
								<div class="picture">
									<a href="<?php echo $values['data']->post->guid; ?>">
										<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $values['product_id'], 'thumbnail' ) ); ?>" alt="<?php echo $values['data']->post->post_title; ?>">
									</a>
								</div>
								<div class="product-info">
									<h1>
										<a href="<?php echo $values['data']->post->guid; ?>">
											<?php echo $values['data']->post->post_title; ?>
										</a>
									</h1>
									<?php
										$sql  = 'SELECT * FROM wp_posts ';
										$sql .= 'WHERE ID = '.get_post_meta($values['product_id'], 'marca', true);
										$brand = $wpdb->get_results($sql);
									?>
									<h2 class="brand"><?php echo $brand[0]->post_title; ?></h2>
									<button class="remove" data-id="<?php echo $item; ?>">✕</button>
								</div>
								<div class="price-quantity">
									<p>R$ <span><?php echo number_format($values['line_subtotal'], 2, ',', '.'); ?></span>/un</p>	
									<button type="button" class="less numeral">-</button>
									<?php
										$quantity = ($values['quantity'] < 10 && !strstr($values['quantity'], '0')) ? '0'.$values['quantity'] : $values['quantity'];
									?>
									<input type="number" name="quantity" data-id="<?php echo $item; ?>" value="<?php echo $quantity; ?>" disabled>
									<button type="button" class="plus numeral">+</button>
								</div>
							</div>
							<?php
						}
					} else {
						?>
						<h1 class="empty-cart">Sem itens no carrinho</h1>
						<?php
					}
					?>
				</div>
				<div class="purchase-details">
					<div class="shaded-box order">
						<p class="order-details">DETALHES DO PEDIDO</p>
						<p class="order-price">Total<span><?php echo $woocommerce->cart->get_cart_total();?></span></p>
						<?php
						if ($items) {
							?>
							<p class="order-installments">em até <b><?php echo $payment_options['installments'];?>x</b> de <b>R$ 
								<?php echo number_format($installments_value, 2, ',', '.'); ?></b> S/ juros OU à vista com <b class="order-discount red"><?php echo $payment_discounts['boleto']; ?> OFF</b></p>
								<p class="order-discount-price">por <strong>R$ <?php echo number_format($discount_value, 2, ',', '.'); ?></strong></p>
								<?php
							}
							?>
						</div>
						<div class="input-box freight shaded-box lesser">
							<span>CALCULE O FRETE</span>
							<small class="zip-code-help">
								<a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">
									<span class="questionmark">?</span>Não sei<br> meu CEP
								</a>
							</small>
							<form action="" id="cart-shipping-calculate">
								<input type="hidden" name="action" value="shippingProduct">
								<input type="text" name="zipcode">
								<button>OK</button>
							</form>
						</div>
						<table class="freight-information shaded-box" id="shipping_totals">
							<tr>
								<th>Valor do Frete</th>
								<th>Disponibilidade</th>
							</tr>
							<!-- <tr>
								<td>R$ 25.74</td>
								<td>SEDEX para o CEP xxxxx-xxx em até xx dia(s).</td>
							</tr>
							<tr>
								<td>R$ 15.74</td>
								<td>PAC para o CEP xxxxx-xxx em até xx dia(s).</td>
							</tr>
							<tr>
								<td>R$ 14.28</td>
								<td>e-SEDEX para o CEP xxxxx-xxx em até xx dia(s).</td>
							</tr> -->
						</table>
						<div class="input-box coupon shaded-box lesser">
							<span>POSSUI CUPOM DE DESCONTO?</span>
							<form action="" id="coupon-calculate">
								<input type="text" name="coupon-number">
								<button type="submit">OK</button>
							</form>
						</div>
						<a href="<?php echo get_bloginfo('url'); ?>/login?checkout=checkout" class="finish-purchase shaded-box">FINALIZAR COMPRA</a>
						<a href="<?php echo get_bloginfo('url'); ?>" class="keep-purchasing shaded-box">
							<i class="fa fa-angle-double-left" aria-hidden="true"></i> CONTINUAR COMPRANDO
						</a>
					</div>
				</div>
			</div>
		</section>
	<!--
	<section class="gifts">
		<div class="center-content">
			<h1 class="lined">ESCOLHA SEU BRINDE<i class="fa fa-gift red" aria-hidden="true"></i></h1>
			<div class="padded giftbox">
				<button class="tab is-active" data-tab="0">VOCÊ GANHOU!</button>
				<button class="tab" data-tab="200">Por mais R$ 200,00</button>
				<button class="tab" data-tab="400">Por mais R$ 400,00</button>
				<div class="shaded-box">
					<div class="inner" data-tab="0">
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled="" selected="">--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled="" selected="">--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled selected>--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled="" selected="">--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
						<div class="gift">
							<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
							<h2 class="name">Whey Gold Optimum Nutrition</h2>
							<select name="bonus">
								<option disabled="" selected="">--Selecione--</option>
							</select>
							<button>QUERO ESTE</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
-->
</main>
<?php 
get_footer();
?>