<?php 
	get_header(); 
?>
<main>
	<?php 
		include 'promotional.php'; 
	?>
	
	<section class="generic-post">
		<div class="center-content">	
			<h1 class="title-rulers hidden">
			<span class="mobile-only">CONTEÚDO EXCLUSIVO</span>	
			<img src="<?php echo get_bloginfo('template_url');?>/images/common/exclusive2.jpg" alt="Conteúdo Exclusivo: Vídeos, Ensaios, Receitas, Artigos sobre suplementação, Musculação, saúde e muito mais" class="exclusive">
		</h1>

		<div class="articles-topic articles-topic-home">
			<?php 
			wp_nav_menu( array(
				'menu' => 'menu_categoria',
				'theme_location' => 'menu_categoria',
				'menu_class' => 'topics',
				'echo' => true,
				'depth' => 0,
				) );
			?>

		</div>

		<?php
			$sqlMaterias  = "
			(
			 SELECT
			  `terms`.`term_id` AS `term_id`,
			  `termrel`.`object_id` AS `object_id`,
			  'materias' AS `tipos`
			 FROM
			  (
			   `wp_terms` `terms`
			   JOIN `wp_term_relationships` `termrel` ON (
			    (
			     `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			    )
			   )
			  )
			 WHERE
			  (`terms`.`slug` = 'materias')
			 LIMIT 0,
			 3
			)
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'videos' AS `videos`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'videos')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'ensaios' AS `ensaios`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'ensaios')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'receitas' AS `receitas`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'receitas')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'eventos' AS `eventos`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'eventos')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'atletas' AS `atletas`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'atletas')
			  LIMIT 0,
			  3
			 )
			";
			
			$results = $wpdb->get_results($sqlMaterias);
			$materiaAtual = '';
			foreach ($results as $r) :
				$id = $r->object_id;
				if ( $materiaAtual != $r->tipos ) $i = 0;
				$title  = get_the_title($id);

				switch ($r->tipos) {
					case 'materias':

						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'materias';

						break;

					case 'videos':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'videos';
						break;

					case 'ensaios':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';
						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'ensaios';
						break;

					case 'receitas':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'receitas';
						break;

					case 'eventos':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'eventos';
						break;

					case 'atletas':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}
						
						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'atletas';
						break;

					default:
						# code...
						break;
				}
			endforeach; ?>
			<article class="padded shaded-box post">
				<span class="post-date has-icon calendar"><?php echo the_date('d/m/Y'); ?></span>
				<h1 class="red"><?php echo get_the_title(); ?></h1>
				<hr class="border" noshade>
				<h3 class="post-category">
					Publicado em:
					<?php 
					$categorias = get_the_category();

					foreach ($categorias as $categoria) {							
						echo '<strong>';
						echo $categoria->cat_name;
						echo '</strong> ';
					}
					?>
				</h3>
				<?php
				 
				if ( wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ) != '' && has_category('videos') != 1) { ?>
					<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>" alt="" class="post-image">
				<?php
				}
					the_content();

					$nomePasta = get_post_meta( get_the_ID(), '_ensaio', true );

					// Checa se tem galeria (old posts)
					if ( ! empty($nomePasta) ) {
						echo '<a href="javascript:void(0)" class="fancybox">Clique aqui para ver as fotos do ensaio</a>';
					}
				?>
					
			</article>
			<?php
			if ( ! empty($nomePasta) ) {

   
			   
			   $path = $_SERVER['SCRIPT_FILENAME'];
			   $path = str_replace('index.php', 'wp-content/uploads/ensaio/'.$nomePasta, $path);

			  // echo $path."<hr>";

			   $files = scandir($path);

				$ensaio = "";
				$fancybox_thumb = '
					<div id="fancybox-thumbs" class="bottom">
                        <ul style="width: 395px; left: 610px;">
                ';

				echo '
				<script type="text/javascript">
				 $(document).ready(function() {
				  $(".fancybox").click(function() {
				    $.fancybox.open([
		        ';
			    $i = 1;
			    foreach($files as $file) {
			    	if ( $i >= 3 ) {
						if($ensaio != ''){ $ensaio .= ","; }
						$ensaio .= "{ href : '".get_bloginfo('url')."/wp-content/uploads/ensaio/".$nomePasta.'/'.$file."', }";
    					$fancybox_thumb .= '<li class=""><a style="width:75px;height:50px;" href="javascript:jQuery.fancybox.jumpto('.($i-1).');"><img src="'.get_bloginfo('url')."/wp-content/uploads/ensaio/".$nomePasta.'/'.$file.'" style="width: 75px; height: 78px; top: -14px; left: 0px;"></a></li>';
			    	}
					$i++;
			    }
				echo $ensaio;
				echo '
				   ], {
				    helpers : {
					overlay: {
						opacity : 0.7,
						css : {"background-color" : "rgba(0,0,0,0.8)"},
						speedIn: 1500,
						speedOut: 1500
					},
				     thumbs : {
				      width: 75,
				      height: 50
				     }
				    }
				   });
				  });
				 }); 
				</script>';

				$fancybox_thumb .= '</ul></div>';
				echo $fancybox_thumb;
			}
			?>

			<div class="social-contact shaded-box padded">
				<span class="social-share-box">
					<p class="red"><b>Gostou? Compartilhe:</b></p>
					<div class="social-square facebook">
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink(); ?>" target="_blank">
							<i class="fa fa-facebook" aria-hidden="true"></i>
							<span class="sr-only">Facebook</span>
						</a>
					</div>
					<div class="social-square twitter">
						<a href="https://twitter.com/home?status=<?php echo '"'.get_the_title().'" - '.get_the_permalink();  ?>" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true"></i>
							<span class="sr-only">Twitter</span>
						</a>
					</div>
					<div class="social-square google">
						<a href="https://plus.google.com/share?url=<?php echo get_the_permalink(); ?>" target="_blank">
							<i class="fa fa-google-plus" aria-hidden="true"></i>
							<span class="sr-only">Google +</span>
						</a>
					</div>
					<div class="social-square pinterest">
						<a href="https://pinterest.com/pin/create/button/?url=<?php echo get_the_permalink(); ?>&media=<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>&description=" target="_blank">
							<i class="fa fa-pinterest-p" aria-hidden="true"></i>
							<span class="sr-only">Pinterest</span>
						</a>
					</div>
					<div class="social-square mail">
						<a href="mailto:exemplo@provador.com?Subject=<?php echo get_the_title(); ?>">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
							<span class="sr-only">E-mail</span>
						</a>
					</div>
				</span>
				<span class="see-category">
					<p class="average">
						Mais sobre: 
						<?php 
							$categorias = get_the_category();

							foreach ($categorias as $categoria) {							
								echo '<span>';
									echo '<a href="'.get_bloginfo('url').'/'.$categoria->slug.'"><strong>'.$categoria->cat_name.'</strong></a>';
								echo '</span>';
							}
						?>
					</p>
				</span>
			</div>
			<div class="padded">
				<div class="comment-snippet">	
					<p class="comments">COMENTE SOBRE ESTE ARTIGO</p>
					<div class="fb-comments" data-href="<?php echo get_bloginfo('url').'/'.$_SERVER['REQUEST_URI']; ?>" data-width="960" data-numposts="3"></div>
				</div>
			</div>
			<h1 class="lined has-icon newspapper">LEIA TAMBÉM <i class="fa fa-newspaper-o" style="color: red;margin-left:10px"></i></h1>
			<div class="padded">

				<?php 
					$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 4, 'post__not_in' => array($post->ID) ) );
					if( $related ) foreach( $related as $post ) {
					setup_postdata($post); ?>
					<article class="post-preview">
						<a href="<?php echo get_the_permalink(); ?>">	
							<div class="thumbnail-container">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="" class="post-thumbnail">
							</div>
							<div class="text-preview">	
								<span class="average"><?php echo get_the_date( 'd/m/Y' ); ?></span>
								<h2>
								<?php 
									$title  = the_title('','',false);
									if(strlen($title) > 65):
									    $title = trim(substr($title, 0, 70)).'...';

										if ( !in_array( substr($title, -1), array('–', '``' ) ) ) {
											$title = trim(substr($title, 0, 65)).'...';
										}

										echo $title;
									else:
									    echo $title;
									endif;
								?>
								</h2>
							</div>
						</a>
					</article>  
					<?php 
					}
					wp_reset_postdata(); 
					?>
			</div>
		</div>
	</section>
	
</main>
<?php 
get_footer(); 

?>

<style>
.fancybox-outer, .fancybox-inner {
	overflow: hidden;
}
.gallery{
    width: 800px;
    max-width: 90%;
    margin-top: 60px !important;
}
.slick-arrow{
    top: 30% !important;
}
.fancybox-inner {
    height: auto !important;
}
</style>