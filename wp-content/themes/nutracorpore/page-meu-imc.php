<?php 
	if (! is_user_logged_in()) {
		header('location: '.get_bloginfo('url').'/login');
	}
	get_header();

	global $woocommerce; 

?>
<div class="center-content">
	<?php 
		include 'promotional.php'; 
		global $woocommerce, $current_user;
	?>
	<h1 class="lined">MINHA CONTA</h1>
	<div class="padded">
		<?php include_once('account-sidebar.php'); ?>
		<main class="account shaded-box">	
			<section class="account-information">
				<h2 class="full-lined red">MEU IMC</h2>
				<form id="form-edit-account" class="edit-account" action="" method="post" id="formformulario">
					<?php include 'registro_imc.php'; ?>
					<?php $class_imc->cadastrar(); ?>
					<fieldset>
						<legend class="sr-only">'Edite aqui as informações da sua conta</legend>
						
						<label class="encapsule">
							<span></span>
							<input type="hidden" class="field" name="email" value="<?php echo $current_user->user_email; ?>">
						</label>
						
						<label class="encapsule">
							<span>Peso (kg):</span>
							<input type="text" class="field" name="peso" placeholder="Ex: 72.5" data-mask="00.0" value="" id="peso">
						</label>						
						<label class="encapsule">
							<span>Altura:</span>
							<input type="text" class="field" name="altura" placeholder="Ex: 1.98" data-mask="0.00" value="" id="altura">
						</label>
					</fieldset>
					<button type="submit" class="generic-blue" onclick="informar_imc()" name="cadastrar"><i class="fa fa-street-view" aria-hidden="true" id="cadastrar"></i> Cadastrar IMC</button>
				</form>
			</section>
		</main>
	</div>
</div>
<?php 
	get_footer(); 
?>

<script>

	function informar_imc(){

	var peso = document.getElementById("peso").value;
	var altura = document.getElementById("altura").value;
	var calculo_altura = altura * altura;
	var calculo_total = peso/calculo_altura;
	var geral = parseFloat(calculo_total.toFixed(1));

	swal("IMC", "Seu IMC é: "+ geral +" !!!")

}

</script>
-->