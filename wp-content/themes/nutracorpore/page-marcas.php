<?php get_header(); ?>

<?php
function glossario($letra) {
	global $wpdb;
	$conteudoBrands = '';

	$sql = "SELECT
				COUNT(post.post_title) AS total
			FROM
				wp_posts AS post
			WHERE
				post.post_type = 'marca'
			AND post.post_title LIKE '$letra%'
			ORDER BY
				post.post_title ASC";
	$results = $wpdb->get_results($sql);
	
	foreach ($results as $r) :
		$total = $r->total;
	endforeach;
	
	if ( $total > 0 ) {

		if ( strlen($letra) == 1 ) {
			$conteudoBrands .= '
			<li class="alphabetical has-icon angle">'.$letra.'</li>
			<ul>';

			$sql = "SELECT
						post.post_title,
						post.post_name
					FROM
						wp_posts AS post
					WHERE
						post.post_type = 'marca'
					AND post.post_title LIKE '$letra%'
					ORDER BY
						post.post_title ASC";
			$results = $wpdb->get_results($sql);

			foreach ($results as $r) :
				$conteudoBrands .= '<li><a href="'.get_bloginfo('url').'/categoria/'.$r->post_name.'">'.$r->post_title.'.</a></li>';
			endforeach;

		$conteudoBrands .= '</ul>';

		return $conteudoBrands;
		
		}
	}

	return $conteudoBrands;

	unset($total);
}

?>
<style>
li{list-style-type: none;}
</style>
<main>
	<section class="branding">
		<div class="center-content cleared">
			<h1 class="lined bigger">MARCAS</h1>
			<ul class="option-submenu brands five">
				<!--<?php
				$args = array(
					'post_type' => 'marca',
					'posts_per_page' => 12
					);
				$loop = new WP_Query( $args );
				$cnt = 0;
				if ( $loop->have_posts() ) :
					while ( $loop->have_posts() ) : $loop->the_post();
				if ($cnt == 0) :
					?>
				<li>
					<ul>
						<?php
						endif;
						$post_data = get_post(get_the_ID());
						$post_name = $post_data->post_name;
						?>
						<li>
							<a href="<?php echo get_bloginfo('url') ;?>/categoria/<?php echo $post_name; ?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>" alt="">
							</a>
						</li>
						<?php
						$cnt++;
						if ($cnt == 3) :
							$cnt = 0;
						?>
					</ul>
				</li>
				<?php
				endif;
				endwhile;
				endif;
				?>-->
                    <li>
                        <ul>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/nutraforce">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/nutra-corpore_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/new-millen">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/new-millen_clr-150x80.jpg" alt="">
                                </a>
                            </li>                           

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/probiotica">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/probiotica_clr-150x80.jpg" alt="">
                                </a>
                            </li>                                

                        </ul>
                    </li>

                    <li>    
                        <ul>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/integralmedica">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/integralmedica_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/atlhetica-nutrition">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/atlhetica-nutrition_clr.png" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/max-titanium">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/max-titanium_clr.png" alt="">
                                </a>
                            </li>                                

                        </ul>
                    </li>

                    <li>    
                        <ul>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/optimum-nutrition">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/optimum-nutrition_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/arnold-nutrition">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/arnold-nutrition_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/dymatize">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/dymatize_clr-150x80.jpg" alt="">
                                </a>
                            </li>                                

                        </ul>
                    </li>

                    <li>    
                        <ul>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/gat">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/gat_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/muscletech">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/muscletech_clr.png" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/mp-muscle-pharma">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/mp-muscle-pharma_clr-150x80.jpg" alt="">
                                </a>
                            </li>                                

                        </ul>
                    </li>

				







			</ul>
			<p class="full-brand-title">TODAS AS MARCAS</p>
			<div class="full-brand-listing cleared" style="font-size: 10px">
			<?php
				$brandsConteudoArr = array();

				for ($char = 'A'; $char <= 'Z'; $char++) {
						$qtd = glossario($char);

					if ( strlen($qtd) != 5 ) {
						$brandsConteudoArr[] = glossario($char);
					}
				}

				$contentBrandsFinal = array_filter($brandsConteudoArr, function($value) { return $value !== ''; });

				
				$j = 1;
				$numberOfBrands = 0;
				foreach ( $contentBrandsFinal as $value ) {
					$contentBrandsFinalArr[] = $value;
					$numberOfBrands++;
				}

				$divided = intval(round($numberOfBrands / 5));

				//print_r($contentBrandsFinal);

				$conta = 1;
				for ( $i=0; $i < $numberOfBrands; $i++ ) {
					
					if ( $conta == 1 ) {
						echo '<div class="brand-instances">';
					}
			
					if ( $conta <= $divided ) {
						echo $contentBrandsFinalArr[$i];
						$conta++;
					} else {
						$conta = 1;
					}

					if ( $conta == 1 ) {
						echo '</div>';
						$conta = 1;
					}
				}
			?>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>