<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

	get_header();

	include 'promotional-category.php';
?>

<div class="center-content cleared">
	<?php echo get_sidebar(); ?>
	
	<?php
	$sql = "SELECT
				term.term_id,
				term.`name`,
				term.slug,
				term.term_group,
				termtax.term_taxonomy_id,
				termrel.object_id
			FROM
				wp_terms AS term
			INNER JOIN wp_term_taxonomy AS termtax ON term.term_id = termtax.term_id
			INNER JOIN wp_term_relationships AS termrel ON termtax.term_taxonomy_id = termrel.term_taxonomy_id
			WHERE
				term.slug = '$category_name'";
	
	//echo "sql:<br> $sql<br>";
	//echo "<hr>";
	
	$results = $wpdb->get_results($sql);	
	
	$qtd_produtos = $wp_query->found_posts;

	?>

	<main class="category">
		<div class="pagination">
			<small class="index display-ib vertical-middle"><?php echo $qtd_produtos;?> Itens encontrados</small><!--
			--><nav class="woocommerce-pagination display-ib vertical-middle">
				<?php					
					echo 'Página: '.paginate_links( apply_filters( 'woocommerce_pagination_args', array(
						'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
						'show_all'     => false,
						'format'       => '',
						'add_args'     => false,
						'current'      => max( 1, get_query_var( 'paged' ) ),
						'total'        => $wp_query->max_num_pages,
						'prev_text'          => __('<span class="display-ib"><div class="small-arrow left"></div></span>'),
						'next_text'          => __('<span class="display-ib"><div class="small-arrow right"></div></span>'),
						'end_size'     => 0,
						'mid_size'     => 1,
					) ) );
				?>
			</nav><!--
			--><div class="sorting display-ib vertical-middle">
				
			</div>
		</div>
		<div class="grid-products greyed four">
			<?php
				$payment_options   = get_option('woocommerce_cielo_credit_settings');
				$payment_discounts = get_option('woocommerce_payment_discounts');
				$percent 		   = str_replace('%', '', $payment_discounts['boleto']);

				while ( have_posts() ) : the_post();
					$product = wc_get_product(get_the_ID());
					$installments = number_format(($product->get_price() / $payment_options['installments']), 2, ',', '.');

				?>			
						
						<div class="product">
							<figure>
								<?php 
								if (get_field('frete_gratis')) { 
									?>
									<span class="generic-stripe orange">FRETE GRÁTIS</span>
									<?php
								}
								?>
								<?php 
								if (get_field('lancamento')) { 
									?>
									<span class="generic-stripe green">LANÇAMENTO</span>
									<?php
								}
								?>
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="">
								<div class="overlay">
									<a href="<?php echo get_the_permalink(); ?>" class="product-detail-page 
									<?php echo ( $product->is_in_stock() && $product->product_type != 'variable' ) ? '' : 'no-add';?>">
										<i class="fa fa-search" aria-hidden="true"></i>
									</a>
									<?php
									if ( $product->is_in_stock() && $product->product_type != 'variable' ) :
									?>
									<button class="add-cart" data-id="<?php echo get_the_ID();?>">
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>
									</button>
									<?php
									endif;

									if(is_array($percent)) {
										$percent = $percent['amount'];
									}
									
									$price = $product->price;
									
									?>
								</div>
								<figcaption class="name">
									<a href="<?php echo get_the_permalink(); ?>">
										<?php echo get_the_title(); ?>
									</a>
								</figcaption>
							</figure>


							<?php 

							$n = 1;

							while($n < 7){

								$parcela = $price / $n;

								if( $totalzinho = number_format($price, 2, ',','.') <= 40  ){

									$final = '1x de <span class="installment-price"> R$ '.$price;

								}else{

									if( $parcela <= 40 ){
										echo "";
									}else{

										$final = $n.'x de <span class="installment-price"> R$ '.str_replace('.',',',number_format($parcela,2));

									}

								}

								$n++;

							}

							?>

							<p class="full-price">
								R$ <?php echo number_format($price, 2, ',', '.'); ?>
								<span class="installments">em até <?php echo $final; ?></span>
							</p>

							<!--<p class="full-price">
								R$ <?php echo number_format($price, 2, ',', '.'); ?>
								<span class="installments">em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span>
							</p>-->

							<p class="discount-price">
								<?php
								$price = $price - ($price * ($percent / 100));
								$price = number_format($price, 2, ',', '.');
								?>
								<?php echo $price; ?>
							</p>
							<small>no boleto ou depósito</small>
						</div>

					<?php
					
				endwhile;
			?>
		</div>
		<div class="pagination">
			<small class="index display-ib vertical-middle" style="float: left;width: 45%;"><?php echo $qtd_produtos;?> Itens encontrados</small><!--
			--><nav class="woocommerce-pagination display-ib vertical-middle" style="float: left;text-align: left;">
				<?php					
					echo 'Página: '.paginate_links( apply_filters( 'woocommerce_pagination_args', array(
						'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
						'show_all'     => false,
						'format'       => '',
						'add_args'     => false,
						'current'      => max( 1, get_query_var( 'paged' ) ),
						'total'        => $wp_query->max_num_pages,
						'next_text'    => '<span class="display-ib"><div class="small-arrow"></div></span>',
						'end_size'     => 0,
						'mid_size'     => 1,
					) ) );
				?>
			</nav>
		</div>
	</main>
</div>
<script>
   $(document).ready(function(){
    
    if(screen.width < 998){ 
        // Começo
        $("#secondary").css( "width", "100%" );
        // Fim
    }else if(screen.width > 998){ 
        // Começo
        $("#secondary").css( "width", "17%" );
        // Fim
    }    
});
</script>
<?php 
	get_footer();
