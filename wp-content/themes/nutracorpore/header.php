<!DOCTYPE html>
<!--[if IE]><html lang="pt-br" class="lt-ie9 lt-ie8"><![endif]-->
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title><?php echo get_bloginfo('site_name');?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="format-detection" content="telephone=no">
	<meta name="description" content="<?php echo get_bloginfo('site_description');?>">
	<meta name="language" content="pt-br">
	<meta name="author" content="Agência Feeshop">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url" content="">
	<meta property="og:title" content="">
	<meta property="og:site_name" content="">
	<meta property="og:description" content="">
	<meta property="og:image" content="">
	<meta property="og:image:type" content="">
	<meta property="og:image:width" content=""> 
	<meta property="og:image:height" content=""> 
	<meta property="og:type" content="website">
	<meta name="language" content="pt-br" />
	<link href="//fonts.googleapis.com/css?family=Asap:400,400i,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url');?>/src/main.css" type="text/css">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_bloginfo('template_url');?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_url');?>/images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_url');?>/images/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo get_bloginfo('template_url');?>/images/favicon/manifest.json">
	<link rel="mask-icon" href="<?php echo get_bloginfo('template_url');?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url');?>/images/favicon/favicon.ico">
	<link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/0.4.2/sweet-alert.min.css" />
	<meta name="msapplication-config" content="<?php echo get_bloginfo('template_url');?>/images/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_url');?>/dist/css/swiper.min.css" type="text/css">
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=1766277083584613";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php
global $current_user;
if (isset($_GET['logout'])) {
	setcookie('address-checkout', '');
	wp_logout();
	//header('location: '.get_bloginfo('url'));
}

	// $info = array();
	// $info['user_login'] = 'gdgfd';
	// $info['user_password'] = '123456';
	// $info['remember'] = true;

	// $user_signon = wp_signon( $info, false );
	// echo isset($user_signon->errors);
	// print_r(json_encode($user_signon));
	// die();
?>
<!--
http://localhost/nutracorpore/wp-content/themes/nutracorpore/src/main.cs
<br><br>
<body>
<?php  echo get_bloginfo('url').'/wp-content/themes/nutracorpore/src/main.cs'; ?>-->
	<div class="oldiewarning" aria-hidden="true" hidden="true">
		<a href="//www.google.com.br/chrome/browser/desktop/" aria-hidden="true">
			POR FAVOR, CLIQUE AQUI E ATUALIZE SEU NAVEGADOR PARA ACESSAR.
		</a>
	</div>
	<div class="loader-overlay">
		<div class="loader">
			<span class="loader-inner"></span>
		</div>
	</div>

<style>

.bgMatricula{
    position: fixed;
    width: 100vw;
    height: 100vh;
    opacity: 0.9;
    background-color: #002c4b;
    z-index: 9999;
    
}

.popMatricula{
    position: absolute;
    top: 50%;
    display: block;
    left: 0;
    right: 0;
    margin: auto;
    transform: translateY(-50%);
    background: url(https://nutracorpore.com.br/wp-content/themes/nutracorpore/images/PopUp-Boas-Vindas.png) no-repeat center center / contain;
    width: 572px;
    height: 610px;
    z-index: 9999;
    cursor: pointer;
    opacity: 9;
}

.popMatricula span{
    position: relative;
    float: right;
    font-size: 28px;
    right: 14px;
    color: #fff;
    top: 14px;
    cursor: pointer;
}

</style>

	<?php 

	    if($_COOKIE["exibe-pop-up"] != 'não'){

	?>


		<section class="bgMatricula" style="display:none">
		    <section class="popMatricula">
		        <span></span>
		    </section>
		</section>


	<?php

	       //setcookie("exibe-pop-up","não",(time() + (30 * 24 * 3600)));

	    }

	    $_COOKIE["exibe-pop-up"];

	?>


	<div class="adapt-menu-fox">
		<header>
			<nav class="mobile">
				<ul class="upper">
					<li class="go-back"><i class="fa fa-angle-left" aria-hidden="true"></i><button class="return">VOLTAR</button></li>
					<li><a href="<?php echo get_bloginfo('url'); ?>">HOME</a></li>
					<li>
						<a>PRODUTOS<i aria-hidden="true" class="fa fa-chevron-right"></i></a>
						<ul class="submenu">
							<li><a href="<?php echo get_bloginfo('url')."/categoria/massa-muscular/"; ?>">MASSA MUSCULAR</a></li>
							<li><a href="<?php echo get_bloginfo('url')."/categoria/energia-e-resistencia/"; ?>">ENERGIA E RESISTÊNCIA</a></li>
							<li><a href="<?php echo get_bloginfo('url')."/marcas/"; ?>">MARCAS</a></li>
							<li><a href="<?php echo get_bloginfo('url')."/categoria/definicao-muscular/"; ?>">DEFINIÇÃO MUSCULAR</a></li>
							<li><a href="<?php echo get_bloginfo('url')."/categoria/perder-gordura/"; ?>">EMAGRECIMENTO</a></li>							
						</ul>
					</li>
					<li><a href="">AJUDA E SUPORTE <i aria-hidden="true" class="fa fa-chevron-right"></i></a></li>
					<li>
						<a>INSTITUCIONAL <i aria-hidden="true" class="fa fa-chevron-right"></i></a>
						<ul class="submenu">
							<li><a href="<?php echo get_bloginfo('url'); ?>/quem-somos">QUEM SOMOS</a></li>
							<li><a href="<?php echo get_bloginfo('url'); ?>/cadastro">CADASTRE-SE</a></li>
							<li><a href="<?php echo get_bloginfo('url'); ?>/fale-conosco">FALE CONOSCO</a></li>
						</ul>
					</li>
					<li><a href="<?php echo get_bloginfo('url')."/minha-conta/"; ?>">MINHA CONTA <i aria-hidden="true" class="fa fa-chevron-right"></i></a></li>
					<li>
						<a>ARTIGOS <i aria-hidden="true" class="fa fa-chevron-right"></i></a>
						<ul class="submenu">
							<?php 
							wp_nav_menu( array(
								'menu' => 'menu_categoria',
								'theme_location' => 'menu_categoria',
								'menu_class' => 'mobile-topics',
								'echo' => true,
								'depth' => 0,
								) );
							?>
						</ul>
					</li>
				</ul>
				<div class="contact-information">
					<div>	
						<p>SAC</p>
						<span>11 3683.0306</span>
						<small>seg à sex - 10h às 18h</small>
					</div>
					<div>
						<p>WHATSAPP</p>
						<span>11 97225.3257</span>
					</div>
				</div>	
			</nav>
			<nav>
				<div class="center-content">
					<button class="hamburger" aria-role="Menu" aria-controls="Navigation">
						<div class="inner">	
							<span></span>
							<span></span>
							<span></span>
						</div>
					</button>
					<div class="information-and-login">
						<div class="login">
							<p style="display: block;"><i class="fa fa-user"></i>&nbsp;<span>Bem-Vindo 
								<?php echo (is_user_logged_in()) ? get_user_meta($current_user->ID, 'first_name', true) : ''; ?> | </span>
								<?php
								if (! is_user_logged_in()) :
									?>
								<a href="<?php echo get_bloginfo('url');?>/login">ENTRAR  </a>ou<a href="<?php echo get_bloginfo('url');?>/cadastro" class="red">  CADASTRE-SE</a></p>
								<?php
								else :
									?>
								<a href="<?php echo get_bloginfo('url');?>/?logout=sair">SAIR</a>
								<?php
								endif;
								?>
							</div>
							<div class="search">
								<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
									<input type="text" name="s" placeholder="O que você está procurando?" autocomplete="off">
									<button type="submit"><i class="fa fa-search"></i></button>
								</form>

								<div id="sugestao">
									<h3>
										Sugestões de produtos
										<span id="close-sugestao">X</span>
									</h3>
									<div id="resultSugestao" style="background: #fff">
										
									</div>
								</div>
								
							</div>
						</div>
						<a href="<?php echo get_bloginfo('url'); ?>">
							<div class="logo">	
							</div>
						</a>
						<div class="orders-account-cart">	
							<ul>
								<li>
									<a href="<?php echo get_bloginfo('url'); ?>/meus-pedidos">
										<i class="fa fa-bars" aria-hidden="true"></i>
										MEUS PEDIDOS
										&nbsp; 
									</a>
									|
								</li>
								<li>
									<a href="<?php echo get_bloginfo('url'); ?>/minha-conta">
										&nbsp;
										<i class="fa fa-user"></i>
										MINHA CONTA 
										&nbsp;
									</a>
									|
								</li>
								<li>
									&nbsp;
									REDES SOCIAIS:
									&nbsp;
									<a href="https://www.facebook.com/nutracorpore" target="_blank">
										<i class="fa fa-facebook"></i>
									</a>
									<a href="https://www.youtube.com/user/nutracorpore" target="_blank">
										<i class="fa fa-youtube"></i>
									</a>
									|
								</li>
							</ul>
							<div class="cart">
								<i class="fa fa-shopping-cart" aria-hidden="true"></i>
								<?php
								global $woocommerce;
								$items = $woocommerce->cart->get_cart();
								$woocommerce->cart->calculate_totals();
								?>
								<small><?php echo count($items); ?></small>

								<div class="carrinhoBox">
									<ul class="entries">
										<?php
										if(count($items) > 0){
											$frete_produto = array();
											foreach($items as $item => $values) {
												$frete_produto[] = $values['product_id'];
												$_product = $values['data']->post;
												$price = get_post_meta($values['product_id'] , '_price', true);

												echo '
												<li>
													<a href="'.get_bloginfo('url').'/carrinho/" class="caixa" style="display: block;">
														<img src="'.wp_get_attachment_url( get_post_thumbnail_id( $values['product_id'], 'thumbnail' ) ).'" class="imagem display-ib" />
														<span class="titulo display-ib">'.$values['data']->post->post_title.'</span>
														<span class="preco display-ib">R$ '.number_format($price, 2, ',', '.').'</span>
													</a>
												</li>';
												
											}

											echo '
											<div class="overall">
												<p>
													<span>'.count($items).' produto(s) no carrinho.</span><br>
													<span>Total: '.$woocommerce->cart->get_cart_total().'</span>
												</p>
												<a href="'.get_bloginfo('url').'/carrinho/">
													IR PARA O CARRINHO
												</a>
											</div>';

										} else {
											echo '<p style="font-size: 14px;background-color: white;padding: 20px;text-align: center;color: black;">Carrinho Vazio</p>';
										}
										?>
									</ul>
								</div>
							</div>
							<div class="customer-support">
								<p>CENTRAL DE ATENDIMENTO</p>
								<!-- <img src="images/icons/talk-balloon-small.svg" aria-hidden="true"> -->
								<i class="fa fa-comments-o" aria-hidden="true"></i>
								<ul class="contact-widget white-box" tabindex="-1">
									<li>
										<h1>TELEFONE</h1>
										<h2>11 3683.0306</h2>
										<small>seg à sex - 10h às 18h</small>
									</li>
									<li>
										<h1>WHATSAPP</h1>
										<h2>11 97225.3257</h2>
									</li>
									<li>
										<h1>E-MAIL</h1>
										<h2>contato@nutracorpore.com.br</h2>
									</li>
									<li>
										<span>Dúvidas ?<a href="<?php echo get_bloginfo('url'); ?>/fale-conosco">FALE CONOSCO</a></span>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</nav>
			</header>
		</div>

		<div class="social-fixed">
			<div class="facebook">
				<a href="https://www.facebook.com/nutracorpore" target="_blank">
					<i class="fa fa-facebook"></i>
				</a>
			</div>
			<div class="youtube">
				<a href="https://www.youtube.com/user/nutracorpore" target="_blank">
					<i class="fa fa-youtube"></i>
				</a>
			</div>
			<div class="instagram">
				<a href="https://www.instagram.com/nutracorpore/" target="_blank">
					<i class="fa fa-instagram"></i>
				</a>
			</div>
			<span id="showSocial"></span>
		</div>
		

		<script type="text/javascript">
			

		$(document).ready(function(){

			$(".popMatricula").click(function(){

				$(".bgMatricula").hide();

			})


		})



		</script>