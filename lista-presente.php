<?php
	include("include/inc_conexao.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Lista de Presente</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="description" content="<?php echo $site_nome;?> Meus Pedidos. Acompanhe o status e histórico de suas compras." />
<meta name="keywords" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Meus Pedidos" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/lista-presente.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#lista_data_evento").mask("99/99/9999");
		$("#lista_titulo").focus();
    });	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">

    <div id="menu-conta-left">
        <?php include("inc_menu_mc.php");?>
    </div>
    
    <div id="box-meio-minha-conta">
    	<div id="box-meus-dados">
        	<h2 class="h2-pg-meus-pedidos">Lista de Presentes</h2>
            <h4 class="h4-minha-conta">Lista de Presentes</h4>
            <div id="relacao-pedidos">
                <div id="titulos-relacao">
                    <span class="numero-da-lista">Lista</span>
                    <span class="data-do-evento">Data do Evento</span>
                    <span class="titulo-lista">Presenteados</span>
                    <span class="links-lista">Link da Lista</span>
    	    </div>
                <div id="itens-relacao">
					
                    <?php
					
					if($_REQUEST['action']=='buscar'){
					
						$data_evento = addslashes($_REQUEST["lista_data_evento"]);
						if( trim($data_evento)!="" ){
							$data_evento = formata_data_db($data_evento);	
						}
						
						$titulo	= addslashes($_REQUEST["lista_titulo"]);
						$nome	= addslashes($_REQUEST["lista_nome"]);
						
					
                    	$ssql = "select listaid, ltitulo, lnome, llink_seo, ldata_evento
								from tbllista_presente
								where ltitulo like '%{$titulo}%' and lnome like '%{$nome}%' ";		
						
						if($data_evento){
							$ssql .= " and ldata_evento = '{$data_evento} 00:00:00' ";
						}
						
						//echo $ssql;
						
						$result = mysql_query($ssql);
						if($result){
							
							if(mysql_num_rows($result)==0){
								echo "Nenhuma lista localizada.";	
							}
							
							while($row=mysql_fetch_assoc($result)){
								
								echo '
								<div class="lista-item">
									<span class="numero-da-lista">'.$row["ltitulo"].'</span>
									<span class="data-do-evento">'.formata_data_tela(left($row["ldata_evento"],10)).'</span>
									<span class="titulo-lista">'.$row["lnome"].'</span>
									<span class="links-lista"><a href="'.$row["llink_seo"].'">Ver Lista</a></span>
								</div>							
								';
								
							}
							mysql_free_result($result);
						}
						
						
						if($codigo==0){
							$codigo = "";	
						}
						
					}
					
					?>
                    


              </div>
            </div>
            <form name="frm_pedidos" id="frm_pedidos" method="get" action="lista-presente.php" >
			<div class="box-minha-lista"></div>
            <div class="box-minha-lista"><span class="dados-minha-lista">Nome da Lista:</span> <input type="text" id="lista_titulo" name="lista_titulo" class="filtro-minha-lista" value="<?php echo $titulo;?>" maxlength="100"/></div>
            <div class="box-minha-lista"><span class="dados-minha-lista">Nome dos Presenteados:</span> <input name="lista_nome" type="text" class="filtro-minha-lista" id="lista_nome" value="<?php echo $nome;?>" maxlength="100"/></div>
            <div class="box-minha-lista"><span class="dados-minha-lista">Data do Evento:</span> <input name="lista_data_evento" type="text" class="filtro-meus-pedidos" id="lista_data_evento" value="<?php echo $data_evento;?>" />
            </div>
            
            <div class="box-btns-conta">
                <div id="box-btn-buscar">
                  <input type="image" name="cmd_buscar" id="cmd_buscar" src="images/btn-busca-meus-pedidos.png" />
                </div>
            </div>
            <input type="hidden" name="action" id="action" value="buscar" />
            </form>
            
        </div>
        
        
    </div>   
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
</body>
</html>