<?php
include("include/inc_conexao.php");
header("Content-type: text/xml");

$categorias = array('4'=>'Moda','5'=>'Vestidos','6'=>'Blusas','7'=>'Casacos','8'=>'Cal�as','9'=>'Saias & Shorts','10'=>'Lingerie','11'=>'Gym & Praia','12'=>'Sapatos','13'=>'Botas','14'=>'Sand�lias','15'=>'T�nis','17'=>'Acess�rios','18'=>'Bolsas','19'=>'Bijoux','20'=>'Beleza','21'=>'Maquiagem','22'=>'Cosm�ticos','23'=>'Perfumes','24'=>'Cabelos','25'=>'Casa & Decor','26'=>'Cama & Banho','27'=>'M�veis','28'=>'Cozinha & Mesa','29'=>'Lumin�rias & Decor','30'=>'Cult','31'=>'Livros','33'=>'CDs','34'=>'DVDs','36'=>'Tecnologia','40'=>'Viagem','41'=>'Presentes','42'=>'Papelaria','43'=>'Flores','44'=>'Gifts Criativos','45'=>'Chocolate','46'=>'Gourmet','48'=>'Aperitivos','49'=>'Adega','50'=>'Pet','51'=>'Baby');
$catPadrao = 4;

$ssql = "select tblproduto.produtoid, pcontrola_estoque, pdisponivel, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.psubtitulo,  tblproduto.pdescricao,  
			tblproduto.pdescricao_detalhada, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, tblproduto.plink_seo, 
			(tblproduto_midia.marquivo) as pimagem, tblmarca.mmarca
			from tblproduto 
			LEFT JOIN tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
			INNER JOIN tblproduto_midia on tblproduto_midia.mcodproduto=tblproduto.produtoid and tblproduto_midia.mprincipal=-1
			INNER JOIN tblmarca on tblproduto.pcodmarca = tblmarca.marcaid
			WHERE NOW() BETWEEN tblproduto.pdata_inicio AND tblproduto.pdata_termino
            GROUP by tblproduto.produtoid
			HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))
			ORDER BY tblmarca.mmarca
			";
$result = mysql_query($ssql);
if($result){
	
	echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<produtos>';

		while($row=mysql_fetch_assoc($result)){
			
			$link	=	"http://www.stylemarket.com.br/" . $row["plink_seo"];
			$imagem	=	"http://www.stylemarket.com.br/" . $row["pimagem"];
			$imagem =	str_replace("-tumb.","-big.",$imagem);
			
			$valor_comparativo = $row["pvalor_unitario"];
			
			if( $row["pvalor_comparativo"] > $row["pvalor_unitario"] ){
				$valor_comparativo = $row["pvalor_comparativo"];	
			}
			
			//lista as categorias
			$ssql1 = "select c.ccategoria 
						from tblproduto_categoria as pc 
						inner join tblcategoria as c on pc.pcodcategoria=c.categoriaid
						where pc.pcodproduto = '".$row["produtoid"]."'
						order by c.ccategoria";
			$result1 = mysql_query($ssql1);
			$cat_ids = $catPadrao;
			$anterior = '';
			if($result1){
				while($row1=mysql_fetch_assoc($result1)){
					$categoria = $row1["ccategoria"];
					foreach($categorias as $id => $nome){
						if(stripos($nome,$categoria) !== false){
							if($id != $anterior && $id != $catPadrao){
								$cat_ids .= ','.$id;
								$anterior = $id;
							}
						}
					}
				}
				mysql_free_result($result1);
			}

			$codigo     = str_replace('&','&amp;',$row["pcodigo"]);
			
			echo '<produto>';
			echo '<nome_titulo><![CDATA[' . $row["pproduto"] . ']]></nome_titulo>';
			echo '<codigo>' . $codigo . '</codigo>';
			echo '<link>' . $link . '</link>';
			echo '<imagem>' . $imagem . '</imagem>';
			echo '<descricao><![CDATA[' . $row["pdescricao"] . ']]></descricao>';
			echo '<categoria>'.$cat_ids.'</categoria>';
			echo '<preco_real>' . number_format( $valor_comparativo, 2, ',', '.') . '</preco_real>';
			echo '<preco_desconto>' . number_format($row["pvalor_unitario"], 2, ',', '.') . '</preco_desconto>';
			echo '<specific>
					<marca>'.$row["mmarca"].'</marca>
					<cor/>
					<tamanho/>
					<autor/>
					<artista/>
					<editora/>
					<ritmo/>
					<distribuidora/>
					<sinopse/>
					<loja>Style Market</loja>
				</specific>';
			echo '</produto>';
		}
		mysql_free_result($result);

	echo '</produtos>';

}
	
?>