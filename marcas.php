<?php
	include("include/inc_conexao.php");
	
	$letras = array("0-9","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","W","Y","Z");
	

/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("marcas.php","",$_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> | Marcas</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> | Marcas" />
<meta name="description" content="<?php echo $site_nome;?> | Marcas" />
<meta name="keywords" content="<?php echo $site_nome;?> | Marcas" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?> | Marcas" />
<meta name="author" content="Betasoft" />
<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/marcas.php" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
$(document).ready(function() {	
	
});	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
		<?php
        
			for($i = 0 ; $i < count($letras) ; $i++ ){
			
				$ssql = "select marcaid, UPPER(mmarca) as mmarca, mlink_seo 
						from tblmarca 
						inner join tblproduto on tblmarca.marcaid = tblproduto.pcodmarca";
					
					
				if($i==0){
					$ssql .= " where mmarca like '0%' or mmarca like '1%' or mmarca like '2%' or mmarca like '3%' or mmarca like '4%' or mmarca like '5%' ";
					$ssql .= " or mmarca like '6%' or mmarca like '7%' or mmarca like '8%' or mmarca like '9%' ";
				}
				else{
					$ssql .= " where mmarca like '".$letras[$i]."%' ";
				}
						
				$ssql .= " group by marcaid order by mmarca ";
				
				
				$result = mysql_query($ssql);
				if($result){
					if(mysql_num_rows($result)>0){
						echo '<div style="width: 240px; letter-spacing: 2px; line-height:150%;margin-left: 10px; float:left; height:150px; margin-top:20px;">';
						echo '<h3 style="font-size:18px; height:25px; color:#000">'.$letras[$i].'</h3>';
							while($row = mysql_fetch_assoc($result)){
								echo '<p><a href="marca/'.$row["mlink_seo"].'" style="text-decoration:none; color:#666; font-size:12px; font-weight:normal;">'.$row["mmarca"].'</a></p>';
							}
							mysql_free_result($result);						
						echo '</div>';
					}
				}
			}
		
		?>
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>