<?php
	include("include/inc_conexao.php");
	
	/*-------------------------------------------------------------------	
	//navegação com ssl
	---------------------------------------------------------------------*/
	$config_certificado_instalado = get_configuracao("config_certificado_instalado");
	if($config_certificado_instalado==-1){
		if(strpos($_SERVER['SERVER_NAME'],".com")>0){
			if($_SERVER['SERVER_PORT']==80){
				header("location: https://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
				exit();
			}
		}	
	}
	




	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=minha-conta.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=minha-conta.php");
		exit();
	}




	/*-------------------------------------------------------------
	carrega os dados
	-------------------------------------------------------------*/
	$cadastro = $_SESSION["cadastro"];
	$ssql = "select cadastroid, crazao_social, cnome, capelido, ccodtipo, ccpf_cnpj, crg_ie, cemail, csenha, csite, cdata_nascimento, ccodsexo, ctelefone, ccelular, cnews, csms
			from tblcadastro where cadastroid = '{$cadastro}'";
	$result = mysql_query($ssql);
	if($result){
		
		if(mysql_num_rows($result)==0){
			unset($_SESSION["cadastro"]);
			session_unset(); // destroi todas as sessoes abertas
			header("location: login.php");	
		}
		
		while($row=mysql_fetch_assoc($result)){
			$tipo			=	$row["ccodtipo"];
			$razaosocial 	=	$row["crazao_social"];
			$nome 			=	$row["cnome"];
			$apelido		=	$row["capelido"];
			$tipo			=	$row["ccodtipo"];
			$cnpj			=	formata_cpf_cnpj_tela($row["ccpf_cnpj"]);
			$ie				=	$row["crg_ie"];
			$email			=	$row["cemail"];
			$senha			=	$row["csenha"];
			$site			=	$row["csite"];
			$nascimento		=	formata_data_tela($row["cdata_nascimento"]);
			$sexo			=	$row["ccodsexo"]==1 ? "Marculino" : "Feminino";
			$telefone		=	formata_telefone_tela($row["ctelefone"]);
			$celular		=	formata_telefone_tela($row["ccelular"]);
			$news			=	($row["cnews"]==-1)?"SIM":"NÃO";
			$sms			=	($row["csms"]==-1)?"SIM":"NÃO";
			
			if($tipo==1){
				$tipo = "PF";
			}else{
				$tipo = "PJ";
			}
			
			
		}	
		mysql_free_result($result);
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Minha Conta</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Minha Conta" />
<meta name="description" content="<?php echo $site_nome;?> Minha Conta" />
<meta name="keywords" content="<?php echo $site_nome;?> Minha Conta" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Minha Conta" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/minha-conta.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		
    });
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
         <div id="menu-topo-conta">
    		<a href="minha-conta.php" id="meu-perfil">Minha Conta</a>
            <span style="float:left; padding:0; font-size:15px;">&nbsp;|&nbsp;</span>
            <a href="meus-pedidos.php" id="meus-pedidos">Meus Pedidos</a>
    	</div>
        
        <div id="menu-conta-left" class="top-margin-align">
            <?php
                include("inc_menu_mc.php");
            ?>
        </div>
    
        <div id="box-meio-minha-conta">
            <div id="box-meus-dados" class="box-margin-align">
                <h4 class="h4-minha-conta">Meus Dados</h4>
                <span class="dados-conta"><strong>Tipo:</strong> <?php echo $tipo;?></span>
                <span class="dados-conta"><strong>Nome Completo:</strong> <?php echo $razaosocial . " " . $nome;?></span>
                <span class="dados-conta"><strong>Apelido:</strong> <?php echo $apelido;?></span>
                <span class="dados-conta"><strong>CPF:</strong> <?php echo $cnpj;?></span>
                <span class="dados-conta"><strong>RG:</strong> <?php echo $ie;?></span>
                <span class="dados-conta"><strong>Email:</strong> <?php echo $email;?></span>
                <span class="dados-conta"><strong>Data de Nascimento:</strong> <?php echo $nascimento;?></span>
                <span class="dados-conta"><strong>Sexo:</strong> <?php echo $sexo;?></span>
                <span class="dados-conta"><strong>Telefone de Contato:</strong> <?php echo $telefone;?></span>
                <span class="dados-conta"><strong>Celular:</strong> <?php echo $celular;?></span>
                <!--//
                <span class="dados-conta"><strong>Página WEB:</strong> <?php echo $site;?></span>
                <span class="dados-conta"><strong>Pontos de Fidelidade:</strong> 000</span>
                //-->
                
                <span class="dados-conta"><strong>News Letter:</strong> <?php echo $news;?></span>
                <span class="dados-conta"><strong>SMS:</strong> <?php echo $sms;?></span>
                
                <div class="box-btns-conta">
                    <div id="btn-editar"><a href="cadastro-editar.php">Editar</a></div>
                    <div id="btn-alterar-senha"><a href="cadastro-senha.php">Alterar Senha</a></div>
                </div>
            </div>
            
            <div id="box-infos-conta" style="margin-bottom:50px;">
                <h4 class="h4-minha-conta">Informações da sua conta</h4>
                <span class="dados-conta"><strong>Endereços:</strong><br /> <a href="cadastro-endereco.php">Ir para catalogo de endereços</a></span>
                <span class="dados-conta"><strong>Senha:</strong><br /> <a href="cadastro-senha.php">Alterar Senha</a></span>
            </div>
            
  
        </div>  
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>