<?php
	include("include/inc_conexao.php");
	
	$pedido	=	addslashes($_REQUEST["pedido"]);
	$email	=	addslashes($_REQUEST["email"]);
	
	if(!is_numeric($pedido)){
		header("location: meus-pedidos.php");	
		exit();
	}
	
	$ssql = "select tblboleto.bagencia, tblboleto.bconta_corrente, tblboleto.bconta_corrente_dac, tblboleto.bcodpedido, tblboleto.bcodcadastro, tblboleto.bemail, tblboleto.bdata_vencimento, tblboleto.bdata_documento, tblboleto.bnumero_documento, tblboleto.bcarteira, tblboleto.bnosso_numero, 
	tblboleto.bnosso_numero_dac, tblboleto.bvalor_documento, tblboleto.binstrucao, tblboleto.breferencia_numerica, tblboleto.bcodigo_barras, tblboleto.btaxa, tblboleto.bstatus, 
	tblcadastro.crazao_social, tblcadastro.cnome, tblcadastro.ccpf_cnpj, tblcadastro.cemail
	from tblboleto 
	inner join tblcadastro on tblboleto.bcodcadastro = tblcadastro.cadastroid
	where tblboleto.bcodpedido='{$pedido}' and tblboleto.bemail='{$email}'";
	$result = mysql_query($ssql);
	
	if(!$result){
		echo "Erro ao carregar os dados do boleto";
		exit();
	}
	
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$taxa				=	$row["btaxa"];
			$data_vencimento	=	formata_data_tela($row["bdata_vencimento"]);
			$valor_documento	=	number_format($row["bvalor_documento"],2,",","");
			
			$cpf				=	formata_cpf_cnpj_tela($row["ccpf_cnpj"]);
			
			$nosso_numero		=	right($row["bnosso_numero"],7);
			$numero_documento	=	$row["bnumero_documento"];
			
			$data_documento		=	formata_data_tela($row["bdata_documento"]);
			
			$agencia			=	$row["bagencia"];
			$conta_corrente		=	$row["bconta_corrente"];
			$conta_corrente_dac	=	$row["bconta_corrente_dac"];
			$carteira			=	$row["bcarteira"];
			
			$nome				=	$row["crazao_social"].' '.$row["cnome"];
			
			$codigo_barras		=	$row["bcodigo_barras"];
			$referencia_numerica=	$row["breferencia_numerica"];
			
		}
		mysql_free_result($result);
	}
	
	
	
	//carrega variaveis e cedente
		$ssql = "select cedenteid, crazao_social, ccnpj, cbanco, cnumero_banco, cnumero_banco_dac, cagencia, cconta_corrente, cconta_corrente_dac, cnosso_numero, 
				cnumero_documento, ccarteira, cconvenio, ctaxa, cdias from tblcedente where cedenteid = 1";
		$result = mysql_query($ssql);		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$cedenteid			 		=	$row["cedenteid"];
				$cedente_razaosocial 		=	$row["crazao_social"];
				$cedente_cnpj		 		=	formata_cpf_cnpj_tela($row["ccnpj"]);
				$dias						=	$row["cdias"];
				$cedente_convenio			=	right("0000000".$row["cconvenio"],7);
			}
			mysql_free_result($result);
		}



// DADOS DO BOLETO PARA O SEU CLIENTE
$dias_de_prazo_para_pagamento = $dias;
$taxa_boleto = $taxa;
$data_venc = $data_vencimento; //date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006"; 
$valor_cobrado = $valor_documento; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
$valor_cobrado = str_replace(",", ".",$valor_cobrado);
$valor_boleto = number_format($valor_cobrado+$taxa_boleto, 2, ',', '');

$dadosboleto["nosso_numero"] = $nosso_numero;  // Nosso numero - REGRA: Máximo de 8 caracteres!
$dadosboleto["numero_documento"] = $numero_documento;	// Num do pedido ou nosso numero
$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
$dadosboleto["data_documento"] = $data_documento; // Data de emissão do Boleto
$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
$dadosboleto["valor_boleto"] = $valor_boleto; 	// Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula

// DADOS DO SEU CLIENTE
$dadosboleto["sacado"] = $nome;
$dadosboleto["endereco1"] = $email;
$dadosboleto["endereco2"] = "" ;//$cidade . ' - ' .$estado .  ' - CEP: ' . $cep;
$dadosboleto["cpf"] = $cpf;

// INFORMACOES PARA O CLIENTE
$dadosboleto["demonstrativo1"] = ("Pedido #".right($nosso_numero,7) ." efetuado na loja ") . $site_nome;
$dadosboleto["demonstrativo2"] = ("Em caso de dúvidas entre em contato conosco: " . $site_email);
if($taxa_boleto>0){
	$dadosboleto["demonstrativo3"] = ("Taxa bancária - R$ ").number_format($taxa_boleto, 2, ',', '');
}

$dadosboleto["instrucoes1"] = ("- Sr. Caixa, não cobrar multa");
$dadosboleto["instrucoes2"] = ("- Não receber após o vencimento");
$dadosboleto["instrucoes3"] = ("");
$dadosboleto["instrucoes4"] = "";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
$dadosboleto["quantidade"] = "";
$dadosboleto["valor_unitario"] = "";
$dadosboleto["aceite"] = "N";		
$dadosboleto["especie"] = "R$";
$dadosboleto["especie_doc"] = "DUP";


// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //


// DADOS PERSONALIZADOS - SANTANDER BANESPA
$dadosboleto["agencia"] = $agencia; // Num da agencia, sem digito
$dadosboleto["conta"] = $conta_corrente;	// Num da conta, sem digito
$dadosboleto["conta_dv"] = $conta_corrente_dac; 	// Digito do Num da conta

// DADOS PERSONALIZADOS 
$dadosboleto["carteira"] = $carteira;  // Código da Carteira: pode ser 102 CSR

$dadosboleto["codigo_cliente"] = $cedente_convenio; // Código do Cliente (PSK) (Somente 7 digitos)
$dadosboleto["ponto_venda"] = $agencia; // Ponto de Venda = Agencia
$dadosboleto["carteira"] = $carteira;  // Cobrança Simples - SEM Registro
$dadosboleto["carteira_descricao"] = "COBRANÇA SIMPLES - CSR";  // Descrição da Carteira

//CODIGO DE BARRAS E LINHA DIGITÁVEL
$dadosboleto["codigo_barras"] = $codigo_barras;
$dadosboleto["linha_digitavel"] = $referencia_numerica;



// SEUS DADOS
$dadosboleto["cedente"] = $cedente_razaosocial;
$dadosboleto["identificacao"] = "Boleto " . $site_nome;
$dadosboleto["cpf_cnpj"] = $cedente_cnpj;
$dadosboleto["endereco"] = "";
$dadosboleto["cidade_uf"] = "";

// NÃO ALTERAR!
include("include/inc_funcoes_santander.php"); 
include("include/inc_layout_santander.php");

if($_REQUEST["action"]=="print"){
	echo '<script>window.print();</script>';	
}

?>