<?php
	include("include/inc_conexao.php");
	include("include/inc_cadastro.php");
	
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie	
	$redir = $_REQUEST["redir"];
	
	if($redir==""){
		$redir = "index.php";	
	}
	

	/*-------------------------------------------------------------------	
	//navegação com ssl
	---------------------------------------------------------------------*/
	$config_certificado_instalado = get_configuracao("config_certificado_instalado");
	if($config_certificado_instalado==-1){
		if(strpos($_SERVER['SERVER_NAME'],".com")>0){
			if($_SERVER['SERVER_PORT']==80){
				header("location: https://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
				exit();
			}
		}	
	}


	

	if($_POST){
	
		if(isset($_POST["action"])){
		
			if($_POST["action"]=="dologin"){
				$email 	= addslashes($_REQUEST["txt-email"]);
				$senha	= addslashes($_REQUEST["txt-senha"]);
				$senha = base64_encode($senha);
				
				$ssql = "select cadastroid, capelido, cemail, csenha from tblcadastro where cemail='{$email}' and csenha='{$senha}'";
				
				$result = mysql_query($ssql);
				if($result){
					$num_rows = mysql_num_rows($result);
					while($row=mysql_fetch_assoc($result)){
						$apelido = $row["capelido"];	
						$_SESSION["cadastro"]=$row["cadastroid"];
						$_SESSION["cadastro_email"]=$row["cemail"];
						setcookie("apelido",$apelido,$expires);
						setcookie("cadastro",$row["cadastroid"],$expires);
					}
					mysql_free_result($result);
				}
				
				//echo $num_rows;
				
				//-------------------------------------------
				if($num_rows>0){
					header("location: " . $redir);
					exit();
				}
				else
				{
					$msg = "Login e senha incorretos.";	
				}
				
				
			}
		
		}
		
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Login de Acesso</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Login de Acesso" />
<meta name="description" content="<?php echo $site_nome;?> Login de Acesso" />
<meta name="keywords" content="<?php echo $site_nome;?> Login de Acesso" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Login de Acesso" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/login.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-colorbox.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-colorbox.js"></script>


<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		<?php
			if( date("Y-m-d") < '2014-01-02' ){
				echo '$.colorbox({href:"images/pop-up-ferias.jpg"});';	
			}
		?>
		
	});	
	
	function checaEmail(){
	  var email = $("#txt-email-cadastro").val();
	  var redir = $("#redir").val();
	  $("#txt-email-cadastro").css('background',"url('images/spinner.gif') no-repeat scroll 96% 50% transparent");
	  $.ajax({
        type: "POST",
        url: "ajax_checa_email.php",
        dataType: "html",
		timeout: 5000,
        data: {txtemail: email},
        success: function(data){
		  if (data == "ok"){
			var form = $('<form style="display:none;" action="cadastro.php" method="post">' +
			  '<input type="text" name="txt-email" value="' + email + '" />' +
			  '<input type="text" name="redir" value="' + redir + '" />' +
			  '</form>');
			$('body').append(form);
			$(form).submit();
		  }else{
		    $("#erro").html('E-mail já cadastrado.');
			$("#txt-email-cadastro").css('background',"");
		  }
		}
	  })
	}
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">
    	<div id="box-efeito-left"></div>
    	<?php
			include("inc_headerSTEP.php");
		?>
	</div>
	<div id="main-box-container" style="overflow:hidden;">
		<div id="andamento">
			<span class="passox <?php if(basename(__FILE__) == 'login.php'){echo "ativo";} ?>" style="margin-left:0;">1 - Identificação</span>
			<span class="passox <?php if(basename(__FILE__) == 'endereco.php'){echo "ativo";} ?>">2 - Entrega</span>
			<span class="passox <?php if(basename(__FILE__) == 'pagamento.php'){echo "ativo";} ?>">3 - Pagamento</span>
			<span class="passox <?php if(basename(__FILE__) == 'finaliza_pedido.php'){echo "ativo";} ?>">4 - Confirmação</span>
		</div>
        
        <div id="container-cadastro">
        
            <form name="frm_login" id="frm_login" method="post" action="login.php" onsubmit="return valida_login();">    
                <input type="hidden" name="action" id="action" value="dologin" />
                <input type="hidden" name="redir" id="redir" value="<?php echo $redir;?>" />     
                
                <div id="box-login">
                
                    <div id="confirmacao-pagamento" style="width:320px; height:auto; margin-top:50px; color:#a95d99; font-size:18px; margin-left:45px; ">
                        <span class="txt-cadastro-login">Já tenho cadastro</span>
                    </div>
                    
                    <input type="text" name="txt-email" id="txt-email" class="txt-login-field" value="Digite o e-mail" onfocus="if(this.value == 'Digite o e-mail'){this.value=''; $(this).attr('style','color:#000;');}" onblur="if(this.value == ''){this.value='Digite o e-mail'; $(this).removeAttr('style'); }" />
                    
                    <input type="text" name="txt-senha" id="txt-senha" class="txt-senha-field" value="Senha" onfocus="if(this.value == 'Senha'){this.value=''; $(this).get(0).type='password'; $(this).attr('style','color:#000;');}" onblur="if(this.value == ''){this.value='Senha'; $(this).get(0).type='text'; $(this).removeAttr('style'); }" />
					
                    
                    <div class="esqueci-email-senha">
                      <a href="esqueci-minha-senha.php" >Esqueci minha senha</a></p>
					  <input type="submit" alt="Botão Entrar" value="Continuar =>" id="btn-entrar"/>
                    </div>
                    
                    
                    <span class="msg" style="margin-left:45px;"><?php echo $msg;?></span>	
                </div>
            </form>            
          
          
            <div id="sep-login"></div>
            
            <div id="box-cadastro">
                <div class="txt-cadastro-login" style="margin-top:57px; margin-left:54px; color:#f00;">
                Ainda não tenho cadastro
                </div>
				<span style="float: left; margin-left: 52px; font-family: Oswald-BOLD; font-size: 18px; color: #676767;">Criar Cadastro</span>
				<div id="texto-cadastro">
					<span class="login-label"><span class="bolinha"></span>Faça seu cadastro com toda a segurança</span>
					<span class="login-label"><span class="bolinha"></span>Nosso site é 100% seguro<br></span>
					<span class="login-label"><span class="bolinha"></span>Comece digitando seu e-mail</span>
				</div>                
                <input type="text" name="txt-email-cadastro" id="txt-email-cadastro" class="txt-login-cadastro" value="Digite o e-mail" onfocus="if(this.value == 'Digite o e-mail'){this.value=''; $(this).attr('style','color:#000;');}" onblur="if(this.value == ''){this.value='Digite o e-mail';  $(this).removeAttr('style'); }" />
				<div style="height: 31px;width: 300px;float: left;margin-left: 45px;color:red;font-family: Arial;font-size: 12px;" id="erro"></div>

                <input value="Criar cadastro =>" class="criarCadastro" onclick="checaEmail();" type="button" name="enviar" />
            </div>        
        
        </div>
    
    </div>
  
  
    
    <div id="footer-container">
		<?php
            include("inc_footerSTEP.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>