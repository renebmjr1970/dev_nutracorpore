<?php
	include("include/inc_conexao.php");
	
	/*-------------------------------------------------------------
	pega número do pedido para detalhar
	--------------------------------------------------------------*/
	$codigo_pedido = $_REQUEST["codigo"];
	
	
	$ssql = "select tblpedido.pedidoid, tblpedido.pcodigo, tblpedido.pcodcadastro, tblpedido.ptitulo, tblpedido.pnome, tblpedido.pendereco, tblpedido.pnumero, tblpedido.pcomplemento,";
	$ssql .= " tblpedido.pbairro, tblpedido.pcidade, tblpedido.pestado, tblpedido.pcep, tblpedido.psubtotal, tblpedido.pvalor_desconto, tblpedido.pvalor_frete, tblpedido.pvalor_presente,";
	$ssql .= " tblpedido.pvalor_total, tblpedido.pcodforma_pagamento, tblpedido.pcodcondicao_pagamento, tblpedido.pcodfrete, tblpedido.pcodcupom_desconto, tblpedido.pcodigo_rastreamento, tblpedido.pcodstatus, tblpedido.pdata_cadastro,";
	
	$ssql .= " tblcadastro_endereco.etitulo, tblcadastro_endereco.enome, tblcadastro_endereco.eendereco, tblcadastro_endereco.enumero, tblcadastro_endereco.ecomplemento, 
				tblcadastro_endereco.ebairro, tblcadastro_endereco.ecidade, tblcadastro_endereco.eestado, tblcadastro_endereco.ecep, tblcadastro_endereco.ereferencia,";
	
	$ssql .= " tblforma_pagamento.fcodtipo, tblforma_pagamento.fdescricao, tblcondicao_pagamento.ccondicao,";
	$ssql .= " tblcadastro.cadastroid, tblcadastro.crazao_social, tblcadastro.cnome, tblcadastro.ccodtipo, tblcadastro.ccpf_cnpj, tblcadastro.crg_ie, tblcadastro.cdata_nascimento, tblcadastro.cemail,";
	$ssql .= " tblcadastro.ctelefone, tblcadastro.ccelular, tblcadastro_tipo.ttipo, ";
	$ssql .= " tblpedido_status.statusid, tblpedido_status.sdescricao, tb_cadastro_entrega.ereferencia as referencia_entrega ";
	
	$ssql .= " from tblpedido ";
	
	$ssql .= " left join tblcadastro_endereco on tblpedido.pcodendereco_fatura=tblcadastro_endereco.enderecoid ";
	$ssql .= " left join tblcadastro_endereco as tb_cadastro_entrega on tblpedido.pcep=tb_cadastro_entrega.ecep ";

	$ssql .= " left join tblcadastro on tblpedido.pcodcadastro=tblcadastro.cadastroid ";
	$ssql .= " left join tblcadastro_tipo on tblcadastro.ccodtipo=tblcadastro_tipo.tipoid ";
	$ssql .= " left  join tblforma_pagamento on tblpedido.pcodforma_pagamento=tblforma_pagamento.formapagamentoid ";
	$ssql .= " left join tblcondicao_pagamento on tblpedido.pcodcondicao_pagamento=tblcondicao_pagamento.condicaoid ";
	$ssql .= " left join tblpedido_status on tblpedido.pcodstatus=tblpedido_status.statusid ";
	$ssql .= " where tblpedido.pedidoid='{$codigo_pedido}' AND tblpedido.pcodcadastro='{$_SESSION["cadastro"]}'";
	$ssql .= " limit 0,1";

	//echo $ssql;
	
	$result = mysql_query($ssql);
	if(mysql_num_rows($result) == 0){
		header("Location: index.php");
	}
	if($result){
		
		while($row=mysql_fetch_assoc($result)){
			
			//$codigo_pedido	 	= "000000".$row["pcodigo"];
			//$codigo_pedido	 	= substr($codigo_pedido, strlen($codigo_pedido)-6,6);
			$codigo_pedido      = 15500+$row["pcodigo"];
			$codigo_cliente		= $row["pcodcadastro"];
			$razao_social		= $row["crazao_social"] . "&nbsp;" . $row["cnome"];
			$tipo_cadastro		= $row["ttipo"];
			$cnpj_cpf			= $row["ccpf_cnpj"];
			$ie_rg				= $row["crg_ie"];
			$data_nascimento 	= formata_data_tela($row["cdata_nascimento"]);
			$telefone			= $row["ctelefone"];
			$celular			= $row["ccelular"];
			$data_pedido     	= formata_data_tela($row["pdata_cadastro"]);
			$identificacao	 	= $row["ptitulo"];
			$nome 			 	= $row["pnome"];
			$email 			 	= $row["cemail"];
			$endereco		 	= $row["pendereco"]. ", " . $row["pnumero"]. " - " . $row["pcomplemento"]. " - " . $row["pbairro"];
			$cidade 		 	= $row["pcidade"];
			$estado			 	= $row["pestado"];
			$cep 			 	= $row["pcep"];
			$cep			 	= substr($cep, 0,5). "-" . substr($cep, 5,3);
			$referencia 	 	= $row["referencia_entrega"];
			$codigo_rastreamento= $row["pcodigo_rastreamento"];
			
			if($row["etitulo"]!=""){
				$endereco_fatura	=  $row["etitulo"]." - ".$row["eendereco"].",&nbsp;".$row["enumero"]."&nbsp;".$row["ecomplemento"]." - ".$row["ebairro"]." - ".$row["ecidade"]." - ".$row["eestado"]." - ".$row["ecep"];
			}
			
			$subtotal		 	= formata_valor_tela($row["psubtotal"]);
			$valor_desconto	 	= formata_valor_tela($row["pvalor_desconto"]);
			$valor_presente		= formata_valor_tela($row["pvalor_presente"]);
			$valor_frete	 	= formata_valor_tela($row["pvalor_frete"]);
			$valor_total	 	= formata_valor_tela($row["pvalor_total"]);
			
			$pagamento_tipo 	= $row["fcodtipo"];
			$tipo_pagamento		= $row["fcodtipo"];
			
			$forma_pagamento 	= $row["fdescricao"];
			
			$cod_condicao_pgto 	= $row["pcodcondicao_pagamento"];
			$condicao_pagamento = $row["ccondicao"];
			
			$status				= $row["statusid"];
			$status_pedido 		= $row["sdescricao"];
			$tipo_frete 		= $row["pcodfrete"];
			$cupom_desconto		= $row["ccupom"];	
						
			$codigo_rastreamento= $row["pcodigo_rastreamento"];

			
		}
		mysql_free_result($result);
	}
	
	
	// Nova busca para pegar tipo do frete - Nome coluna igual a de outra tabela
	
	$ssql = "select tblfrete_tipo.fdescricao, tblfrete_tipo.flink_rastreamento from tblfrete_tipo where freteid=" . $tipo_frete;
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$tipo_frete = $row["fdescricao"];
			$link_rastreamento	= $row["flink_rastreamento"];
			$link_rastreamento	= str_replace("#codigo_rastreamento#",$codigo_rastreamento,$link_rastreamento);
		}
		mysql_free_result($result);
	}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Minha Conta - Meus Pedidos</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="description" content="<?php echo $site_nome;?> Meus Pedidos. Acompanhe o status e histórico de suas compras." />
<meta name="keywords" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Meus Pedidos" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/detalhe-pedido.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#data1").mask("99/99/9999");
		$("#data2").mask("99/99/9999");
    });	
	
	function seta_status(st){
		document.getElementById("status").value = String(st);
		document.getElementById("frm_pedidos").submit();
	}
	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">
       	<?php
			include("inc_header.php");
		?>    
    </div>
    
	<div id="main-box-container">
        <div id="menu-topo-conta">
            <a href="minha-conta.php" id="meu-perfil">Minha Conta</a>
            <span style="float:left; padding:0; font-size:15px;">&nbsp;|&nbsp;</span>
            <a href="meus-pedidos.php" id="meus-pedidos">Meus Pedidos</a>
        </div>
    <div id="menu-conta-left" class="top-margin-align">
        <div class="box-categoria">
            <div class="categoria-menu"><span class="cat-menu-left">Meus Pedidos</span></div>
            <div class="subcategoria-menu"><span class="subcat-menu-left"><a href="meus-pedidos.php">Todos os pedidos</a></span></div>
            <div class="subcategoria-menu"><span class="subcat-menu-left"><a href="javascript:void(0);" onclick="javascript:seta_status(2);">Pedidos em andamento</a></span></div>
            <div class="subcategoria-menu"><span class="subcat-menu-left"><a href="javascript:void(0);" onclick="javascript:seta_status(6);">Pedidos concluídos</a></span></div>
        </div>
        
    </div>
    
    <div id="box-meio-minha-conta">
    	<div id="box-meus-dados" class="box-margin-align">
        	<h4 class="h4-minha-conta">Minha Conta - Meus pedidos</h4>
          <div class="clear-detalhe-pedido"><span class="txt-detalhe-pedido-bold">Detalhes do Pedido n&deg;: </span> <span class="txt-detalhe-pedido"><?php echo $codigo_pedido; ?></span></div>
            <div class="clear-detalhe-pedido"><span class="txt-detalhe-pedido-bold">Data do Pedido: </span> <span class="txt-detalhe-pedido"><?php echo $data_pedido; ?></span></div>
            <h4 class="h4-detalhe-pedido">DADOS DE ENTREGA</h4>
            <div class="relacao-pedidos-detalhes">
            	<span class="dados-pedido-bold">Identificação: </span> <span class="dados-pedido"><?php echo $identificacao; ?></span>
                <span class="dados-pedido-bold">Destinatário: </span> <span class="dados-pedido"><?php echo $nome; ?></span>
                <span class="dados-pedido-bold">Endereço: </span> <span class="dados-pedido"><?php echo $endereco; ?></span>
                <span class="dados-pedido-bold">Cidade: </span> <span class="dados-pedido"><?php echo $cidade; ?></span>
                <span class="dados-pedido-bold">Estado: </span> <span class="dados-pedido"><?php echo $estado ?></span>
                <span class="dados-pedido-bold">CEP: </span> <span class="dados-pedido"><?php echo $cep; ?></span>
                <span class="dados-pedido-bold">Ponto de Referência: </span> <span class="dados-pedido"><?php echo $referencia; ?></span>
                <span class="dados-pedido-bold">&nbsp;</span>
                <!--//
                <span class="dados-pedido-bold">Data prevista para entrega: </span> <span class="dados-pedido"></span>
                //-->
                <span class="dados-pedido-bold">Status: </span> <span class="dados-pedido"><?php echo $status_pedido; ?></span>
                
				<?php 
				if($codigo_rastreamento!=""){
				?>
                <span class="dados-pedido-bold">Código de Rastreamento: </span> <a href="<?php echo $link_rastreamento;?>" target="_blank" class="link-rastreio">Rastrear Entrega</a>
                <?php
				}
				?>
                
                <span class="dados-pedido-bold">&nbsp;</span>
                <span class="dados-pedido-bold">Frete: </span> <span class="dados-pedido"><?php echo $tipo_frete; ?></span>
                <span class="dados-pedido-bold">Tipo de Frete: </span> <span class="dados-pedido">Normal</span>
            </div>
            <div class="relacao-pedidos-detalhes">
                <div id="titulos-relacao-detalhe">
					<span class="descricao-produto descricao-color">Descrição do produto</span>
                    <span class="qtde-produto">Quantidade</span>
                    <span class="servicos-produto">Serviços</span>
                    <span class="vlr-unitario-produto">Preço Unitário</span>
                    <span class="vlr-total-produto">Valor Total</span>
               	</div>
                <div id="itens-relacao">
					<?php
                        $ssql = "select tblpedido_item.pcodproduto, tblpedido_item.pcodpropriedade, tblpedido_item.pcodtamanho, tblpedido_item.pquantidade, tblpedido_item.pvalor_unitario, tblpedido_item.pdesconto,";
                        $ssql = $ssql . " tblproduto.pproduto";
                        $ssql = $ssql . " from tblpedido_item, tblproduto";
                        $ssql = $ssql . " where tblpedido_item.pcodpedido=" . $codigo_pedido;
                        $ssql = $ssql . " and tblproduto.produtoid=tblpedido_item.pcodproduto and tblpedido_item.pquantidade>0";
                        
						//echo $ssql;
        
                        $result = mysql_query($ssql);
                        if($result){
                        
						
							while($row=mysql_fetch_assoc($result)){	
								$qtde_itens += $row["pquantidade"];
							}
							
							if($desconto_cupom > 0 ){
								$desconto_cupom_pagseguro = $desconto_cupom/$qtde_itens;	
							}
						
							mysql_data_seek($result,0);
						
                            while($row=mysql_fetch_assoc($result)){
								$i++;
								
                                $produto			= $row["pproduto"];
                                $produto_qtde 	 	= $row["pquantidade"];
                                $valor_unitario 	= formata_valor_tela($row["pvalor_unitario"]);
                                $produto_desconto 	= formata_valor_tela($row["pdesconto"]);
                                $valor_final 		= $produto_qtde * $valor_unitario;
                                $valor_final 		= formata_valor_tela($valor_final);
                                
                                echo '	<div class="pedido-item">
                                            <span class="descricao-produto">'. $produto .'</span>
                                            <span class="qtde-produto">'. $produto_qtde .'</span>
                                            <span class="servicos-produto">Serviços</span>
                                            <span class="vlr-unitario-produto">R$ '. $valor_unitario .'</span>
                                            <span class="vlr-total-produto">R$ '. $valor_final .'</span>
                                        </div>';
										
										
								$produto = left($row["pproduto"],80);
								$produto = utf8_decode($produto); 
								$pagseguro_frete = "0";		
								
								$pagseguro_items .='<input type="hidden" name="item_id_'.$i.'" value="'.$row["pcodproduto"].'">';	
								$pagseguro_items .='<input type="hidden" name="item_descr_'.$i.'" value="'.$produto. '">';
								$pagseguro_items .='<input type="hidden" name="item_quant_'.$i.'" value="'.$row["pquantidade"].'">';
								$pagseguro_items .='<input type="hidden" name="item_valor_'.$i.'" value="'.number_format($row["pvalor_unitario"]-$desconto_cupom_pagseguro,2,".",",").'">';
								$pagseguro_items .='<input type="hidden" name="item_frete_'.$i.'" value="'.number_format($pagseguro_frete,2).'" />';
								$pagseguro_items .='<input type="hidden" name="item_peso_'.$i.'" value="0"  />';										
										

								if(floatval($valor_frete)>0){
									$i++;
									$pagseguro_items .='<input type="hidden" name="item_id_'.$i.'" value="'.$i.'">';	
									$pagseguro_items .='<input type="hidden" name="item_descr_'.$i.'" value="Despesa de frete">';
									$pagseguro_items .='<input type="hidden" name="item_quant_'.$i.'" value="1">';
									$pagseguro_items .='<input type="hidden" name="item_valor_'.$i.'" value="'.number_format($valor_frete,2,".",",").'">';
									$pagseguro_items .='<input type="hidden" name="item_frete_'.$i.'" value="0" />';
									$pagseguro_items .='<input type="hidden" name="item_peso_'.$i.'" value="0"  />';				
								}
																			
                        
                            }
							mysql_free_result($result);
                        }
						
					//////////////////////////////////////////////////////////////////////////////
						//
						$pagseguro_email = get_configuracao("pagseguro_email");
						$pagseguro_email = strtolower($pagseguro_email);
						
						$link_pagseguro = '
									  <form method="post" action="https://pagseguro.uol.com.br/checkout/checkout.jhtml" id="frm_pagseguro" name="frm_pagseguro" target="pagseguro">
									  <input type="hidden" name="email_cobranca" value="'.$pagseguro_email.'"/>
									  <input type="hidden" name="tipo" value="CP"/>
									  <input type="hidden" name="moeda" value="BRL"/>
									  <input type="hidden" name="ref_transacao" value="'.$codigo_pedido.'" />';
						 $link_pagseguro .= $pagseguro_items;
						 
						 $link_pagseguro .= '             
									  <!-- IN&Iacute;CIO DOS DADOS DO USU&Aacute;RIO -->
									  <input type="hidden" name="cliente_nome" value="'.$nome.'" />
									  <input type="hidden" name="cliente_cep" value="'.$cep.'" />
									  <input type="hidden" name="cliente_end" value="'.$endereco.'" />
									  <input type="hidden" name="cliente_num" value="'.$numero.'" />
									  <input type="hidden" name="cliente_compl" value="'.$complemento.'" />
									  <input type="hidden" name="cliente_bairro" value="'.$bairro.'" />
									  <input type="hidden" name="cliente_cidade" value="'.$cidade.'" />
									  <input type="hidden" name="cliente_uf" value="'.$estado.'" />
									  <input type="hidden" name="cliente_pais" value="BRA" />
									  <input type="hidden" name="cliente_ddd" value="'.$ddd.'" />
									  <input type="hidden" name="cliente_tel" value="'.$telefone.'" />
									  <input type="hidden" name="cliente_email" value="'.$email.'" />
									  <input type="image" name="cmd_pagseguro" id="cmd_pagseguro" src="images/ico-pagseguro.gif">
									  <!-- FIM DOS DADOS DO USU&Aacute;RIO -->
									  </form>';						
						
                    ?>
                <!--div id="itens-relacao">
                    <div class="pedido-item">
                        <span class="descricao-produto">Descrição do produto</span>
                        <span class="qtde-produto">Quantidade</span>
                        <span class="servicos-produto">Serviços</span>
                        <span class="vlr-unitario-produto">Preço Unitário</span>
                        <span class="vlr-total-produto">Valor Total</span>
               		</div-->						
             	</div>
                <div id="box-valores-detalhe">
                	<span class="dados-pedido-detalhe">Sub-Total</span> <span class="valores-pedido-detalhe">R$ <?php echo $subtotal; ?></span>
                    <span class="dados-pedido-detalhe">Frete</span> <span class="valores-pedido-detalhe">R$ <?php echo $valor_frete; ?></span>
                    <span class="dados-pedido-detalhe">Total</span> <span class="valores-pedido-detalhe">R$ <?php echo $valor_total; ?></span>
                </div>
            </div>
            
            <h4 class="h4-detalhe-pedido">DADOS DE PAGAMENTO</h4>
            <div class="relacao-pedidos-detalhes">
            	<span class="dados-pagamento-pedido-bold">Valor total dos produtos: </span> <span class="dados-pedido">R$ <?php echo $subtotal; ?></span>
                <span class="dados-pagamento-pedido-bold">Despesas com frete: </span> <span class="dados-pedido">R$ <?php echo $valor_frete; ?></span>
                <span class="dados-pagamento-pedido-bold">Serviços (embalagens, garantias e outros): </span> <span class="dados-pedido">R$ <?php echo $valor_presente; ?></span>
                <span class="dados-pagamento-pedido-bold">Valor total à pagar: </span> <span class="dados-pedido">R$ <?php echo $valor_total; ?></span>
                <span class="dados-pedido-bold">&nbsp;</span>
                <span class="dados-pedido-bold"><?php echo $forma_pagamento; ?></span>
                <span class="dados-forma-pgto-pedido">Valor: R$ <?php echo $valor_total; ?></span>
                <?php 
					$ssql = "select * from tblcondicao_pagamento where condicaoid=" . $cod_condicao_pgto;
                    $result = mysql_query($ssql);
                    if($result){
						while($row=mysql_fetch_assoc($result)){
							$qtde_parcelas = $row["cnumero_parcelas"];
    					}
						mysql_free_result($result);
					}
				?>
                <span class="dados-forma-pgto-pedido">Parcelas: <?php echo $qtde_parcelas; ?></span>
                <span class="dados-forma-pgto-pedido">Status: <?php echo $status_pedido; ?></span>
                 <span class="dados-forma-pgto-pedido">&nbsp;</span>
            </div>
            

            <?php
            	if( $status == 1 || $status == 3 ){
					if($tipo_pagamento==5){
						
			?>
			<h4 class="h4-detalhe-pedido">Efetuar Pagamento</h4>
            <div class="relacao-pedidos-detalhes">
                <span class="dados-forma-pgto-pedido">Para efetuar seu pagamento clique no icone abaixo:<br /><br /></span>
                <span class="dados-forma-pgto-pedido"><?php echo $link_pagseguro;?><br /></span>
            </div>
            <?php
					}
				}
			?>            
            
        <?php
          	if( $status == 1 || $status == 3 ){
					if($tipo_pagamento==1){
			?>
            <h4 class="h4-detalhe-pedido">Efetuar Pagamento</h4>
            <div class="relacao-pedidos-detalhes">
            	
                <span class="dados-pagamento-pedido">
                Para efetuar o pagamento do boleto clique no icone abaixo:<br />
                <a href="boleto_itau.php?pedido=<?php echo $codigo_pedido;?>&email=<?php echo $email;?>" target="_blank"><img src="images/ico-boleto.gif" border="0" alt="imprimir boleto" /></a>
                </span>
                <span class="dados-pagamento-pedido">&nbsp;</span>
                <span class="dados-pagamento-pedido">&nbsp;</span>
                
            </div>         
            <?php
					}
				}
			?>               


            
            
<div id="box-btn-voltar">
                  <a href="" onclick="history.go(-1)"><img src="images/btn-voltar.jpg" border="0" alt="voltar"  /></a>
                </div>
        </div>
        
        
    </div>  
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>