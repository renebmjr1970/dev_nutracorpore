<?php

include("include/inc_conexao.php");
header("Content-type: text/xml");

$ssql = "SELECT tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.pdescricao_detalhada, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
				tblproduto.pdisponivel, tblproduto.ppeso,
				tblestoque.ecodproduto, pai.ppropriedade AS propriedade_pai, tblestoque.ecodtamanho, tblproduto_propriedade.ppropriedade AS ptamanho, 
				tblestoque.ecodpropriedade, proper.ppropriedade AS ppropriedade, tblestoque.eestoque,
				tblmarca.mmarca
				
				FROM tblproduto
				
				INNER JOIN tblestoque ON tblproduto.produtoid = tblestoque.ecodproduto
				LEFT JOIN tblproduto_propriedade ON tblestoque.ecodtamanho = tblproduto_propriedade.propriedadeid
				LEFT JOIN tblproduto_propriedade AS proper ON tblestoque.ecodpropriedade = proper.propriedadeid
				LEFT JOIN tblproduto_propriedade AS pai ON tblproduto_propriedade.pcodpropriedade = pai.propriedadeid
				INNER JOIN tblmarca on tblproduto.pcodmarca = tblmarca.marcaid

		WHERE tblproduto.pdisponivel = -1					
		order by tblproduto.pdata_cadastro desc
			";
$result = mysql_query($ssql);
if($result){
	
	echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<produtos>';

	while($row=mysql_fetch_assoc($result)){
		
		$link	=	"http://www.popdecor.com.br/" . $row["plink_seo"];
		$imagem	=	"http://www.popdecor.com.br/" . $row["pimagem"];
		$imagem =	str_replace("-tumb.","-big.",$imagem);
		
		
		$valor_unitario		= $row["pvalor_unitario"];
		$preco_promocao		= $row["pvalor_unitario"];
		
		if( $row["pvalor_comparativo"] > $row["pvalor_unitario"] ){
			$valor_unitario = $row["pvalor_comparativo"];	
		}
		
		$estoque			= intval($row["eestoque"]);
		$peso				= number_format($row["ppeso"],2,".","");
		
		
		$valor_unitario		= number_format($valor_unitario,2,".","");
		$preco_promocao		= number_format($preco_promocao,2,".","");
		

		echo '
		<produto>
		<produto_id>'.$row["produtoid"].'</produto_id>
		<sku><![CDATA['.$row["pcodigo"].']]></sku>
		<nome><![CDATA['.$row["pproduto"].']]></nome>
		<descricao><![CDATA['.$row["pdescricao_detalhada"].']]></descricao>
		<categorias>';
		
		//////////////////////////////////////////////////////////////////
		//categorias
		//////////////////////////////////////////////////////////////////
		$ssql1 = "select tblcategoria.ccategoria 
					from tblcategoria
				 	inner join tblproduto_categoria on tblcategoria.categoriaid = tblproduto_categoria.pcodcategoria 
					and tblproduto_categoria.pcodproduto = ".$row["produtoid"]."
					where tblcategoria.cativa = -1
					group by tblcategoria.ccategoria
					order by tblcategoria.ccategoria
				";
		$result1 = mysql_query($ssql1);
		if($result1){
			while($row1=mysql_fetch_assoc($result1)){
				echo '<categoria><![CDATA['.$row1["ccategoria"].']]></categoria>';
			}
			mysql_free_result($result1);
		}
		
		
		echo'
		</categorias>
		<imagens>';
		
		
		//////////////////////////////////////////////////////////////////
		//imagems
		//////////////////////////////////////////////////////////////////
		$ssql1 = "select tblproduto_midia.marquivo 
					from tblproduto_midia
				 	inner join tblproduto on tblproduto_midia.mcodproduto = tblproduto.produtoid
					where tblproduto_midia.mcodproduto = ".$row["produtoid"]."
					order by tblproduto_midia.mprincipal
				";
		$result1 = mysql_query($ssql1);
		if($result1){
			while($row1=mysql_fetch_assoc($result1)){
				$imagem = $site_site . "/" . $row1["marquivo"];
				$imagem = str_replace("-tumb.","-big.",$imagem);
				echo '<imagem><![CDATA['.$imagem.']]></imagem>';
			}
			mysql_free_result($result1);
		}
		
		
		echo '
		</imagens>
		<preco>'.$valor_unitario.'</preco>
		<preco_promocao>'.$preco_promocao.'</preco_promocao>
		<fabricante><![CDATA['.$row["mmarca"].']]></fabricante>
		<estoque>'.$estoque.'</estoque>
		<peso>'.$peso.'</peso>
		<atributos>
		<genero>unissex</genero>
		<publico></publico>
		<colecao></colecao>
		<cor></cor>
		<material></material>
		<estampa></estampa>
		<textura></textura>
		</atributos>
		<grade>
		<variacao>
			<variacao_id></variacao_id>
			<tipo></tipo>
			<cor></cor>
			<estoque></estoque>
			<peso></peso>
		</variacao>
		</grade>
		</produto>
		';
	}
	mysql_free_result();
	echo '</produtos>';
}
	


?>


