<?php
	include("include/inc_conexao.php");	
	include_once("include/inc_funcao.php");
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}	
	
	
	//monta a categoria
	$uri = str_replace("/nutraNew/","",$_SERVER['REQUEST_URI']);	

	$pagina = 1;
	$start = 0;
	$limit = 9;
	$ordem = 0;
	
	$canonical = "";

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}
	
	if(strpos($uri,"?")<>""){
		$uri = substr($uri,0,strpos($uri,"?")-1);
	}
	
	if(substr($uri,0,1)=="/"){
		$uri = substr($uri,1,strlen($uri));	
	}
	
	$nodes = explode("/",$uri);
	$departamentoid = intval(get_categoriaid_artigo_by_node($nodes[1],0));
	$categoriaid 	= intval(get_categoriaid_artigo_by_node($nodes[2],$departamentoid));
	$subcategoriaid = intval(get_categoriaid_artigo_by_node($nodes[3],$categoriaid));
	
	$codcategoria = $departamentoid;
	if((int)$categoriaid > 0){ $codcategoria = $categoriaid; }
	if((int)$subcategoriaid > 0){ $codcategoria = $subcategoriaid; }

	$config_agrupa_por_referencia = get_configuracao("config_agrupa_por_referencia");
	if($config_agrupa_por_referencia==0){
		$agrupa_por_referencia = ", tblproduto.produtoid ";	
	}
	
	if($departamentoid > 0 && $categoriaid == 0 && $subcategoriaid == 0){
		$ssql_categoria = $departamentoid;
	}

	if($departamentoid > 0 && $categoriaid > 0 && $subcategoriaid == 0){
		$ssql_categoria = $categoriaid;
	}

	if($departamentoid > 0 && $categoriaid > 0 && $subcategoriaid > 0){
		$ssql_categoria = $subcategoriaid;
	}
	
	$categoria = "$ssql_categoria";
	
	$ssql_categoria = mysql_query("select if(c.categoriaid is null,0,c.categoriaid) as categoria, if(s.categoriaid is null,0,s.categoriaid) as subcategoria
					from tblcategoria_artigo as c
					left join tblcategoria_artigo as s on s.ccodcategoria = c.categoriaid
					where c.ccodcategoria = $ssql_categoria;");
	while($row = mysql_fetch_array($ssql_categoria)){
		$categoria .= ",".$row["categoria"].",".$row["subcategoria"];
	}
	$hoje = date("Y-m-d");
	$ssql_categoria ="select artigoid,atitulo,adata_cadastro,aimagem,alink_seo,ccategoria from tblartigos
	left join tblcategoria_artigo on acategoria = categoriaid ";
	if($categoria != ''){$ssql_categoria .= " where acategoria in ($categoria) AND (data_inicio <= '{$hoje}' OR data_inicio IS NULL)";}else{$ssql_categoria .= " where data_inicio <= '{$hoje}' OR data_inicio IS NULL";}
	$ssql_categoria .= " order by adata_cadastro desc";

	$result = mysql_query($ssql_categoria);					
	if($result){
		$total_registros = mysql_num_rows($result);	
	}
	
	$ssql_categoria .= " limit $start, $limit";
	
	//echo $ssql_categoria;
	
	/*-------------------------------------------------------------------
	base href
	--------------------------------------------------------------------*/
	$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("categoria.php","",$_SERVER['SCRIPT_NAME']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $categoria?>" />
<meta name="description" content="<?php echo $categoria;?>" />
<meta name="keywords" content="<?php echo $categoria;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $categoria;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/categoria.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<?php
include("include/inc_analytics.php");	
?>
</head>

<body>
<div id="global-container">
	<div id="header-content">
	<?php
			include("inc_header.php");
		?>
    </div>
	<div id="main-box-container">
   	  <div id="box-sup-artigo">
			<form name="artigobusca"  method="post" action="busca/" onsubmit="return valida_busca_artigo();">
				<input type="text" value="<?=$string ?>" name="string" id="search-artigo">
				<input type="submit" value="" name="enviar" id="search-artigo-btn">
			</form>
		</div>
    	<div id="container-menu-left">
        	<?php
            	include("inc_left_artigo.php");
			?>            
        </div>
        <div class="box-products-container" style="margin:0px;">
            <div id="products-category-box">
			<?php
				if($total_registros){
			?>
			<div id="bannerArtigo">
				<?php
					$bannerCont = 0;
					$banners = mysql_query($ssql_categoria);
					$mes = array('','Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez');
					while($banner = mysql_fetch_array($banners)){
						if($bannerCont){ $class = "off"; }else{ $class = "on"; }
						$data = explode(" ",$banner["adata_cadastro"]);
						$data = explode("-",$data[0]);
						$data = $data[2]." / ".$mes[(int)$data[1]];
						$imagem = str_replace("tumb","big",$banner["aimagem"]);
						if(!file_exists($imagem)){ $imagem = "imagem/produto/med-indisponivel.png"; }
				?>
					<span class="<?=$class ?>" title="BannerArtigo" style="background:url('<?=$imagem ?>') no-repeat center;" onclick="window.location='artigo/<?=$banner["alink_seo"]."---".$banner["artigoid"] ?>';" rel="<?=$bannerCont ?>">
					<span class="dataBannerArtigo"><?=$data ?></span>
					<span class="tituloBannerArtigo"><?=$banner["atitulo"] ?></span>
					<span class="publicadoBannerArtigo">Publicado em: <?=$banner["ccategoria"] ?></span>
					</span>
				<?php
						$bannerCont++;
					}
					$width = str_replace(",",".",(630-$bannerCont)/$bannerCont);
				?>
				<div id="botoesBanner">
				<?php
					for($x=0;$x<$bannerCont;$x++){
						echo '<div class="botaoBanner" rel="width:'.$width.'px" style="width:'.$width.'px';
						if($x==0){ echo ";background:#2578c8"; }
						echo '" onmouseover="ativaBArtigo('.$x.');">'.$x.'</div>';
					}
				?>
				</div>
			</div>
			<?php
				}
			?>
			<div id="products-title">MAIS RECENTES</div>
				<div style="width:650px;">
					<?php
						if($total_registros){
							$ssql_categoria = mysql_query($ssql_categoria);
							while($row = mysql_fetch_array($ssql_categoria)){
								$data = explode(" ",$row["adata_cadastro"]);
								$data = explode("-",$data[0]);
								$data = $data[2]."/".$data[1]."/".$data[0];
								$imagem = $row["aimagem"];
								if(!file_exists($imagem)){ $imagem = "imagem/produto/tumb-indisponivel.png"; }
						?>
							<a alt="artigo" href="artigo/<?=$row["alink_seo"]."---".$row["artigoid"] ?>">
							<div class="artigo-box-two">
								<span class="img-prev-two">
									<img src="<?=$imagem ?>" alt="Artigo">
								</span>
								<span class="artigo-rel-date"><?=$data ?></span>
								<span class="artigo-rel-tit">
									<?=$row["atitulo"] ?>
								</span>
							</div>
							</a>
						<?php
							}
						}else{
							echo "<div align='center' style='margin:30px 0;'>Nenhum resgistro encontrado.</div>";
						}
					?>
				</div>
                <div id="org-sup-box-content">
                    <div class="pagination-box">
                    	<div class="paginacao"><span class="paginacao-text">Página:</span></span> 
							<?php
								echo paginacao($pagina, $limit, $total_registros);
							?>                            
                        </div>
                    </div>
                </div>
              
              
			</div>
        </div>
		<div id="aside-right-bar2" style="margin-left:18px;margin-top:0px;">
			<?php carregaBanners(2); ?>
		</div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>