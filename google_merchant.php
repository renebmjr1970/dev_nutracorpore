<?php
ini_set('display_errors', true);
include("include/inc_conexao.php");

header("Content-type: text/xml");

	echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">';
	echo '<channel>';
	echo "<title>".$site_nome."</title>";
	echo "<link>http://".$_SERVER['SERVER_NAME']."</link>";
	echo "<description>".$site_descricao."</description>";
	
	$ssql  = "SELECT ";
	$ssql .= "* ";
	$ssql .= "FROM tblproduto ";
	$ssql .= "JOIN tblmarca on tblmarca.marcaid = tblproduto.pcodmarca ";
	$ssql .= "JOIN tblproduto_midia on tblproduto_midia.mcodproduto = tblproduto.produtoid ";
	// $ssql .= "LIMIT 0, 74";

	//$ssql .= "INNER JOIN tblproduto_condicao_pagamento ON tblproduto_condicao_pagamento.pcodproduto = tblproduto.produtoid ";
	//$ssql .= "INNER JOIN tblcondicao_pagamento ON tblcondicao_pagamento.condicaoid = tblproduto_condicao_pagamento.pcodcondicao ";
	//$ssql .= "WHERE pfeed = 1";
	//$ssql .= "LIMIT 0, 80";
	$cnt = 0;
	$result = mysql_query($ssql);
	if($result){
		while( $row = mysql_fetch_assoc($result) ){
			
			if ($row['produtoid'] <> 103) {			
			

			$id		 	  = $row["produtoid"];
			$codigo		  = $row["pcodigo"];
			$produto 	  = mb_convert_case(($row["pproduto"]), MB_CASE_TITLE, "UTF-8");
			$imagem_big   = str_replace("-tumb", "-big", $row["marquivo"]);
			$imagem		  = "http://".$_SERVER['SERVER_NAME']."/" . $imagem_big;
			$descricao	  = $row["pdescricao"];
			$valor		  = $row["pvalor_unitario"];
			$marca		  = $row["mmarca"];

			$categoria	  = '';
			
			$cursorCategorias = "SELECT tblcategoria.ccategoria, tblcategoria.clink_seo
			FROM tblcategoria
			JOIN tblproduto_categoria ON tblcategoria.categoriaid = tblproduto_categoria.pcodcategoria
			WHERE tblproduto_categoria.pcodproduto = $id
			ORDER BY tblcategoria.ccodcategoria";
			
			$listaCategorias = mysql_query($cursorCategorias);
			while($cursor=mysql_fetch_assoc($listaCategorias)){
				$compara = strtolower($cursor["ccategoria"]);
				if(!strstr($compara,"creatina") && !strstr($compara,"carnetina") && !strstr($compara,"carnitine") && !strstr($compara,"polifenol de alcachofra")){
					if($categoria!=""){
						$categoria .= " > ";
					}
					$categoria .= $cursor["ccategoria"];
					$subcategoria = $cursor["clink_seo"];
				}
			}
			
			$condicao_pagamento = "SELECT MAX(tblcondicao_pagamento.cnumero_parcelas) AS parcelas FROM tblproduto_condicao_pagamento INNER JOIN tblcondicao_pagamento ON tblcondicao_pagamento.condicaoid = tblproduto_condicao_pagamento.pcodcondicao WHERE tblproduto_condicao_pagamento.pcodproduto = ".$row["produtoid"];
			$condicao = mysql_query($condicao_pagamento);
			while($ln = mysql_fetch_assoc($condicao)){
				$parcelas = $ln["parcelas"];
			}
			$parcela      = number_format($valor / $parcelas, 2, ",", ".");
			
			//$link  = "http://".$_SERVER['SERVER_NAME']."/" . $row["plink_seo"] . "?utm_source=google&utm_medium=shopping&utm_campaign=".$subcategoria;
			$link  = "http://".$_SERVER['SERVER_NAME']."/".$row["plink_seo"];
			echo "<item>";
			echo "<title><![CDATA[".$produto."]]></title>";
			echo "<link><![CDATA[".$link."]]></link>";
			echo "<description><![CDATA[".$descricao."]]></description>"; 
			echo "<g:id>".$id."</g:id>";
			echo "<g:condition>new</g:condition>";
			echo "<g:price>".number_format($valor-($valor * 0.10),2,",",".")."</g:price>";
			echo "<price-no-rebate>".$comdesconto."</price-no-rebate>";
			echo "<month-price-number>".$parcelas."</month-price-number>";
			echo "<month-price-value>".$parcela."</month-price-value>"; 
			echo "<g:availability>in stock</g:availability>";
			echo "<g:image_link><![CDATA[".$imagem."]]></g:image_link>";
			echo "	<g:shipping>
						<g:country>BR</g:country>
						<g:service>Transportadora</g:service>
						<g:price>0.00</g:price>
					</g:shipping>";
			echo "<g:brand><![CDATA[".$marca."]]></g:brand>";
			echo "<g:google_product_category><![CDATA[".$categoria."]]></g:google_product_category>";
			echo "<g:product_type><![CDATA[".$subcategoria."]]></g:product_type>";
			echo "<g:mpn><![CDATA[".$codigo."]]></g:mpn>";
			echo "</item>";
			
			}
			$cnt++;
		}
		echo '</channel>';
		echo '</rss>';

		mysql_free_result($result);
	}

	

?>