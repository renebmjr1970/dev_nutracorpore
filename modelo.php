<?php
	include("include/inc_conexao.php");
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Cadastro</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Cadastro" />
<meta name="description" content="<?php echo $site_nome;?> Cadastro. Efetue seu cadastro e tenha acesso a comprar produto em nossa loja virtual." />
<meta name="keywords" content="<?php echo $site_nome;?> Cadastro" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?> Cadastro" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/modelo.php" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
$(document).ready(function() {	
	$("#txt-telefone").mask("99 9999-9999");
	$("#txt-celular").mask("99 99999-9999");
	$("#txt-cnpj").mask("999.999.999-99");
	$("#txt-data-nascimento").mask("99/99/9999");
	
	
	<?php
		if($id>0){
			echo '$("#txt-email").attr("disabled","disabled");';
			echo '$("#txt-email2").attr("disabled","disabled");';
			echo '$("#txt-cnpj").attr("disabled","disabled");';			
			echo '$("#tipo").attr("disabled","disabled");';			
		}
		else
		{
			echo '$("#tipo").attr("checked","checked");';			
			echo '$("#news").attr("checked","checked");';			
			echo '$("#sms").attr("checked","checked");';			
		}
	?>
	
});	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
            
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>