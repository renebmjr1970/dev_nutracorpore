<link href="css/owl.carousel.css" rel="stylesheet">
<script src="js/owl.carousel.js"></script>
<?php
	$welcome_message = get_welcome_message();
	
	$totals = get_carrinho_totals();
	
	@list($cart_items, $cart_total, $cart_subtotal, $cart_frete, $cart_desconto, $cart_desconto_cupom, $cart_peso) = explode("||",$totals);
	
	if(!is_numeric($cart_items)){
		$cart_items = 0;	
	}
	
	
	
	$cart_items 			= number_format($cart_items,0);
	$cart_total 			= number_format($cart_total,2,",",".");
	$cart_subtotal 			= number_format($cart_subtotal,2,",",".");
	$cart_frete				= number_format($cart_frete,2,",",".");
	$cart_desconto			= number_format($cart_desconto,2,",",".");
	$cart_desconto_cupom	= number_format($cart_desconto_cupom,2,",",".");
	$cart_peso				= number_format($cart_peso,2,",",".");
	


	/*----------------------------------------------------------------------
	verifica se veio parametro utm_source para gerar descontos
	----------------------------------------------------------------------*/
	if( isset( $_REQUEST["utm_source"] ) ){
		$expires = time()+ 60 * 60 * 1 * 1; // 1 hora de cookie
		$utm_source = addslashes($_REQUEST["utm_source"]);
		
		$ssql = "select dutm_source from tbldesconto where dutm_source='{$utm_source}'";
		$result = mysql_query($ssql);
		if($result){
			$num_rows = mysql_num_rows($result);
			if($num_rows>0){
				$_SESSION["utm_source"] = $utm_source;
				setcookie("utm_source", $utm_source, $expires);
			}
			else
			{
				unset($_SESSION["utm_source"]);
				unset($_COOKIE["utm_source"]);
			}	
		}	
	}
	
	
	
	/*----------------------------------------------------------------------
	HTTP REFER - Referência de venda
	----------------------------------------------------------------------*/	
	if( isset($_SERVER['HTTP_REFERER']) ){
		$_SESSION['HTTP_REFERER'] = left( addslashes( $_SERVER['HTTP_REFERER'] ), 255);	
	}
	
	
	
	/*----------------------------------------------------------------------
	insere a palavra bucada na tblbusca quando vindo do buscador Google
	origem = 1 : busca do site
	origem = 2 : busca google
	----------------------------------------------------------------------*/
	if(isset($_SERVER['HTTP_REFERER'])){
	  $url = $_SERVER['HTTP_REFERER'];
	}else{
	  $url = "index.php";
	}
	$query = parse_url ($url, PHP_URL_QUERY); 
	$host = parse_url ($url, PHP_URL_HOST); 
	
	if (strstr ($query, 'q=') && strstr ($host, 'google.')) { 
		
		$ip = $_SERVER['REMOTE_ADDR'];
		$refer = "google";
		$refer = strtolower($refer);
		
		$keys = explode("&",$query);
		$string = $keys[2];
		$string = str_replace("q=","",$string);
		$string = urldecode($string);
		$string = addslashes(urldecode($string));
		
		$ssql = "select btag from tblbusca where btag='{$string}' and bip='{$ip}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)==0){
				$ssql = "insert into tblbusca (btag, borigem, breferencia, bip, bdata_alteracao, bdata_cadastro)
							values('{$string}','2','{$refer}','{$ip}','{$data_hoje}','{$data_hoje}')";
				mysql_query($ssql);
			}
			//echo $ssql.' '. $refer;
		}
		
	}
	
	
	
	/*-----------------------------------------------------------------------
	Estilo do menu por pagina / categoria
	-----------------------------------------------------------------------*/
	$url_pagina = str_replace("/stylemarket/","", $_SERVER['REQUEST_URI']);
	

?>
<div id="header">
	<div id="top-header">
	    <div id="logo" title="<?php echo $site_nome;?>" alt="<?php echo $site_nome;?>">
	       	<a href="index.php"><img src="images/logo.png" alt="<?php echo $site_nome;?>" title="<?php echo $site_nome;?>" border="0" /></a>        
	    </div>
	</div>
</div>
<div id="main-menu-container">
	<div class="carousel-container">
		<span style="color: #fff; font-family: DoppioOne-REGULAR; font-size: 15px; position: absolute; right: 130px; top: 7px;">Ambiente 100% seguro</span>
		<span style="color: #fff; font-family: DoppioOne-REGULAR; font-size: 12px; position: absolute; right: 127px; top: 26px;">Prossiga sua compra com tranquilidade.</span>
		<img src="images/100pcseguro.png" style="position: absolute; top: -41px; width: 110px; right: 0;">
	</div>
</div>
<script type="text/javascript">
	<?php carregaJs(); ?>
</script>